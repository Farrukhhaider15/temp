﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;


namespace Final_DB_Project
{
    public partial class mainPage : Form
    {
        public static mainPage mp = new mainPage();
        static  string query;
        public mainPage()
        {
            InitializeComponent();
            bunifuPages1.SetPage(2);
            customizeDesign();
        }
        private void customizeDesign()
        {
            manage_emp_subPanel.Visible = false;
            manage_manu_subPanel.Visible = false;
            manage_pro_subPanel.Visible = false;
            manage_stock_subPanel.Visible = false;
        }
        private void hideSubMenu()
        {
            if (manage_emp_subPanel.Visible == true)
            {
                manage_emp_subPanel.Visible = false;
            }
            if (manage_manu_subPanel.Visible == true)
            {
                manage_manu_subPanel.Visible = false;
            }
            if (manage_pro_subPanel.Visible == true)
            {
                manage_pro_subPanel.Visible = false;
            }
            if (manage_stock_subPanel.Visible == true)
            {
                manage_stock_subPanel.Visible = false;
            }
        }
        private void showSubMenu(Panel subMenu)
        {
            if (subMenu.Visible == false)
            {
                hideSubMenu();
                subMenu.Visible = true;
            }
            else
            {
                subMenu.Visible = false;
            }
        }
        private void bunifuButton1_Click(object sender, EventArgs e)
        {
        }

        private void bunifuButton2_Click(object sender, EventArgs e)
        {
        }

        private void bunifuButton3_Click(object sender, EventArgs e)
        {
        }

        private void bunifuButton4_Click(object sender, EventArgs e)
        {
        }

        private void bunifuButton5_Click(object sender, EventArgs e)
        {
        }

        private void mainPage_Load(object sender, EventArgs e)
        {

        }

        private void mainPage_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void bunifuLabel1_Click(object sender, EventArgs e)
        {

        }

        private void bunifuPanel1_Click(object sender, EventArgs e)
        {

        }

        private void bunifuButton2_Click_1(object sender, EventArgs e)
        {

        }

        private void bunifuButton3_Click_1(object sender, EventArgs e)
        {

        }

        private void bunifuButton4_Click_1(object sender, EventArgs e)
        {

        }

        private void bunifuButton5_Click_1(object sender, EventArgs e)
        {

        }

        private void bunifuButton2_Click_2(object sender, EventArgs e)
        {
        }

        private void bunifuButton3_Click_2(object sender, EventArgs e)
        {
           
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void bunifuShadowPanel3_ControlAdded(object sender, ControlEventArgs e)
        {

        }

        private void bunifuButton1_Click_1(object sender, EventArgs e)
        {
            
        }

        private void bunifuButton2_Click_3(object sender, EventArgs e)
        {
            bunifuPages1.SetPage(0);
        }

        private void bunifuShadowPanel13_ControlAdded(object sender, ControlEventArgs e)
        {

        }

        private void tableLayoutPanel2_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel2_Paint_2(object sender, PaintEventArgs e)
        {

        }

        private void bunifuButton8_Click(object sender, EventArgs e)
        {

        }

        private void bunifuButton8_Click_1(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            showSubMenu(manage_emp_subPanel);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            bunifuPages1.SetPage(2);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            bunifuPages1.SetPage(0);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            showSubMenu(manage_emp_subPanel);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            showSubMenu(manage_manu_subPanel);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            showSubMenu(manage_pro_subPanel);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            showSubMenu(manage_stock_subPanel);
        }

        private void bunifuButton2_Click_4(object sender, EventArgs e)
        {
            bunifuPages1.SetPage(1);
        }

        private void bunifuButton6_Click(object sender, EventArgs e)
        {
            
            bunifuPages1.SetPage(4);
            loadingDataIntoCombobox();
            query = "select R.Id , R.[Name] , R.Type , R.[Purchase Price] , R.[Sale Price] , R.[Description] , ManufacturerDetails.Name AS [Company Name], R.[Expiry Date] , R.[Manufacturing Date]  from  (select ProductDetails.Id, ProductDetails.[Name], LookUp.Category as Type , ProductDetails.[Purchase Price] ,ProductDetails.[Sale Price] , ProductDetails.[Description] , ProductDetails.ManufacturerId , ProductDetails.[Expiry Date] , ProductDetails.[Manufacturing Date]       from ProductDetails join Lookup on ProductDetails.Type = Lookup.Id) as R join ManufacturerDetails on ManufacturerDetails.Id = R.ManufacturerId  order by  R.Id desc ";
            displayData(dataGridView1, query);

        }
        int flag=0;
        private void addMedicine_Click(object sender, EventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd2 = new SqlCommand(" begin tran addMedicine  ", con);
                cmd2.ExecuteNonQuery();

                Type purchaseType = medicinePurchasePrice.GetType();
                Type saleType = medicineSalePrice.GetType();

                if (medicineName.Text != "")
                {
                    if (medicineTypeComboBox.Text != "")
                    {
                        if (medicinePurchasePrice.Text != "")
                        {
                            if (medicineSalePrice.Text != "" && (float.Parse(medicineSalePrice.Text) > float.Parse(medicinePurchasePrice.Text)) )   
                            {
                                if (companyNameCombobox.Text != "")
                                {
                                    if (manufacturingDate.Text != "")
                                    {
                                        if (expiryDate.Text != "" && ( DateTime.Parse(expiryDate.Text) > DateTime.Parse(manufacturingDate.Text)  ) )
                                        {
                                            if (description.Text != "")
                                            {
                                                SqlCommand cmd0 = new SqlCommand(" select Id  from Lookup   where Category = @medicineType ", con);
                                                cmd0.Parameters.AddWithValue("@medicineType", medicineTypeComboBox.Text);
                                                cmd0.ExecuteNonQuery();
                                                SqlCommand cmd1 = new SqlCommand(" select Id  from ManufacturerDetails   where Name = @medicineCompany ", con);
                                                cmd1.Parameters.AddWithValue("@medicineCompany", companyNameCombobox.Text);
                                                cmd1.ExecuteNonQuery();
                                                SqlCommand cmd = new SqlCommand(" insert into ProductDetails values(@firstName , @lastName , @contact , @email , @description , @registrationNumber , @expirydate , @status   ) ", con);
                                                cmd.Parameters.AddWithValue("@firstName", medicineName.Text);
                                                cmd.Parameters.AddWithValue("@lastName", cmd0.ExecuteScalar().ToString());
                                                cmd.Parameters.AddWithValue("@contact", int.Parse(medicinePurchasePrice.Text));
                                                cmd.Parameters.AddWithValue("@email", int.Parse(medicineSalePrice.Text));
                                                cmd.Parameters.AddWithValue("@description", description.Text);
                                                cmd.Parameters.AddWithValue("@registrationNumber", (cmd1.ExecuteScalar().ToString()));
                                                cmd.Parameters.AddWithValue("@expirydate", DateTime.Parse(expiryDate.Text));
                                                cmd.Parameters.AddWithValue("@status", DateTime.Parse(manufacturingDate.Text));
                                                cmd.ExecuteNonQuery();
                                                MessageBox.Show("Medicine Added Successfully ");
                                                displayData(dataGridView1 , " select R.Id , R.[Name] , R.Type , R.[Purchase Price] , R.[Sale Price] , R.[Description] , ManufacturerDetails.Name AS [Company Name], R.[Expiry Date] , R.[Manufacturing Date]  from  (select ProductDetails.Id, ProductDetails.[Name], LookUp.Category as Type , ProductDetails.[Purchase Price] ,ProductDetails.[Sale Price] , ProductDetails.[Description] , ProductDetails.ManufacturerId , ProductDetails.[Expiry Date] , ProductDetails.[Manufacturing Date]       from ProductDetails join Lookup on ProductDetails.Type = Lookup.Id) as R join ManufacturerDetails on ManufacturerDetails.Id = R.ManufacturerId order by  R.Id desc  ");
                                                SqlCommand cmd3 = new SqlCommand(" commit tran addMedicine  ", con);
                                                cmd3.ExecuteNonQuery();
                                            }
                                            else
                                            {
                                                MessageBox.Show("Please fill description in correct format.......");
                                            }
                                        }
                                        else
                                        {
                                            MessageBox.Show("Please fill expiry date in correct format.......");
                                        }

                                    }
                                    else
                                    {
                                        MessageBox.Show("Please fill manufacturing date in correct format.......");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Please fill company name in correct format.......");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Please fill sale price in correct format.......");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Please fill purchase price in correct format.......");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please fill medicine type in correct format.......");
                    }
                }
                else
                {
                    MessageBox.Show("Please fill medicine name in correct format.......");

                }

            }
            catch
            {
                MessageBox.Show("Please fill all the above fields in correct format");
                displayData(dataGridView1, "select R.Id , R.[Name] , R.Type , R.[Purchase Price] , R.[Sale Price] , R.[Description] , ManufacturerDetails.Name AS [Company Name], R.[Expiry Date] , R.[Manufacturing Date]  from  (select ProductDetails.Id, ProductDetails.[Name], LookUp.Category as Type , ProductDetails.[Purchase Price] ,ProductDetails.[Sale Price] , ProductDetails.[Description] , ProductDetails.ManufacturerId , ProductDetails.[Expiry Date] , ProductDetails.[Manufacturing Date]       from ProductDetails join Lookup on ProductDetails.Type = Lookup.Id) as R join ManufacturerDetails on ManufacturerDetails.Id = R.ManufacturerId  order by  R.Id desc");
            }

            
        }

        private void tabPage5_Click(object sender, EventArgs e)
        {
            
        }

        public void loadingDataIntoCombobox()
        {
            medicineTypeComboBox.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("   select Category from Lookup where Name = 'Medicine Type'   ", con);
            SqlDataReader DR1 = cmd.ExecuteReader();
            while (DR1.Read())
            {
                medicineTypeComboBox.Items.Add(DR1[0]);
            }
            DR1.Close();

            companyNameCombobox.Items.Clear();
            SqlCommand cmd1 = new SqlCommand("   select Name  from ManufacturerDetails      ", con);
            SqlDataReader DR2 = cmd1.ExecuteReader();
            while (DR2.Read())
            {
                companyNameCombobox.Items.Add(DR2[0]);
            }
            DR2.Close();
        }

        public void displayData(System.Windows.Forms.DataGridView dataGridView1  , string query )
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void bunifuButton5_Click_2(object sender, EventArgs e)
        {
            query = "select R.Id , R.[Name] , R.Type , R.[Purchase Price] , R.[Sale Price] , R.[Description] , ManufacturerDetails.Name AS [Company Name], R.[Expiry Date] , R.[Manufacturing Date]  from  (select ProductDetails.Id, ProductDetails.[Name], LookUp.Category as Type , ProductDetails.[Purchase Price] ,ProductDetails.[Sale Price] , ProductDetails.[Description] , ProductDetails.ManufacturerId , ProductDetails.[Expiry Date] , ProductDetails.[Manufacturing Date]       from ProductDetails join Lookup on ProductDetails.Type = Lookup.Id) as R join ManufacturerDetails on ManufacturerDetails.Id = R.ManufacturerId ";
            bunifuPages1.SetPage(5);
            displayData(dataGridView2, query);
            dataGridView2.Columns["Id"].ReadOnly = true;
        }

        private void guna2GradientTileButton2_Click(object sender, EventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd3 = new SqlCommand(" begin tran updateMedicine  ", con);
                cmd3.ExecuteNonQuery();

                SqlCommand cmd0 = new SqlCommand(" select Id  from Lookup   where Category = @medicineType ", con);
                cmd0.Parameters.AddWithValue("@medicineType", dataGridView2.Rows[dataGridView2.CurrentRow.Index].Cells[2].FormattedValue.ToString());
                cmd0.ExecuteNonQuery();

                SqlCommand cmd1 = new SqlCommand(" select Id  from [ManufacturerDetails]   where Name = @manufacturerType ", con);
                cmd1.Parameters.AddWithValue("@manufacturerType", dataGridView2.Rows[dataGridView2.CurrentRow.Index].Cells[6].FormattedValue.ToString());
                cmd1.ExecuteNonQuery();

                int flag = 0;

                if (float.Parse(dataGridView2.Rows[dataGridView2.CurrentRow.Index].Cells[4].FormattedValue.ToString()) > float.Parse(dataGridView2.Rows[dataGridView2.CurrentRow.Index].Cells[3].FormattedValue.ToString()))
                {
                    if (cmd0.ExecuteScalar() != null)
                    {
                        if (cmd1.ExecuteScalar() != null)
                        {
                            if (DateTime.Parse(dataGridView2.Rows[dataGridView2.CurrentRow.Index].Cells[7].FormattedValue.ToString()) > DateTime.Parse(dataGridView2.Rows[dataGridView2.CurrentRow.Index].Cells[8].FormattedValue.ToString()))
                            {
                                SqlCommand cmd = new SqlCommand(" UPDATE ProductDetails SET Name = @studentID  ,Type = @studentName , [Purchase Price] = @courseName   , [Sale Price] = @marks , Description = @updatedDate, ManufacturerId = @date, [Expiry Date] = @grade, [Manufacturing Date] = @description   where Id =  @search  ", con);
                                cmd.Parameters.AddWithValue("@studentID", dataGridView2.Rows[dataGridView2.CurrentRow.Index].Cells[1].FormattedValue.ToString());
                                cmd.Parameters.AddWithValue("@studentName", cmd0.ExecuteScalar().ToString());
                                cmd.Parameters.AddWithValue("@courseName", dataGridView2.Rows[dataGridView2.CurrentRow.Index].Cells[3].FormattedValue.ToString());
                                cmd.Parameters.AddWithValue("@marks", dataGridView2.Rows[dataGridView2.CurrentRow.Index].Cells[4].FormattedValue.ToString());
                                cmd.Parameters.AddWithValue("@updatedDate", dataGridView2.Rows[dataGridView2.CurrentRow.Index].Cells[5].FormattedValue.ToString());
                                cmd.Parameters.AddWithValue("@date", cmd1.ExecuteScalar().ToString());
                                cmd.Parameters.AddWithValue("@grade", dataGridView2.Rows[dataGridView2.CurrentRow.Index].Cells[7].FormattedValue.ToString());
                                cmd.Parameters.AddWithValue("@description", dataGridView2.Rows[dataGridView2.CurrentRow.Index].Cells[8].FormattedValue.ToString());
                                cmd.Parameters.AddWithValue("@search", dataGridView2.Rows[dataGridView2.CurrentRow.Index].Cells[0].FormattedValue.ToString());
                                cmd.ExecuteNonQuery();
                                MessageBox.Show("Successfully updated");

                                displayData(dataGridView2, " select R.Id , R.[Name] , R.Type , R.[Purchase Price] , R.[Sale Price] , R.[Description] , ManufacturerDetails.Name AS [Company Name], R.[Expiry Date] , R.[Manufacturing Date]  from  (select ProductDetails.Id, ProductDetails.[Name], LookUp.Category as Type , ProductDetails.[Purchase Price] ,ProductDetails.[Sale Price] , ProductDetails.[Description] , ProductDetails.ManufacturerId , ProductDetails.[Expiry Date] , ProductDetails.[Manufacturing Date]       from ProductDetails join Lookup on ProductDetails.Type = Lookup.Id) as R join ManufacturerDetails on ManufacturerDetails.Id = R.ManufacturerId   ");
                                SqlCommand cmd4 = new SqlCommand(" commit tran  updateMedicine  ", con);
                                cmd4.ExecuteNonQuery();
                            }
                            else
                            {
                                MessageBox.Show("Expiry Date is always greater than manufacturing date price. Please update in correct format......... ");
                            }

                        }
                        else
                        {
                            MessageBox.Show("This medicine company does not appear in this system......... ");
                        }
                    }
                    else
                    {
                        MessageBox.Show("This medicine type does not appear in this system  ......... ");
                    }
                }

                else
                {
                    MessageBox.Show("Sale price is always greater than purchase price. Please update in correct format.........  ");
                }
              
            }
            catch
            {
                MessageBox.Show(" Please update the medicines in correct format ");
                displayData(dataGridView2, "select R.Id , R.[Name] , R.Type , R.[Purchase Price] , R.[Sale Price] , R.[Description] , ManufacturerDetails.Name AS [Company Name], R.[Expiry Date] , R.[Manufacturing Date]  from  (select ProductDetails.Id, ProductDetails.[Name], LookUp.Category as Type , ProductDetails.[Purchase Price] ,ProductDetails.[Sale Price] , ProductDetails.[Description] , ProductDetails.ManufacturerId , ProductDetails.[Expiry Date] , ProductDetails.[Manufacturing Date]       from ProductDetails join Lookup on ProductDetails.Type = Lookup.Id) as R join ManufacturerDetails on ManufacturerDetails.Id = R.ManufacturerId  order by  R.Id desc");
            }

            
        }

        private void guna2GradientTileButton1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd0 = new SqlCommand(" select R.Id , R.[Name] , R.Type , R.[Purchase Price] , R.[Sale Price] , R.[Description] , ManufacturerDetails.Name AS [Company Name], R.[Expiry Date] , R.[Manufacturing Date]  from  (select ProductDetails.Id, ProductDetails.[Name], LookUp.Category as Type , ProductDetails.[Purchase Price] ,ProductDetails.[Sale Price] , ProductDetails.[Description] , ProductDetails.ManufacturerId , ProductDetails.[Expiry Date] , ProductDetails.[Manufacturing Date]       from ProductDetails join Lookup on ProductDetails.Type = Lookup.Id) as R join ManufacturerDetails on ManufacturerDetails.Id = R.ManufacturerId where R.Name = @search", con);
            cmd0.Parameters.AddWithValue("@search", searchBar.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd0);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView2.DataSource = dt;
            cmd0.ExecuteNonQuery();
        }

        private void bunifuButton4_Click_2(object sender, EventArgs e)
        {
            bunifuPages1.SetPage(6);
            displayData(dataGridView3, "   select * from [ManufacturerDetails] order by Id desc  ");
        }
        
        private void guna2GradientTileButton3_Click(object sender, EventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd2 = new SqlCommand(" begin tran deleteMedicine  ", con);
                cmd2.ExecuteNonQuery();

                if (searchBar.Text != "")
                {
                    SqlCommand cmd3 = new SqlCommand(" DELETE from ProductDetails where  Name = @searchID  ", con);
                    cmd3.Parameters.AddWithValue("@searchID", searchBar.Text);
                    if (cmd3 == null)
                    {
                        MessageBox.Show("Search is not found..........");

                    }

                    else
                    {
                        cmd3.ExecuteNonQuery();
                        MessageBox.Show("Successfully deleted");
                        SqlCommand cmd4 = new SqlCommand(" commit tran  deleteMedicine  ", con);
                        cmd4.ExecuteNonQuery();
                    }

                }
                else
                {
                    MessageBox.Show(dataGridView2.Rows[dataGridView2.CurrentRow.Index].Cells[0].FormattedValue.ToString());
                    SqlCommand cmd3 = new SqlCommand(" DELETE from ProductDetails where  Id = @searchID  ", con);
                    cmd3.Parameters.AddWithValue("@searchID", dataGridView2.Rows[dataGridView2.CurrentRow.Index].Cells[0].FormattedValue.ToString());
                    cmd3.ExecuteNonQuery();
                    MessageBox.Show("Successfully deleted");
                    SqlCommand cmd4 = new SqlCommand(" commit tran  deleteMedicine  ", con);
                    cmd4.ExecuteNonQuery();

                }
                displayData(dataGridView2, " select R.Id , R.[Name] , R.Type , R.[Purchase Price] , R.[Sale Price] , R.[Description] , ManufacturerDetails.Name AS [Company Name], R.[Expiry Date] , R.[Manufacturing Date]  from  (select ProductDetails.Id, ProductDetails.[Name], LookUp.Category as Type , ProductDetails.[Purchase Price] ,ProductDetails.[Sale Price] , ProductDetails.[Description] , ProductDetails.ManufacturerId , ProductDetails.[Expiry Date] , ProductDetails.[Manufacturing Date]       from ProductDetails join Lookup on ProductDetails.Type = Lookup.Id) as R join ManufacturerDetails on ManufacturerDetails.Id = R.ManufacturerId   ");

            }
            catch
            {
                MessageBox.Show("Medicine is not deleted. Please try again...........");
                displayData(dataGridView2, "select R.Id , R.[Name] , R.Type , R.[Purchase Price] , R.[Sale Price] , R.[Description] , ManufacturerDetails.Name AS [Company Name], R.[Expiry Date] , R.[Manufacturing Date]  from  (select ProductDetails.Id, ProductDetails.[Name], LookUp.Category as Type , ProductDetails.[Purchase Price] ,ProductDetails.[Sale Price] , ProductDetails.[Description] , ProductDetails.ManufacturerId , ProductDetails.[Expiry Date] , ProductDetails.[Manufacturing Date]       from ProductDetails join Lookup on ProductDetails.Type = Lookup.Id) as R join ManufacturerDetails on ManufacturerDetails.Id = R.ManufacturerId  order by  R.Id desc");
            }


        }

        private void guna2GradientButton1_Click(object sender, EventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd2 = new SqlCommand(" begin tran addCompany  ", con);
                cmd2.ExecuteNonQuery();
     
                        if (companyName.Text != "")
                        {
                            if ( companyCity.Text != "" )
                            {
                                if (companyCountry.Text != "")
                                {
                                    if (companyAddress.Text != "")
                                    {
                                        if ( companyContactNumber.Text != ""    )
                                        {
                                            if (companyEmail.Text != ""  )
                                            {
                                               
                                                SqlCommand cmd = new SqlCommand(" insert into ManufacturerDetails values(@firstName , @lastName , @contact , @email , @description , @registrationNumber    ) ", con);
                                                cmd.Parameters.AddWithValue("@firstName", companyName.Text);
                                                cmd.Parameters.AddWithValue("@lastName", companyCity.Text);
                                                cmd.Parameters.AddWithValue("@contact", companyCountry.Text);
                                                cmd.Parameters.AddWithValue("@email", companyCountry.Text);
                                                cmd.Parameters.AddWithValue("@description", companyContactNumber.Text);
                                                cmd.Parameters.AddWithValue("@registrationNumber", companyEmail.Text);
                                                cmd.ExecuteNonQuery();
                                                SqlCommand cmd3 = new SqlCommand(" commit tran addCompany  ", con);
                                                cmd3.ExecuteNonQuery();
                                        MessageBox.Show("Company Added Successfully ");
                                                displayData(dataGridView3, "   select * from [ManufacturerDetails] order by Id desc  ");
                                                
                                            }
                                            else
                                            {
                                                MessageBox.Show("Please fill email in correct format.......");
                                            }
                                        }
                                        else
                                        {
                                            MessageBox.Show("Please fill contact number in correct format.......");
                                        }

                                    }
                                    else
                                    {
                                        MessageBox.Show("Please fill address in correct format.......");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Please fill country in correct format.......");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Please fill city in correct format.......");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Please fill name in correct format.......");
                        }
                    

            }
            catch
            {
                MessageBox.Show("Medicine is not added .Please fill all the above fields in correct format.........");
                displayData(dataGridView3, "   select * from [ManufacturerDetails] order by Id desc  ");
            }

            
        }

        private void guna2GradientButton2_Click(object sender, EventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd2 = new SqlCommand(" begin tran addSupplier  ", con);
                cmd2.ExecuteNonQuery();
   
                                if (shipperName.Text != "")
                                {
                                    if (shipperContactNumber.Text != "")
                                    {
                                        if ( shipperCompanyNameComboBox.Text != "" )
                                        {
                                            if (shipperAddress.Text != "")
                                            {
                                                
                                                SqlCommand cmd1 = new SqlCommand(" select Id  from ManufacturerDetails   where Name = @companyName ", con);
                                                cmd1.Parameters.AddWithValue("@companyName", shipperCompanyNameComboBox.Text);
                                                cmd1.ExecuteNonQuery();
                                                SqlCommand cmd = new SqlCommand(" insert into Shippers values(@firstName , @lastName , @contact , @email    ) ", con);
                                                cmd.Parameters.AddWithValue("@firstName", shipperName.Text);
                                                cmd.Parameters.AddWithValue("@lastName", shipperContactNumber.Text);
                                                cmd.Parameters.AddWithValue("@contact", cmd1.ExecuteScalar().ToString() );
                                                cmd.Parameters.AddWithValue("@email", (shipperAddress.Text));
                                                cmd.ExecuteNonQuery();
                                                MessageBox.Show("Shippers Added Successfully ");
                                                displayData(dataGridView4, " select Shippers.Id , Shippers.name , Shippers.[Contact Number] , ManufacturerDetails.Name AS CompanyName , Shippers.Address from Shippers join ManufacturerDetails on Shippers.[Manufacturer Id] = ManufacturerDetails.Id   order by Id desc  ");
                                                SqlCommand cmd3 = new SqlCommand(" commit tran addSupplier  ", con);
                                                cmd3.ExecuteNonQuery();
                                            }
                                            else
                                            {
                                                MessageBox.Show("Please fill shipper's address in correct format.......");
                                            }
                                        }
                                        else
                                        {
                                            MessageBox.Show("Please fill shipper's company name in correct format.......");
                                        }

                                    }
                                    else
                                    {
                                        MessageBox.Show("Please fill contact number in correct format.......");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Please fill shipper's name in correct format.......");
                                }
                            

            }
            catch
            {
                MessageBox.Show("Shipper is not added .Please fill all the above fields in correct format.........");
                displayData(dataGridView4, " select Shippers.Id, Shippers.name, Shippers.[Contact Number], ManufacturerDetails.Name AS CompanyName, Shippers.Address from Shippers join ManufacturerDetails on Shippers.[Manufacturer Id] = ManufacturerDetails.Id   order by Id desc  ");
            }

            
        }

        private void bunifuButton8_Click_2(object sender, EventArgs e)
        {
            bunifuPages1.SetPage(7);
            displayData(dataGridView4, " select Shippers.Id, Shippers.name, Shippers.[Contact Number], ManufacturerDetails.Name AS CompanyName, Shippers.Address from Shippers join ManufacturerDetails on Shippers.[Manufacturer Id] = ManufacturerDetails.Id   order by Id desc  ");
            loadDataIntoShipperCombobox(shipperCompanyNameComboBox , "select Name  from ManufacturerDetails");
        }
        public void loadDataIntoShipperCombobox(ComboBox shipperCompanyNameComboBox , string query)
        {
           
            var con = Configuration.getInstance().getConnection();
            shipperCompanyNameComboBox.Items.Clear();
            SqlCommand cmd1 = new SqlCommand(query, con);
            SqlDataReader DR2 = cmd1.ExecuteReader();
            while (DR2.Read())
            {
                shipperCompanyNameComboBox.Items.Add(DR2[0]);
            }
            DR2.Close();
        }

        public void loadingDataofShippersCompany()
        {
            var con = Configuration.getInstance().getConnection();
            shipperCompanyNameComboBox.Items.Clear();
            SqlCommand cmd1 = new SqlCommand("   select Name from ManufacturerDetails      ", con);
            SqlDataReader DR2 = cmd1.ExecuteReader();
            while (DR2.Read())
            {
                shipperCompanyNameComboBox.Items.Add(DR2[0]);
            }
            DR2.Close();
        }

        private void bunifuButton3_Click_3(object sender, EventArgs e)
        {
        }

        private void bunifuButton3_Click_4(object sender, EventArgs e)
        {
            bunifuPages1.SetPage(8);
            displayData(dataGridView5, "select * from ManufacturerDetails order by Id desc");
            dataGridView5.Columns["Id"].ReadOnly = true;

        }

        private void searchCompany_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd0 = new SqlCommand(" select * from ManufacturerDetails where Name = @search", con);
            cmd0.Parameters.AddWithValue("@search", searchBar1.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd0);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView5.DataSource = dt;
            cmd0.ExecuteNonQuery();
        }

        private void updateCompany_Click(object sender, EventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd3 = new SqlCommand(" begin tran updateCompany  ", con);
                cmd3.ExecuteNonQuery();

                if (dataGridView5.Rows[dataGridView5.CurrentRow.Index].Cells[1].FormattedValue.ToString() != "")
                {
                    if (dataGridView5.Rows[dataGridView5.CurrentRow.Index].Cells[2].FormattedValue.ToString() != "")
                    {
                        if (dataGridView5.Rows[dataGridView5.CurrentRow.Index].Cells[3].FormattedValue.ToString() != "")
                        {
                            if (dataGridView5.Rows[dataGridView5.CurrentRow.Index].Cells[4].FormattedValue.ToString() != "")
                            {
                                if (dataGridView5.Rows[dataGridView5.CurrentRow.Index].Cells[5].FormattedValue.ToString() != "")
                                {
                                    if (dataGridView5.Rows[dataGridView5.CurrentRow.Index].Cells[6].FormattedValue.ToString() != "")
                                    {
                                        SqlCommand cmd = new SqlCommand(" UPDATE ManufacturerDetails SET   [Name] = @studentName , [City] = @courseName   , [Country] = @marks , [Address] = @updatedDate, [ContactNumber] = @date, [Email] = @grade  where Id =  @studentID  ", con);
                                        cmd.Parameters.AddWithValue("@studentID", dataGridView5.Rows[dataGridView5.CurrentRow.Index].Cells[0].FormattedValue.ToString());
                                        cmd.Parameters.AddWithValue("@studentName", dataGridView5.Rows[dataGridView5.CurrentRow.Index].Cells[1].FormattedValue.ToString());
                                        cmd.Parameters.AddWithValue("@courseName", dataGridView5.Rows[dataGridView5.CurrentRow.Index].Cells[2].FormattedValue.ToString());
                                        cmd.Parameters.AddWithValue("@marks", dataGridView5.Rows[dataGridView5.CurrentRow.Index].Cells[3].FormattedValue.ToString());
                                        cmd.Parameters.AddWithValue("@updatedDate", dataGridView5.Rows[dataGridView5.CurrentRow.Index].Cells[4].FormattedValue.ToString());
                                        cmd.Parameters.AddWithValue("@date", dataGridView5.Rows[dataGridView5.CurrentRow.Index].Cells[5].FormattedValue.ToString());
                                        cmd.Parameters.AddWithValue("@grade", dataGridView5.Rows[dataGridView5.CurrentRow.Index].Cells[6].FormattedValue.ToString());
                                        cmd.ExecuteNonQuery();
                                        MessageBox.Show("Successfully updated");

                                        displayData(dataGridView5, " select * from ManufacturerDetails order by Id desc   ");
                                        SqlCommand cmd4 = new SqlCommand(" commit tran  updateCompany  ", con);
                                        cmd4.ExecuteNonQuery();
                                    }
                                    else
                                    {
                                        MessageBox.Show("please update company's email in correct formay.......      ");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("please update company's contact number in correct formay.......      ");

                                }
                            }
                            else
                            {
                                MessageBox.Show("please update company's address  in correct formay.......      ");

                            }
                        }
                        else
                        {
                            MessageBox.Show("please update company's country in correct formay.......      ");

                        }
                    }
                    else
                    {
                        MessageBox.Show("please update company's city in correct formay.......      ");

                    }
                }
                else
                {
                    MessageBox.Show("please update company's name in correct formay.......      ");

                }
            }
            catch
            {
                MessageBox.Show(" Company's information is not updated Please try again........ ");
                displayData(dataGridView5, "   select * from [ManufacturerDetails] order by Id desc  ");
            }
            

        }

        private void deleteCompany_Click(object sender, EventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd4 = new SqlCommand(" begin tran deleteCompany  ", con);
                cmd4.ExecuteNonQuery();

                if (searchBar1.Text != "")
                {
                    SqlCommand cmd3 = new SqlCommand(" DELETE from [ManufacturerDetails] where  Name = @searchID  ", con);
                    cmd3.Parameters.AddWithValue("@searchID", searchBar1.Text);


                    if (cmd3 == null)
                    {
                        MessageBox.Show("Search is not found..........");

                    }

                    else
                    {
                        cmd3.ExecuteNonQuery();
                        SqlCommand cmd5 = new SqlCommand(" commit tran  deleteCompany  ", con);
                        cmd5.ExecuteNonQuery();
                        MessageBox.Show("Successfully deleted");
                        
                    }

                }
                else
                {
                    SqlCommand cmd3 = new SqlCommand(" DELETE from ManufacturerDetails where  Id = @searchID  ", con);
                    cmd3.Parameters.AddWithValue("@searchID", dataGridView5.Rows[dataGridView5.CurrentRow.Index].Cells[0].FormattedValue.ToString());
                    cmd3.ExecuteNonQuery();
                    MessageBox.Show("Successfully deleted");
                    SqlCommand cmd6 = new SqlCommand(" commit tran  deleteCompany  ", con);
                    cmd6.ExecuteNonQuery();

                }
                //displayData(dataGridView5, " select R.Id , R.[Name] , R.Type , R.[Purchase Price] , R.[Sale Price] , R.[Description] , ManufacturerDetails.Name AS [Company Name], R.[Expiry Date] , R.[Manufacturing Date]  from  (select ProductDetails.Id, ProductDetails.[Name], LookUp.Category as Type , ProductDetails.[Purchase Price] ,ProductDetails.[Sale Price] , ProductDetails.[Description] , ProductDetails.ManufacturerId , ProductDetails.[Expiry Date] , ProductDetails.[Manufacturing Date]       from ProductDetails join Lookup on ProductDetails.Type = Lookup.Id) as R join ManufacturerDetails on ManufacturerDetails.Id = R.ManufacturerId   ");
                displayData(dataGridView5, "select * from ManufacturerDetails order by Id desc");

            }
            catch
            {
                MessageBox.Show(" Company is not deleted. Please try again............ ");
                displayData(dataGridView5, "   select * from [ManufacturerDetails] order by Id desc  ");
            }

        }

        private void bunifuButton7_Click(object sender, EventArgs e)
        {
           
        }

        private void guna2GradientTileButton8_Click(object sender, EventArgs e)
        {
            
        }


        private void searchShipper_Click(object sender, EventArgs e)
        {
            
        }

        private void bunifuButton7_Click_1(object sender, EventArgs e)
        {
            bunifuPages1.SetPage(9);
            displayData(dataGridView6, " select Shippers.Id, Shippers.name, Shippers.[Contact Number], ManufacturerDetails.Name AS CompanyName, Shippers.Address from Shippers join ManufacturerDetails on Shippers.[Manufacturer Id] = ManufacturerDetails.Id   order by Id desc  ");
            dataGridView6.Columns["Id"].ReadOnly = true;
        }

        private void guna2GradientTileButton9_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd0 = new SqlCommand(" select Shippers.Id, Shippers.[name], Shippers.[Contact Number], ManufacturerDetails.Name AS CompanyName, Shippers.Address from Shippers join ManufacturerDetails on Shippers.[Manufacturer Id] = ManufacturerDetails.Id    where Shippers.name = @search order by Id desc  ", con);
            cmd0.Parameters.AddWithValue("@search", searchBar2.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd0);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView6.DataSource = dt;
            cmd0.ExecuteNonQuery();
        }

        private void guna2GradientTileButton8_Click_1(object sender, EventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd3 = new SqlCommand(" begin tran updateShippers  ", con);
                cmd3.ExecuteNonQuery();
                SqlCommand cmd1 = new SqlCommand(" select Id  from [ManufacturerDetails]   where Name = @companyName ", con);
                cmd1.Parameters.AddWithValue("@companyName", dataGridView6.Rows[dataGridView6.CurrentRow.Index].Cells[3].FormattedValue.ToString());
                cmd1.ExecuteNonQuery();


                if (dataGridView6.Rows[dataGridView6.CurrentRow.Index].Cells[0].FormattedValue.ToString() != "")
                {
                    if (dataGridView6.Rows[dataGridView6.CurrentRow.Index].Cells[1].FormattedValue.ToString() != "")
                    {
                        if (dataGridView6.Rows[dataGridView6.CurrentRow.Index].Cells[2].FormattedValue.ToString() != "")
                        {
                            if (dataGridView6.Rows[dataGridView6.CurrentRow.Index].Cells[3].FormattedValue.ToString() != "")
                            {
                                if (dataGridView6.Rows[dataGridView6.CurrentRow.Index].Cells[4].FormattedValue.ToString() != "")
                                {
                                    if (cmd1.ExecuteScalar() == null)
                                    {
                                        MessageBox.Show("The company that your have entered does not exist..........");
                                    }
                                    else
                                    {
                                        SqlCommand cmd = new SqlCommand(" UPDATE [Shippers] SET [name] = @studentName , [Contact Number] = @courseName   , [Manufacturer Id] = @marks , [Address] = @date   where Id =  @studentID  ", con);
                                        cmd.Parameters.AddWithValue("@studentID", dataGridView6.Rows[dataGridView6.CurrentRow.Index].Cells[0].FormattedValue.ToString());
                                        cmd.Parameters.AddWithValue("@studentName", dataGridView6.Rows[dataGridView6.CurrentRow.Index].Cells[1].FormattedValue.ToString());
                                        cmd.Parameters.AddWithValue("@courseName", dataGridView6.Rows[dataGridView6.CurrentRow.Index].Cells[2].FormattedValue.ToString());
                                        cmd.Parameters.AddWithValue("@marks", cmd1.ExecuteScalar().ToString());
                                        cmd.Parameters.AddWithValue("@date", dataGridView6.Rows[dataGridView6.CurrentRow.Index].Cells[4].FormattedValue.ToString());
                                        cmd.ExecuteNonQuery();
                                        MessageBox.Show("Successfully updated");

                                        SqlCommand cmd4 = new SqlCommand(" commit tran  updateShippers  ", con);
                                        cmd4.ExecuteNonQuery();
                                    }
                                    displayData(dataGridView6, " select Shippers.Id, Shippers.name, Shippers.[Contact Number], ManufacturerDetails.Name AS CompanyName, Shippers.Address from Shippers join ManufacturerDetails on Shippers.[Manufacturer Id] = ManufacturerDetails.Id   order by Id desc   ");





                                }
                                else
                                {
                                    MessageBox.Show("Please fill the shipper's address in correct format..........");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Please fill the shipper's company name in correct format.  ..........");

                            }
                        }
                        else
                        {
                            MessageBox.Show("Please fill the shipper's contact number in correct format..........");

                        }
                    }
                    else
                    {
                        MessageBox.Show("Please fill the shipper's name in correct format..........");

                    }
                }
            }
            catch
            {
                MessageBox.Show(" Unfortunately shipper is not updated. Please try again.............. ");
                displayData(dataGridView6, " select Shippers.Id, Shippers.name, Shippers.[Contact Number], ManufacturerDetails.Name AS CompanyName, Shippers.Address from Shippers join ManufacturerDetails on Shippers.[Manufacturer Id] = ManufacturerDetails.Id   order by Id desc  ");
            }
            
         
        }

        private void guna2GradientTileButton7_Click(object sender, EventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd5 = new SqlCommand(" begin tran deleteShipper  ", con);
                cmd5.ExecuteNonQuery();

                if (searchBar2.Text != "")
                {
                    SqlCommand cmd3 = new SqlCommand(" DELETE from Shippers where  name = @searchID  ", con);
                    cmd3.Parameters.AddWithValue("@searchID", searchBar2.Text);
                    if (cmd3 == null)
                    {
                        MessageBox.Show("Search is not found..........");

                    }

                    else
                    {
                        cmd3.ExecuteNonQuery();
                        MessageBox.Show("Successfully deleted");
                        SqlCommand cmd6 = new SqlCommand(" commit tran  deleteShipper  ", con);
                        cmd6.ExecuteNonQuery();
                    }

                }
                else
                {
                    SqlCommand cmd3 = new SqlCommand(" DELETE from Shippers where  Id = @searchID  ", con);
                    cmd3.Parameters.AddWithValue("@searchID", dataGridView6.Rows[dataGridView6.CurrentRow.Index].Cells[0].FormattedValue.ToString());
                    cmd3.ExecuteNonQuery();
                    MessageBox.Show("Successfully deleted");
                    SqlCommand cmd7 = new SqlCommand(" commit tran  deleteShipper  ", con);
                    cmd7.ExecuteNonQuery();

                }
                displayData(dataGridView6, " select Shippers.Id, Shippers.name, Shippers.[Contact Number], ManufacturerDetails.Name AS CompanyName, Shippers.Address from Shippers join ManufacturerDetails on Shippers.[Manufacturer Id] = ManufacturerDetails.Id   order by Id desc  ");

            }
            catch
            {
                MessageBox.Show("Unfortunately Shipper is not deleted. Please try again..............  ");
                displayData(dataGridView6, " select Shippers.Id, Shippers.name, Shippers.[Contact Number], ManufacturerDetails.Name AS CompanyName, Shippers.Address from Shippers join ManufacturerDetails on Shippers.[Manufacturer Id] = ManufacturerDetails.Id   order by Id desc  ");
            }
            
        }
    }
}
