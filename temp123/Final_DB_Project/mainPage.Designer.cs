﻿
namespace Final_DB_Project
{
    partial class mainPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainPage));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges3 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges4 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges5 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges6 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges7 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges8 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Utilities.BunifuPages.BunifuAnimatorNS.Animation animation1 = new Utilities.BunifuPages.BunifuAnimatorNS.Animation();
            this.bunifuLabel3 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel4 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuShadowPanel1 = new Bunifu.UI.WinForms.BunifuShadowPanel();
            this.bunifuPanel1 = new Bunifu.UI.WinForms.BunifuPanel();
            this.manage_stock_subPanel = new System.Windows.Forms.Panel();
            this.bunifuButton7 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton8 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.button6 = new System.Windows.Forms.Button();
            this.manage_pro_subPanel = new System.Windows.Forms.Panel();
            this.bunifuButton5 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton6 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.button2 = new System.Windows.Forms.Button();
            this.manage_manu_subPanel = new System.Windows.Forms.Panel();
            this.bunifuButton3 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton4 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.button1 = new System.Windows.Forms.Button();
            this.manage_emp_subPanel = new System.Windows.Forms.Panel();
            this.bunifuButton1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.button3 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.bunifuPanel2 = new Bunifu.UI.WinForms.BunifuPanel();
            this.label21 = new System.Windows.Forms.Label();
            this.bunifuPictureBox1 = new Bunifu.UI.WinForms.BunifuPictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.bunifuPages1 = new Bunifu.UI.WinForms.BunifuPages();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.bunifuShadowPanel13 = new Bunifu.UI.WinForms.BunifuShadowPanel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label20 = new System.Windows.Forms.Label();
            this.bunifuShadowPanel12 = new Bunifu.UI.WinForms.BunifuShadowPanel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label19 = new System.Windows.Forms.Label();
            this.bunifuShadowPanel11 = new Bunifu.UI.WinForms.BunifuShadowPanel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.label18 = new System.Windows.Forms.Label();
            this.bunifuShadowPanel9 = new Bunifu.UI.WinForms.BunifuShadowPanel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label16 = new System.Windows.Forms.Label();
            this.bunifuShadowPanel10 = new Bunifu.UI.WinForms.BunifuShadowPanel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label17 = new System.Windows.Forms.Label();
            this.bunifuShadowPanel8 = new Bunifu.UI.WinForms.BunifuShadowPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.guna2HScrollBar2 = new Guna.UI2.WinForms.Guna2HScrollBar();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.bunifuPanel3 = new Bunifu.UI.WinForms.BunifuPanel();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.bunifuShadowPanel7 = new Bunifu.UI.WinForms.BunifuShadowPanel();
            this.label14 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.bunifuShadowPanel6 = new Bunifu.UI.WinForms.BunifuShadowPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.bunifuShadowPanel5 = new Bunifu.UI.WinForms.BunifuShadowPanel();
            this.label12 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.bunifuShadowPanel2 = new Bunifu.UI.WinForms.BunifuShadowPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.bunifuShadowPanel4 = new Bunifu.UI.WinForms.BunifuShadowPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.bunifuShadowPanel3 = new Bunifu.UI.WinForms.BunifuShadowPanel();
            this.label11 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.guna2HScrollBar1 = new Guna.UI2.WinForms.Guna2HScrollBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.bunifuLabel36 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel2 = new Bunifu.UI.WinForms.BunifuLabel();
            this.emp_name = new Guna.UI2.WinForms.Guna2TextBox();
            this.bunifuLabel26 = new Bunifu.UI.WinForms.BunifuLabel();
            this.emp_pin = new Guna.UI2.WinForms.Guna2TextBox();
            this.bunifuLabel28 = new Bunifu.UI.WinForms.BunifuLabel();
            this.emp_cnic = new Guna.UI2.WinForms.Guna2TextBox();
            this.bunifuLabel27 = new Bunifu.UI.WinForms.BunifuLabel();
            this.emp_gender = new Guna.UI2.WinForms.Guna2TextBox();
            this.bunifuLabel30 = new Bunifu.UI.WinForms.BunifuLabel();
            this.emp_salary = new Guna.UI2.WinForms.Guna2TextBox();
            this.bunifuLabel29 = new Bunifu.UI.WinForms.BunifuLabel();
            this.emp_designation = new Guna.UI2.WinForms.Guna2ComboBox();
            this.bunifuLabel32 = new Bunifu.UI.WinForms.BunifuLabel();
            this.emp_dob = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.bunifuLabel31 = new Bunifu.UI.WinForms.BunifuLabel();
            this.emp_city = new Guna.UI2.WinForms.Guna2TextBox();
            this.bunifuLabel34 = new Bunifu.UI.WinForms.BunifuLabel();
            this.emp_country = new Guna.UI2.WinForms.Guna2TextBox();
            this.bunifuLabel33 = new Bunifu.UI.WinForms.BunifuLabel();
            this.emp_address = new System.Windows.Forms.TextBox();
            this.emp_contact = new Bunifu.UI.WinForms.BunifuLabel();
            this.emp_email = new Bunifu.UI.WinForms.BunifuLabel();
            this.guna2TextBox1 = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2TextBox2 = new Guna.UI2.WinForms.Guna2TextBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label27 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.addMedicine = new Guna.UI2.WinForms.Guna2GradientButton();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.expiryDate = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.manufacturingDate = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.label30 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.medicineCompanyName = new System.Windows.Forms.Label();
            this.medicineManufacturinfgDate = new System.Windows.Forms.Label();
            this.medicineExpiryDate = new System.Windows.Forms.Label();
            this.medicineDescription = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.medicineName = new Guna.UI2.WinForms.Guna2TextBox();
            this.medicinePurchasePrice = new Guna.UI2.WinForms.Guna2TextBox();
            this.medicineSalePrice = new Guna.UI2.WinForms.Guna2TextBox();
            this.companyNameCombobox = new Guna.UI2.WinForms.Guna2ComboBox();
            this.description = new Guna.UI2.WinForms.Guna2TextBox();
            this.medicineTypeComboBox = new Guna.UI2.WinForms.Guna2ComboBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.guna2GradientTileButton3 = new Guna.UI2.WinForms.Guna2GradientTileButton();
            this.guna2GradientTileButton2 = new Guna.UI2.WinForms.Guna2GradientTileButton();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.searchBar = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2GradientTileButton1 = new Guna.UI2.WinForms.Guna2GradientTileButton();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.guna2HtmlLabel1 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.guna2GradientButton1 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.companyEmail = new Guna.UI2.WinForms.Guna2TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.companyName = new Guna.UI2.WinForms.Guna2TextBox();
            this.companyCity = new Guna.UI2.WinForms.Guna2TextBox();
            this.companyContactNumber = new Guna.UI2.WinForms.Guna2TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.companyCountry = new Guna.UI2.WinForms.Guna2TextBox();
            this.companyAddress = new Guna.UI2.WinForms.Guna2TextBox();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel23 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.guna2GradientButton2 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.shipperName = new Guna.UI2.WinForms.Guna2TextBox();
            this.shipperContactNumber = new Guna.UI2.WinForms.Guna2TextBox();
            this.shipperAddress = new Guna.UI2.WinForms.Guna2TextBox();
            this.shipperCompanyNameComboBox = new Guna.UI2.WinForms.Guna2ComboBox();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel26 = new System.Windows.Forms.TableLayoutPanel();
            this.deleteCompany = new Guna.UI2.WinForms.Guna2GradientTileButton();
            this.updateCompany = new Guna.UI2.WinForms.Guna2GradientTileButton();
            this.tableLayoutPanel27 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel25 = new System.Windows.Forms.TableLayoutPanel();
            this.searchBar1 = new Guna.UI2.WinForms.Guna2TextBox();
            this.searchCompany = new Guna.UI2.WinForms.Guna2GradientTileButton();
            this.tableLayoutPanel24 = new System.Windows.Forms.TableLayoutPanel();
            this.guna2HtmlLabel2 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel29 = new System.Windows.Forms.TableLayoutPanel();
            this.deleetShippers = new Guna.UI2.WinForms.Guna2GradientTileButton();
            this.updateShippers = new Guna.UI2.WinForms.Guna2GradientTileButton();
            this.tableLayoutPanel30 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridView6 = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel31 = new System.Windows.Forms.TableLayoutPanel();
            this.searchBar2 = new Guna.UI2.WinForms.Guna2TextBox();
            this.searchShippers = new Guna.UI2.WinForms.Guna2GradientTileButton();
            this.tableLayoutPanel28 = new System.Windows.Forms.TableLayoutPanel();
            this.guna2HtmlLabel3 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.bunifuShadowPanel1.SuspendLayout();
            this.bunifuPanel1.SuspendLayout();
            this.manage_stock_subPanel.SuspendLayout();
            this.manage_pro_subPanel.SuspendLayout();
            this.manage_manu_subPanel.SuspendLayout();
            this.manage_emp_subPanel.SuspendLayout();
            this.bunifuPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuPictureBox1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.bunifuPages1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.bunifuShadowPanel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.bunifuShadowPanel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.bunifuShadowPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.bunifuShadowPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.bunifuShadowPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.bunifuShadowPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.bunifuPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.bunifuShadowPanel7.SuspendLayout();
            this.bunifuShadowPanel6.SuspendLayout();
            this.bunifuShadowPanel5.SuspendLayout();
            this.bunifuShadowPanel2.SuspendLayout();
            this.bunifuShadowPanel4.SuspendLayout();
            this.bunifuShadowPanel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.tableLayoutPanel18.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.tableLayoutPanel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.tableLayoutPanel22.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.tableLayoutPanel26.SuspendLayout();
            this.tableLayoutPanel27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            this.tableLayoutPanel25.SuspendLayout();
            this.tableLayoutPanel24.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.tableLayoutPanel29.SuspendLayout();
            this.tableLayoutPanel30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).BeginInit();
            this.tableLayoutPanel31.SuspendLayout();
            this.tableLayoutPanel28.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuLabel3
            // 
            this.bunifuLabel3.AllowParentOverrides = false;
            this.bunifuLabel3.AutoEllipsis = false;
            this.bunifuLabel3.Cursor = System.Windows.Forms.Cursors.Default;
            this.bunifuLabel3.CursorType = System.Windows.Forms.Cursors.Default;
            this.bunifuLabel3.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold);
            this.bunifuLabel3.Location = new System.Drawing.Point(13, 10);
            this.bunifuLabel3.Name = "bunifuLabel3";
            this.bunifuLabel3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel3.Size = new System.Drawing.Size(140, 37);
            this.bunifuLabel3.TabIndex = 0;
            this.bunifuLabel3.Text = "Dashboard";
            this.bunifuLabel3.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel3.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel4
            // 
            this.bunifuLabel4.AllowParentOverrides = false;
            this.bunifuLabel4.AutoEllipsis = false;
            this.bunifuLabel4.CursorType = null;
            this.bunifuLabel4.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold);
            this.bunifuLabel4.Location = new System.Drawing.Point(13, 53);
            this.bunifuLabel4.Name = "bunifuLabel4";
            this.bunifuLabel4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel4.Size = new System.Drawing.Size(51, 25);
            this.bunifuLabel4.TabIndex = 2;
            this.bunifuLabel4.Text = "Home";
            this.bunifuLabel4.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel4.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuShadowPanel1
            // 
            this.bunifuShadowPanel1.BackColor = System.Drawing.Color.MidnightBlue;
            this.bunifuShadowPanel1.BorderColor = System.Drawing.Color.White;
            this.bunifuShadowPanel1.BorderRadius = 1;
            this.bunifuShadowPanel1.BorderThickness = 1;
            this.bunifuShadowPanel1.Controls.Add(this.bunifuPanel1);
            this.bunifuShadowPanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuShadowPanel1.FillStyle = Bunifu.UI.WinForms.BunifuShadowPanel.FillStyles.Solid;
            this.bunifuShadowPanel1.GradientMode = Bunifu.UI.WinForms.BunifuShadowPanel.GradientModes.Vertical;
            this.bunifuShadowPanel1.Location = new System.Drawing.Point(0, 0);
            this.bunifuShadowPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuShadowPanel1.Name = "bunifuShadowPanel1";
            this.bunifuShadowPanel1.PanelColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel1.PanelColor2 = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel1.ShadowColor = System.Drawing.Color.DarkGray;
            this.bunifuShadowPanel1.ShadowDept = 2;
            this.bunifuShadowPanel1.ShadowDepth = 5;
            this.bunifuShadowPanel1.ShadowStyle = Bunifu.UI.WinForms.BunifuShadowPanel.ShadowStyles.Surrounded;
            this.bunifuShadowPanel1.ShadowTopLeftVisible = false;
            this.bunifuShadowPanel1.Size = new System.Drawing.Size(269, 866);
            this.bunifuShadowPanel1.Style = Bunifu.UI.WinForms.BunifuShadowPanel.BevelStyles.Flat;
            this.bunifuShadowPanel1.TabIndex = 3;
            // 
            // bunifuPanel1
            // 
            this.bunifuPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuPanel1.AutoScroll = true;
            this.bunifuPanel1.BackgroundColor = System.Drawing.Color.Transparent;
            this.bunifuPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuPanel1.BackgroundImage")));
            this.bunifuPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuPanel1.BorderColor = System.Drawing.Color.MidnightBlue;
            this.bunifuPanel1.BorderRadius = 3;
            this.bunifuPanel1.BorderThickness = 1;
            this.bunifuPanel1.Controls.Add(this.manage_stock_subPanel);
            this.bunifuPanel1.Controls.Add(this.button6);
            this.bunifuPanel1.Controls.Add(this.manage_pro_subPanel);
            this.bunifuPanel1.Controls.Add(this.button2);
            this.bunifuPanel1.Controls.Add(this.manage_manu_subPanel);
            this.bunifuPanel1.Controls.Add(this.button1);
            this.bunifuPanel1.Controls.Add(this.manage_emp_subPanel);
            this.bunifuPanel1.Controls.Add(this.button3);
            this.bunifuPanel1.Controls.Add(this.button5);
            this.bunifuPanel1.Controls.Add(this.button4);
            this.bunifuPanel1.Controls.Add(this.bunifuPanel2);
            this.bunifuPanel1.Location = new System.Drawing.Point(0, 4);
            this.bunifuPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuPanel1.Name = "bunifuPanel1";
            this.bunifuPanel1.ShowBorders = true;
            this.bunifuPanel1.Size = new System.Drawing.Size(267, 859);
            this.bunifuPanel1.TabIndex = 0;
            // 
            // manage_stock_subPanel
            // 
            this.manage_stock_subPanel.Controls.Add(this.bunifuButton7);
            this.manage_stock_subPanel.Controls.Add(this.bunifuButton8);
            this.manage_stock_subPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.manage_stock_subPanel.Location = new System.Drawing.Point(0, 729);
            this.manage_stock_subPanel.Margin = new System.Windows.Forms.Padding(4);
            this.manage_stock_subPanel.Name = "manage_stock_subPanel";
            this.manage_stock_subPanel.Size = new System.Drawing.Size(267, 89);
            this.manage_stock_subPanel.TabIndex = 29;
            // 
            // bunifuButton7
            // 
            this.bunifuButton7.AllowAnimations = true;
            this.bunifuButton7.AllowMouseEffects = true;
            this.bunifuButton7.AllowToggling = false;
            this.bunifuButton7.AnimationSpeed = 200;
            this.bunifuButton7.AutoGenerateColors = false;
            this.bunifuButton7.AutoRoundBorders = false;
            this.bunifuButton7.AutoSizeLeftIcon = true;
            this.bunifuButton7.AutoSizeRightIcon = true;
            this.bunifuButton7.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton7.BackColor1 = System.Drawing.Color.Transparent;
            this.bunifuButton7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton7.BackgroundImage")));
            this.bunifuButton7.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton7.ButtonText = "View/Update Shippers";
            this.bunifuButton7.ButtonTextMarginLeft = 0;
            this.bunifuButton7.ColorContrastOnClick = 45;
            this.bunifuButton7.ColorContrastOnHover = 45;
            this.bunifuButton7.Cursor = System.Windows.Forms.Cursors.Default;
            borderEdges1.BottomLeft = true;
            borderEdges1.BottomRight = true;
            borderEdges1.TopLeft = true;
            borderEdges1.TopRight = true;
            this.bunifuButton7.CustomizableEdges = borderEdges1;
            this.bunifuButton7.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton7.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuButton7.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton7.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton7.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuButton7.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton7.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuButton7.ForeColor = System.Drawing.Color.White;
            this.bunifuButton7.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuButton7.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuButton7.IconLeftPadding = new System.Windows.Forms.Padding(11, 3, 3, 3);
            this.bunifuButton7.IconMarginLeft = 11;
            this.bunifuButton7.IconPadding = 10;
            this.bunifuButton7.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bunifuButton7.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuButton7.IconRightPadding = new System.Windows.Forms.Padding(3, 3, 7, 3);
            this.bunifuButton7.IconSize = 25;
            this.bunifuButton7.IdleBorderColor = System.Drawing.Color.MidnightBlue;
            this.bunifuButton7.IdleBorderRadius = 1;
            this.bunifuButton7.IdleBorderThickness = 1;
            this.bunifuButton7.IdleFillColor = System.Drawing.Color.Transparent;
            this.bunifuButton7.IdleIconLeftImage = null;
            this.bunifuButton7.IdleIconRightImage = null;
            this.bunifuButton7.IndicateFocus = true;
            this.bunifuButton7.Location = new System.Drawing.Point(0, 46);
            this.bunifuButton7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuButton7.Name = "bunifuButton7";
            this.bunifuButton7.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuButton7.OnDisabledState.BorderRadius = 1;
            this.bunifuButton7.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton7.OnDisabledState.BorderThickness = 1;
            this.bunifuButton7.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton7.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton7.OnDisabledState.IconLeftImage = null;
            this.bunifuButton7.OnDisabledState.IconRightImage = null;
            this.bunifuButton7.onHoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.bunifuButton7.onHoverState.BorderRadius = 1;
            this.bunifuButton7.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton7.onHoverState.BorderThickness = 1;
            this.bunifuButton7.onHoverState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.bunifuButton7.onHoverState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton7.onHoverState.IconLeftImage = null;
            this.bunifuButton7.onHoverState.IconRightImage = null;
            this.bunifuButton7.OnIdleState.BorderColor = System.Drawing.Color.MidnightBlue;
            this.bunifuButton7.OnIdleState.BorderRadius = 1;
            this.bunifuButton7.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton7.OnIdleState.BorderThickness = 1;
            this.bunifuButton7.OnIdleState.FillColor = System.Drawing.Color.Transparent;
            this.bunifuButton7.OnIdleState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton7.OnIdleState.IconLeftImage = null;
            this.bunifuButton7.OnIdleState.IconRightImage = null;
            this.bunifuButton7.OnPressedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            this.bunifuButton7.OnPressedState.BorderRadius = 1;
            this.bunifuButton7.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton7.OnPressedState.BorderThickness = 1;
            this.bunifuButton7.OnPressedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            this.bunifuButton7.OnPressedState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton7.OnPressedState.IconLeftImage = null;
            this.bunifuButton7.OnPressedState.IconRightImage = null;
            this.bunifuButton7.Size = new System.Drawing.Size(267, 38);
            this.bunifuButton7.TabIndex = 10;
            this.bunifuButton7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuButton7.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.bunifuButton7.TextMarginLeft = 0;
            this.bunifuButton7.TextPadding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.bunifuButton7.UseDefaultRadiusAndThickness = true;
            this.bunifuButton7.Click += new System.EventHandler(this.bunifuButton7_Click_1);
            // 
            // bunifuButton8
            // 
            this.bunifuButton8.AllowAnimations = true;
            this.bunifuButton8.AllowMouseEffects = true;
            this.bunifuButton8.AllowToggling = false;
            this.bunifuButton8.AnimationSpeed = 200;
            this.bunifuButton8.AutoGenerateColors = false;
            this.bunifuButton8.AutoRoundBorders = false;
            this.bunifuButton8.AutoSizeLeftIcon = true;
            this.bunifuButton8.AutoSizeRightIcon = true;
            this.bunifuButton8.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton8.BackColor1 = System.Drawing.Color.Transparent;
            this.bunifuButton8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton8.BackgroundImage")));
            this.bunifuButton8.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton8.ButtonText = "Add Shipper";
            this.bunifuButton8.ButtonTextMarginLeft = 0;
            this.bunifuButton8.ColorContrastOnClick = 45;
            this.bunifuButton8.ColorContrastOnHover = 45;
            this.bunifuButton8.Cursor = System.Windows.Forms.Cursors.Default;
            borderEdges2.BottomLeft = true;
            borderEdges2.BottomRight = true;
            borderEdges2.TopLeft = true;
            borderEdges2.TopRight = true;
            this.bunifuButton8.CustomizableEdges = borderEdges2;
            this.bunifuButton8.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton8.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuButton8.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton8.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton8.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuButton8.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton8.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuButton8.ForeColor = System.Drawing.Color.White;
            this.bunifuButton8.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuButton8.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuButton8.IconLeftPadding = new System.Windows.Forms.Padding(11, 3, 3, 3);
            this.bunifuButton8.IconMarginLeft = 11;
            this.bunifuButton8.IconPadding = 10;
            this.bunifuButton8.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bunifuButton8.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuButton8.IconRightPadding = new System.Windows.Forms.Padding(3, 3, 7, 3);
            this.bunifuButton8.IconSize = 25;
            this.bunifuButton8.IdleBorderColor = System.Drawing.Color.MidnightBlue;
            this.bunifuButton8.IdleBorderRadius = 1;
            this.bunifuButton8.IdleBorderThickness = 1;
            this.bunifuButton8.IdleFillColor = System.Drawing.Color.Transparent;
            this.bunifuButton8.IdleIconLeftImage = null;
            this.bunifuButton8.IdleIconRightImage = null;
            this.bunifuButton8.IndicateFocus = true;
            this.bunifuButton8.Location = new System.Drawing.Point(0, 0);
            this.bunifuButton8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuButton8.Name = "bunifuButton8";
            this.bunifuButton8.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuButton8.OnDisabledState.BorderRadius = 1;
            this.bunifuButton8.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton8.OnDisabledState.BorderThickness = 1;
            this.bunifuButton8.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton8.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton8.OnDisabledState.IconLeftImage = null;
            this.bunifuButton8.OnDisabledState.IconRightImage = null;
            this.bunifuButton8.onHoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.bunifuButton8.onHoverState.BorderRadius = 1;
            this.bunifuButton8.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton8.onHoverState.BorderThickness = 1;
            this.bunifuButton8.onHoverState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.bunifuButton8.onHoverState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton8.onHoverState.IconLeftImage = null;
            this.bunifuButton8.onHoverState.IconRightImage = null;
            this.bunifuButton8.OnIdleState.BorderColor = System.Drawing.Color.MidnightBlue;
            this.bunifuButton8.OnIdleState.BorderRadius = 1;
            this.bunifuButton8.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton8.OnIdleState.BorderThickness = 1;
            this.bunifuButton8.OnIdleState.FillColor = System.Drawing.Color.Transparent;
            this.bunifuButton8.OnIdleState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton8.OnIdleState.IconLeftImage = null;
            this.bunifuButton8.OnIdleState.IconRightImage = null;
            this.bunifuButton8.OnPressedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            this.bunifuButton8.OnPressedState.BorderRadius = 1;
            this.bunifuButton8.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton8.OnPressedState.BorderThickness = 1;
            this.bunifuButton8.OnPressedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            this.bunifuButton8.OnPressedState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton8.OnPressedState.IconLeftImage = null;
            this.bunifuButton8.OnPressedState.IconRightImage = null;
            this.bunifuButton8.Size = new System.Drawing.Size(267, 46);
            this.bunifuButton8.TabIndex = 9;
            this.bunifuButton8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuButton8.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.bunifuButton8.TextMarginLeft = 0;
            this.bunifuButton8.TextPadding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.bunifuButton8.UseDefaultRadiusAndThickness = true;
            this.bunifuButton8.Click += new System.EventHandler(this.bunifuButton8_Click_2);
            // 
            // button6
            // 
            this.button6.Dock = System.Windows.Forms.DockStyle.Top;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SkyBlue;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.Location = new System.Drawing.Point(0, 681);
            this.button6.Margin = new System.Windows.Forms.Padding(4);
            this.button6.Name = "button6";
            this.button6.Padding = new System.Windows.Forms.Padding(13, 0, 0, 0);
            this.button6.Size = new System.Drawing.Size(267, 48);
            this.button6.TabIndex = 28;
            this.button6.Text = "Manage Shippers";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // manage_pro_subPanel
            // 
            this.manage_pro_subPanel.Controls.Add(this.bunifuButton5);
            this.manage_pro_subPanel.Controls.Add(this.bunifuButton6);
            this.manage_pro_subPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.manage_pro_subPanel.Location = new System.Drawing.Point(0, 592);
            this.manage_pro_subPanel.Margin = new System.Windows.Forms.Padding(4);
            this.manage_pro_subPanel.Name = "manage_pro_subPanel";
            this.manage_pro_subPanel.Size = new System.Drawing.Size(267, 89);
            this.manage_pro_subPanel.TabIndex = 27;
            // 
            // bunifuButton5
            // 
            this.bunifuButton5.AllowAnimations = true;
            this.bunifuButton5.AllowMouseEffects = true;
            this.bunifuButton5.AllowToggling = false;
            this.bunifuButton5.AnimationSpeed = 200;
            this.bunifuButton5.AutoGenerateColors = false;
            this.bunifuButton5.AutoRoundBorders = false;
            this.bunifuButton5.AutoSizeLeftIcon = true;
            this.bunifuButton5.AutoSizeRightIcon = true;
            this.bunifuButton5.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton5.BackColor1 = System.Drawing.Color.Transparent;
            this.bunifuButton5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton5.BackgroundImage")));
            this.bunifuButton5.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton5.ButtonText = "View/Update Product";
            this.bunifuButton5.ButtonTextMarginLeft = 0;
            this.bunifuButton5.ColorContrastOnClick = 45;
            this.bunifuButton5.ColorContrastOnHover = 45;
            this.bunifuButton5.Cursor = System.Windows.Forms.Cursors.Default;
            borderEdges3.BottomLeft = true;
            borderEdges3.BottomRight = true;
            borderEdges3.TopLeft = true;
            borderEdges3.TopRight = true;
            this.bunifuButton5.CustomizableEdges = borderEdges3;
            this.bunifuButton5.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton5.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuButton5.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton5.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton5.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuButton5.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton5.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuButton5.ForeColor = System.Drawing.Color.White;
            this.bunifuButton5.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuButton5.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuButton5.IconLeftPadding = new System.Windows.Forms.Padding(11, 3, 3, 3);
            this.bunifuButton5.IconMarginLeft = 11;
            this.bunifuButton5.IconPadding = 10;
            this.bunifuButton5.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bunifuButton5.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuButton5.IconRightPadding = new System.Windows.Forms.Padding(3, 3, 7, 3);
            this.bunifuButton5.IconSize = 25;
            this.bunifuButton5.IdleBorderColor = System.Drawing.Color.MidnightBlue;
            this.bunifuButton5.IdleBorderRadius = 1;
            this.bunifuButton5.IdleBorderThickness = 1;
            this.bunifuButton5.IdleFillColor = System.Drawing.Color.Transparent;
            this.bunifuButton5.IdleIconLeftImage = null;
            this.bunifuButton5.IdleIconRightImage = null;
            this.bunifuButton5.IndicateFocus = true;
            this.bunifuButton5.Location = new System.Drawing.Point(0, 46);
            this.bunifuButton5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuButton5.Name = "bunifuButton5";
            this.bunifuButton5.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuButton5.OnDisabledState.BorderRadius = 1;
            this.bunifuButton5.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton5.OnDisabledState.BorderThickness = 1;
            this.bunifuButton5.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton5.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton5.OnDisabledState.IconLeftImage = null;
            this.bunifuButton5.OnDisabledState.IconRightImage = null;
            this.bunifuButton5.onHoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.bunifuButton5.onHoverState.BorderRadius = 1;
            this.bunifuButton5.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton5.onHoverState.BorderThickness = 1;
            this.bunifuButton5.onHoverState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.bunifuButton5.onHoverState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton5.onHoverState.IconLeftImage = null;
            this.bunifuButton5.onHoverState.IconRightImage = null;
            this.bunifuButton5.OnIdleState.BorderColor = System.Drawing.Color.MidnightBlue;
            this.bunifuButton5.OnIdleState.BorderRadius = 1;
            this.bunifuButton5.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton5.OnIdleState.BorderThickness = 1;
            this.bunifuButton5.OnIdleState.FillColor = System.Drawing.Color.Transparent;
            this.bunifuButton5.OnIdleState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton5.OnIdleState.IconLeftImage = null;
            this.bunifuButton5.OnIdleState.IconRightImage = null;
            this.bunifuButton5.OnPressedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            this.bunifuButton5.OnPressedState.BorderRadius = 1;
            this.bunifuButton5.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton5.OnPressedState.BorderThickness = 1;
            this.bunifuButton5.OnPressedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            this.bunifuButton5.OnPressedState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton5.OnPressedState.IconLeftImage = null;
            this.bunifuButton5.OnPressedState.IconRightImage = null;
            this.bunifuButton5.Size = new System.Drawing.Size(267, 38);
            this.bunifuButton5.TabIndex = 10;
            this.bunifuButton5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuButton5.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.bunifuButton5.TextMarginLeft = 0;
            this.bunifuButton5.TextPadding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.bunifuButton5.UseDefaultRadiusAndThickness = true;
            this.bunifuButton5.Click += new System.EventHandler(this.bunifuButton5_Click_2);
            // 
            // bunifuButton6
            // 
            this.bunifuButton6.AllowAnimations = true;
            this.bunifuButton6.AllowMouseEffects = true;
            this.bunifuButton6.AllowToggling = false;
            this.bunifuButton6.AnimationSpeed = 200;
            this.bunifuButton6.AutoGenerateColors = false;
            this.bunifuButton6.AutoRoundBorders = false;
            this.bunifuButton6.AutoSizeLeftIcon = true;
            this.bunifuButton6.AutoSizeRightIcon = true;
            this.bunifuButton6.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton6.BackColor1 = System.Drawing.Color.Transparent;
            this.bunifuButton6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton6.BackgroundImage")));
            this.bunifuButton6.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton6.ButtonText = "Add Product";
            this.bunifuButton6.ButtonTextMarginLeft = 0;
            this.bunifuButton6.ColorContrastOnClick = 45;
            this.bunifuButton6.ColorContrastOnHover = 45;
            this.bunifuButton6.Cursor = System.Windows.Forms.Cursors.Default;
            borderEdges4.BottomLeft = true;
            borderEdges4.BottomRight = true;
            borderEdges4.TopLeft = true;
            borderEdges4.TopRight = true;
            this.bunifuButton6.CustomizableEdges = borderEdges4;
            this.bunifuButton6.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton6.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuButton6.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton6.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton6.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuButton6.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton6.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuButton6.ForeColor = System.Drawing.Color.White;
            this.bunifuButton6.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuButton6.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuButton6.IconLeftPadding = new System.Windows.Forms.Padding(11, 3, 3, 3);
            this.bunifuButton6.IconMarginLeft = 11;
            this.bunifuButton6.IconPadding = 10;
            this.bunifuButton6.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bunifuButton6.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuButton6.IconRightPadding = new System.Windows.Forms.Padding(3, 3, 7, 3);
            this.bunifuButton6.IconSize = 25;
            this.bunifuButton6.IdleBorderColor = System.Drawing.Color.MidnightBlue;
            this.bunifuButton6.IdleBorderRadius = 1;
            this.bunifuButton6.IdleBorderThickness = 1;
            this.bunifuButton6.IdleFillColor = System.Drawing.Color.Transparent;
            this.bunifuButton6.IdleIconLeftImage = null;
            this.bunifuButton6.IdleIconRightImage = null;
            this.bunifuButton6.IndicateFocus = true;
            this.bunifuButton6.Location = new System.Drawing.Point(0, 0);
            this.bunifuButton6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuButton6.Name = "bunifuButton6";
            this.bunifuButton6.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuButton6.OnDisabledState.BorderRadius = 1;
            this.bunifuButton6.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton6.OnDisabledState.BorderThickness = 1;
            this.bunifuButton6.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton6.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton6.OnDisabledState.IconLeftImage = null;
            this.bunifuButton6.OnDisabledState.IconRightImage = null;
            this.bunifuButton6.onHoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.bunifuButton6.onHoverState.BorderRadius = 1;
            this.bunifuButton6.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton6.onHoverState.BorderThickness = 1;
            this.bunifuButton6.onHoverState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.bunifuButton6.onHoverState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton6.onHoverState.IconLeftImage = null;
            this.bunifuButton6.onHoverState.IconRightImage = null;
            this.bunifuButton6.OnIdleState.BorderColor = System.Drawing.Color.MidnightBlue;
            this.bunifuButton6.OnIdleState.BorderRadius = 1;
            this.bunifuButton6.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton6.OnIdleState.BorderThickness = 1;
            this.bunifuButton6.OnIdleState.FillColor = System.Drawing.Color.Transparent;
            this.bunifuButton6.OnIdleState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton6.OnIdleState.IconLeftImage = null;
            this.bunifuButton6.OnIdleState.IconRightImage = null;
            this.bunifuButton6.OnPressedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            this.bunifuButton6.OnPressedState.BorderRadius = 1;
            this.bunifuButton6.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton6.OnPressedState.BorderThickness = 1;
            this.bunifuButton6.OnPressedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            this.bunifuButton6.OnPressedState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton6.OnPressedState.IconLeftImage = null;
            this.bunifuButton6.OnPressedState.IconRightImage = null;
            this.bunifuButton6.Size = new System.Drawing.Size(267, 46);
            this.bunifuButton6.TabIndex = 9;
            this.bunifuButton6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuButton6.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.bunifuButton6.TextMarginLeft = 0;
            this.bunifuButton6.TextPadding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.bunifuButton6.UseDefaultRadiusAndThickness = true;
            this.bunifuButton6.Click += new System.EventHandler(this.bunifuButton6_Click);
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SkyBlue;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(0, 544);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Padding = new System.Windows.Forms.Padding(13, 0, 0, 0);
            this.button2.Size = new System.Drawing.Size(267, 48);
            this.button2.TabIndex = 26;
            this.button2.Text = "Manage Products";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // manage_manu_subPanel
            // 
            this.manage_manu_subPanel.Controls.Add(this.bunifuButton3);
            this.manage_manu_subPanel.Controls.Add(this.bunifuButton4);
            this.manage_manu_subPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.manage_manu_subPanel.Location = new System.Drawing.Point(0, 455);
            this.manage_manu_subPanel.Margin = new System.Windows.Forms.Padding(4);
            this.manage_manu_subPanel.Name = "manage_manu_subPanel";
            this.manage_manu_subPanel.Size = new System.Drawing.Size(267, 89);
            this.manage_manu_subPanel.TabIndex = 25;
            // 
            // bunifuButton3
            // 
            this.bunifuButton3.AllowAnimations = true;
            this.bunifuButton3.AllowMouseEffects = true;
            this.bunifuButton3.AllowToggling = false;
            this.bunifuButton3.AnimationSpeed = 200;
            this.bunifuButton3.AutoGenerateColors = false;
            this.bunifuButton3.AutoRoundBorders = false;
            this.bunifuButton3.AutoSizeLeftIcon = true;
            this.bunifuButton3.AutoSizeRightIcon = true;
            this.bunifuButton3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton3.BackColor1 = System.Drawing.Color.Transparent;
            this.bunifuButton3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton3.BackgroundImage")));
            this.bunifuButton3.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton3.ButtonText = "View/Update Manufacturer";
            this.bunifuButton3.ButtonTextMarginLeft = 0;
            this.bunifuButton3.ColorContrastOnClick = 45;
            this.bunifuButton3.ColorContrastOnHover = 45;
            this.bunifuButton3.Cursor = System.Windows.Forms.Cursors.Default;
            borderEdges5.BottomLeft = true;
            borderEdges5.BottomRight = true;
            borderEdges5.TopLeft = true;
            borderEdges5.TopRight = true;
            this.bunifuButton3.CustomizableEdges = borderEdges5;
            this.bunifuButton3.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton3.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuButton3.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton3.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton3.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuButton3.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuButton3.ForeColor = System.Drawing.Color.White;
            this.bunifuButton3.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuButton3.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuButton3.IconLeftPadding = new System.Windows.Forms.Padding(11, 3, 3, 3);
            this.bunifuButton3.IconMarginLeft = 11;
            this.bunifuButton3.IconPadding = 10;
            this.bunifuButton3.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bunifuButton3.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuButton3.IconRightPadding = new System.Windows.Forms.Padding(3, 3, 7, 3);
            this.bunifuButton3.IconSize = 25;
            this.bunifuButton3.IdleBorderColor = System.Drawing.Color.MidnightBlue;
            this.bunifuButton3.IdleBorderRadius = 1;
            this.bunifuButton3.IdleBorderThickness = 1;
            this.bunifuButton3.IdleFillColor = System.Drawing.Color.Transparent;
            this.bunifuButton3.IdleIconLeftImage = null;
            this.bunifuButton3.IdleIconRightImage = null;
            this.bunifuButton3.IndicateFocus = true;
            this.bunifuButton3.Location = new System.Drawing.Point(0, 46);
            this.bunifuButton3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuButton3.Name = "bunifuButton3";
            this.bunifuButton3.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuButton3.OnDisabledState.BorderRadius = 1;
            this.bunifuButton3.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton3.OnDisabledState.BorderThickness = 1;
            this.bunifuButton3.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton3.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton3.OnDisabledState.IconLeftImage = null;
            this.bunifuButton3.OnDisabledState.IconRightImage = null;
            this.bunifuButton3.onHoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.bunifuButton3.onHoverState.BorderRadius = 1;
            this.bunifuButton3.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton3.onHoverState.BorderThickness = 1;
            this.bunifuButton3.onHoverState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.bunifuButton3.onHoverState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton3.onHoverState.IconLeftImage = null;
            this.bunifuButton3.onHoverState.IconRightImage = null;
            this.bunifuButton3.OnIdleState.BorderColor = System.Drawing.Color.MidnightBlue;
            this.bunifuButton3.OnIdleState.BorderRadius = 1;
            this.bunifuButton3.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton3.OnIdleState.BorderThickness = 1;
            this.bunifuButton3.OnIdleState.FillColor = System.Drawing.Color.Transparent;
            this.bunifuButton3.OnIdleState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton3.OnIdleState.IconLeftImage = null;
            this.bunifuButton3.OnIdleState.IconRightImage = null;
            this.bunifuButton3.OnPressedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            this.bunifuButton3.OnPressedState.BorderRadius = 1;
            this.bunifuButton3.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton3.OnPressedState.BorderThickness = 1;
            this.bunifuButton3.OnPressedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            this.bunifuButton3.OnPressedState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton3.OnPressedState.IconLeftImage = null;
            this.bunifuButton3.OnPressedState.IconRightImage = null;
            this.bunifuButton3.Size = new System.Drawing.Size(267, 38);
            this.bunifuButton3.TabIndex = 10;
            this.bunifuButton3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuButton3.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.bunifuButton3.TextMarginLeft = 0;
            this.bunifuButton3.TextPadding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.bunifuButton3.UseDefaultRadiusAndThickness = true;
            this.bunifuButton3.Click += new System.EventHandler(this.bunifuButton3_Click_4);
            // 
            // bunifuButton4
            // 
            this.bunifuButton4.AllowAnimations = true;
            this.bunifuButton4.AllowMouseEffects = true;
            this.bunifuButton4.AllowToggling = false;
            this.bunifuButton4.AnimationSpeed = 200;
            this.bunifuButton4.AutoGenerateColors = false;
            this.bunifuButton4.AutoRoundBorders = false;
            this.bunifuButton4.AutoSizeLeftIcon = true;
            this.bunifuButton4.AutoSizeRightIcon = true;
            this.bunifuButton4.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton4.BackColor1 = System.Drawing.Color.Transparent;
            this.bunifuButton4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton4.BackgroundImage")));
            this.bunifuButton4.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton4.ButtonText = "Add Manufacturer";
            this.bunifuButton4.ButtonTextMarginLeft = 0;
            this.bunifuButton4.ColorContrastOnClick = 45;
            this.bunifuButton4.ColorContrastOnHover = 45;
            this.bunifuButton4.Cursor = System.Windows.Forms.Cursors.Default;
            borderEdges6.BottomLeft = true;
            borderEdges6.BottomRight = true;
            borderEdges6.TopLeft = true;
            borderEdges6.TopRight = true;
            this.bunifuButton4.CustomizableEdges = borderEdges6;
            this.bunifuButton4.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton4.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuButton4.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton4.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton4.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuButton4.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton4.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuButton4.ForeColor = System.Drawing.Color.White;
            this.bunifuButton4.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuButton4.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuButton4.IconLeftPadding = new System.Windows.Forms.Padding(11, 3, 3, 3);
            this.bunifuButton4.IconMarginLeft = 11;
            this.bunifuButton4.IconPadding = 10;
            this.bunifuButton4.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bunifuButton4.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuButton4.IconRightPadding = new System.Windows.Forms.Padding(3, 3, 7, 3);
            this.bunifuButton4.IconSize = 25;
            this.bunifuButton4.IdleBorderColor = System.Drawing.Color.MidnightBlue;
            this.bunifuButton4.IdleBorderRadius = 1;
            this.bunifuButton4.IdleBorderThickness = 1;
            this.bunifuButton4.IdleFillColor = System.Drawing.Color.Transparent;
            this.bunifuButton4.IdleIconLeftImage = null;
            this.bunifuButton4.IdleIconRightImage = null;
            this.bunifuButton4.IndicateFocus = true;
            this.bunifuButton4.Location = new System.Drawing.Point(0, 0);
            this.bunifuButton4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuButton4.Name = "bunifuButton4";
            this.bunifuButton4.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuButton4.OnDisabledState.BorderRadius = 1;
            this.bunifuButton4.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton4.OnDisabledState.BorderThickness = 1;
            this.bunifuButton4.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton4.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton4.OnDisabledState.IconLeftImage = null;
            this.bunifuButton4.OnDisabledState.IconRightImage = null;
            this.bunifuButton4.onHoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.bunifuButton4.onHoverState.BorderRadius = 1;
            this.bunifuButton4.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton4.onHoverState.BorderThickness = 1;
            this.bunifuButton4.onHoverState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.bunifuButton4.onHoverState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton4.onHoverState.IconLeftImage = null;
            this.bunifuButton4.onHoverState.IconRightImage = null;
            this.bunifuButton4.OnIdleState.BorderColor = System.Drawing.Color.MidnightBlue;
            this.bunifuButton4.OnIdleState.BorderRadius = 1;
            this.bunifuButton4.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton4.OnIdleState.BorderThickness = 1;
            this.bunifuButton4.OnIdleState.FillColor = System.Drawing.Color.Transparent;
            this.bunifuButton4.OnIdleState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton4.OnIdleState.IconLeftImage = null;
            this.bunifuButton4.OnIdleState.IconRightImage = null;
            this.bunifuButton4.OnPressedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            this.bunifuButton4.OnPressedState.BorderRadius = 1;
            this.bunifuButton4.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton4.OnPressedState.BorderThickness = 1;
            this.bunifuButton4.OnPressedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            this.bunifuButton4.OnPressedState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton4.OnPressedState.IconLeftImage = null;
            this.bunifuButton4.OnPressedState.IconRightImage = null;
            this.bunifuButton4.Size = new System.Drawing.Size(267, 46);
            this.bunifuButton4.TabIndex = 9;
            this.bunifuButton4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuButton4.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.bunifuButton4.TextMarginLeft = 0;
            this.bunifuButton4.TextPadding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.bunifuButton4.UseDefaultRadiusAndThickness = true;
            this.bunifuButton4.Click += new System.EventHandler(this.bunifuButton4_Click_2);
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SkyBlue;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(0, 407);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Padding = new System.Windows.Forms.Padding(9, 0, 0, 0);
            this.button1.Size = new System.Drawing.Size(267, 48);
            this.button1.TabIndex = 24;
            this.button1.Text = "Manage Manufacturer";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // manage_emp_subPanel
            // 
            this.manage_emp_subPanel.Controls.Add(this.bunifuButton1);
            this.manage_emp_subPanel.Controls.Add(this.bunifuButton2);
            this.manage_emp_subPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.manage_emp_subPanel.Location = new System.Drawing.Point(0, 304);
            this.manage_emp_subPanel.Margin = new System.Windows.Forms.Padding(4);
            this.manage_emp_subPanel.Name = "manage_emp_subPanel";
            this.manage_emp_subPanel.Size = new System.Drawing.Size(267, 103);
            this.manage_emp_subPanel.TabIndex = 23;
            // 
            // bunifuButton1
            // 
            this.bunifuButton1.AllowAnimations = true;
            this.bunifuButton1.AllowMouseEffects = true;
            this.bunifuButton1.AllowToggling = false;
            this.bunifuButton1.AnimationSpeed = 200;
            this.bunifuButton1.AutoGenerateColors = false;
            this.bunifuButton1.AutoRoundBorders = false;
            this.bunifuButton1.AutoSizeLeftIcon = true;
            this.bunifuButton1.AutoSizeRightIcon = true;
            this.bunifuButton1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton1.BackColor1 = System.Drawing.Color.Transparent;
            this.bunifuButton1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton1.BackgroundImage")));
            this.bunifuButton1.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton1.ButtonText = "View/Update Employee";
            this.bunifuButton1.ButtonTextMarginLeft = 0;
            this.bunifuButton1.ColorContrastOnClick = 45;
            this.bunifuButton1.ColorContrastOnHover = 45;
            this.bunifuButton1.Cursor = System.Windows.Forms.Cursors.Default;
            borderEdges7.BottomLeft = true;
            borderEdges7.BottomRight = true;
            borderEdges7.TopLeft = true;
            borderEdges7.TopRight = true;
            this.bunifuButton1.CustomizableEdges = borderEdges7;
            this.bunifuButton1.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton1.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuButton1.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton1.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton1.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuButton1.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuButton1.ForeColor = System.Drawing.Color.White;
            this.bunifuButton1.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuButton1.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuButton1.IconLeftPadding = new System.Windows.Forms.Padding(11, 3, 3, 3);
            this.bunifuButton1.IconMarginLeft = 11;
            this.bunifuButton1.IconPadding = 10;
            this.bunifuButton1.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bunifuButton1.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuButton1.IconRightPadding = new System.Windows.Forms.Padding(3, 3, 7, 3);
            this.bunifuButton1.IconSize = 25;
            this.bunifuButton1.IdleBorderColor = System.Drawing.Color.MidnightBlue;
            this.bunifuButton1.IdleBorderRadius = 1;
            this.bunifuButton1.IdleBorderThickness = 1;
            this.bunifuButton1.IdleFillColor = System.Drawing.Color.Transparent;
            this.bunifuButton1.IdleIconLeftImage = null;
            this.bunifuButton1.IdleIconRightImage = null;
            this.bunifuButton1.IndicateFocus = true;
            this.bunifuButton1.Location = new System.Drawing.Point(0, 50);
            this.bunifuButton1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuButton1.Name = "bunifuButton1";
            this.bunifuButton1.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuButton1.OnDisabledState.BorderRadius = 1;
            this.bunifuButton1.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton1.OnDisabledState.BorderThickness = 1;
            this.bunifuButton1.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton1.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton1.OnDisabledState.IconLeftImage = null;
            this.bunifuButton1.OnDisabledState.IconRightImage = null;
            this.bunifuButton1.onHoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.bunifuButton1.onHoverState.BorderRadius = 1;
            this.bunifuButton1.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton1.onHoverState.BorderThickness = 1;
            this.bunifuButton1.onHoverState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.bunifuButton1.onHoverState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton1.onHoverState.IconLeftImage = null;
            this.bunifuButton1.onHoverState.IconRightImage = null;
            this.bunifuButton1.OnIdleState.BorderColor = System.Drawing.Color.MidnightBlue;
            this.bunifuButton1.OnIdleState.BorderRadius = 1;
            this.bunifuButton1.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton1.OnIdleState.BorderThickness = 1;
            this.bunifuButton1.OnIdleState.FillColor = System.Drawing.Color.Transparent;
            this.bunifuButton1.OnIdleState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton1.OnIdleState.IconLeftImage = null;
            this.bunifuButton1.OnIdleState.IconRightImage = null;
            this.bunifuButton1.OnPressedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            this.bunifuButton1.OnPressedState.BorderRadius = 1;
            this.bunifuButton1.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton1.OnPressedState.BorderThickness = 1;
            this.bunifuButton1.OnPressedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            this.bunifuButton1.OnPressedState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton1.OnPressedState.IconLeftImage = null;
            this.bunifuButton1.OnPressedState.IconRightImage = null;
            this.bunifuButton1.Size = new System.Drawing.Size(267, 50);
            this.bunifuButton1.TabIndex = 10;
            this.bunifuButton1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuButton1.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.bunifuButton1.TextMarginLeft = 0;
            this.bunifuButton1.TextPadding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.bunifuButton1.UseDefaultRadiusAndThickness = true;
            // 
            // bunifuButton2
            // 
            this.bunifuButton2.AllowAnimations = true;
            this.bunifuButton2.AllowMouseEffects = true;
            this.bunifuButton2.AllowToggling = false;
            this.bunifuButton2.AnimationSpeed = 200;
            this.bunifuButton2.AutoGenerateColors = false;
            this.bunifuButton2.AutoRoundBorders = false;
            this.bunifuButton2.AutoSizeLeftIcon = true;
            this.bunifuButton2.AutoSizeRightIcon = true;
            this.bunifuButton2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton2.BackColor1 = System.Drawing.Color.Transparent;
            this.bunifuButton2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton2.BackgroundImage")));
            this.bunifuButton2.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton2.ButtonText = "Add Employee";
            this.bunifuButton2.ButtonTextMarginLeft = 0;
            this.bunifuButton2.ColorContrastOnClick = 45;
            this.bunifuButton2.ColorContrastOnHover = 45;
            this.bunifuButton2.Cursor = System.Windows.Forms.Cursors.Default;
            borderEdges8.BottomLeft = true;
            borderEdges8.BottomRight = true;
            borderEdges8.TopLeft = true;
            borderEdges8.TopRight = true;
            this.bunifuButton2.CustomizableEdges = borderEdges8;
            this.bunifuButton2.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton2.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuButton2.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton2.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton2.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuButton2.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuButton2.ForeColor = System.Drawing.Color.White;
            this.bunifuButton2.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuButton2.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuButton2.IconLeftPadding = new System.Windows.Forms.Padding(11, 3, 3, 3);
            this.bunifuButton2.IconMarginLeft = 11;
            this.bunifuButton2.IconPadding = 10;
            this.bunifuButton2.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bunifuButton2.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuButton2.IconRightPadding = new System.Windows.Forms.Padding(3, 3, 7, 3);
            this.bunifuButton2.IconSize = 25;
            this.bunifuButton2.IdleBorderColor = System.Drawing.Color.MidnightBlue;
            this.bunifuButton2.IdleBorderRadius = 1;
            this.bunifuButton2.IdleBorderThickness = 1;
            this.bunifuButton2.IdleFillColor = System.Drawing.Color.Transparent;
            this.bunifuButton2.IdleIconLeftImage = null;
            this.bunifuButton2.IdleIconRightImage = null;
            this.bunifuButton2.IndicateFocus = true;
            this.bunifuButton2.Location = new System.Drawing.Point(0, 0);
            this.bunifuButton2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuButton2.Name = "bunifuButton2";
            this.bunifuButton2.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuButton2.OnDisabledState.BorderRadius = 1;
            this.bunifuButton2.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton2.OnDisabledState.BorderThickness = 1;
            this.bunifuButton2.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton2.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton2.OnDisabledState.IconLeftImage = null;
            this.bunifuButton2.OnDisabledState.IconRightImage = null;
            this.bunifuButton2.onHoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.bunifuButton2.onHoverState.BorderRadius = 1;
            this.bunifuButton2.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton2.onHoverState.BorderThickness = 1;
            this.bunifuButton2.onHoverState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.bunifuButton2.onHoverState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton2.onHoverState.IconLeftImage = null;
            this.bunifuButton2.onHoverState.IconRightImage = null;
            this.bunifuButton2.OnIdleState.BorderColor = System.Drawing.Color.MidnightBlue;
            this.bunifuButton2.OnIdleState.BorderRadius = 1;
            this.bunifuButton2.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton2.OnIdleState.BorderThickness = 1;
            this.bunifuButton2.OnIdleState.FillColor = System.Drawing.Color.Transparent;
            this.bunifuButton2.OnIdleState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton2.OnIdleState.IconLeftImage = null;
            this.bunifuButton2.OnIdleState.IconRightImage = null;
            this.bunifuButton2.OnPressedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            this.bunifuButton2.OnPressedState.BorderRadius = 1;
            this.bunifuButton2.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton2.OnPressedState.BorderThickness = 1;
            this.bunifuButton2.OnPressedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            this.bunifuButton2.OnPressedState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton2.OnPressedState.IconLeftImage = null;
            this.bunifuButton2.OnPressedState.IconRightImage = null;
            this.bunifuButton2.Size = new System.Drawing.Size(267, 50);
            this.bunifuButton2.TabIndex = 9;
            this.bunifuButton2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuButton2.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.bunifuButton2.TextMarginLeft = 0;
            this.bunifuButton2.TextPadding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.bunifuButton2.UseDefaultRadiusAndThickness = true;
            this.bunifuButton2.Click += new System.EventHandler(this.bunifuButton2_Click_4);
            // 
            // button3
            // 
            this.button3.Dock = System.Windows.Forms.DockStyle.Top;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SkyBlue;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(0, 256);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Padding = new System.Windows.Forms.Padding(13, 0, 0, 0);
            this.button3.Size = new System.Drawing.Size(267, 48);
            this.button3.TabIndex = 22;
            this.button3.Text = "Manage Employee";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button5
            // 
            this.button5.Dock = System.Windows.Forms.DockStyle.Top;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SkyBlue;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(0, 208);
            this.button5.Margin = new System.Windows.Forms.Padding(4);
            this.button5.Name = "button5";
            this.button5.Padding = new System.Windows.Forms.Padding(13, 0, 0, 0);
            this.button5.Size = new System.Drawing.Size(267, 48);
            this.button5.TabIndex = 21;
            this.button5.Text = "Dashboard";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Dock = System.Windows.Forms.DockStyle.Top;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SkyBlue;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(0, 160);
            this.button4.Margin = new System.Windows.Forms.Padding(4);
            this.button4.Name = "button4";
            this.button4.Padding = new System.Windows.Forms.Padding(13, 0, 0, 0);
            this.button4.Size = new System.Drawing.Size(267, 48);
            this.button4.TabIndex = 20;
            this.button4.Text = "Home";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // bunifuPanel2
            // 
            this.bunifuPanel2.BackgroundColor = System.Drawing.Color.Transparent;
            this.bunifuPanel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuPanel2.BackgroundImage")));
            this.bunifuPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuPanel2.BorderColor = System.Drawing.Color.Transparent;
            this.bunifuPanel2.BorderRadius = 3;
            this.bunifuPanel2.BorderThickness = 1;
            this.bunifuPanel2.Controls.Add(this.label21);
            this.bunifuPanel2.Controls.Add(this.bunifuPictureBox1);
            this.bunifuPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuPanel2.Location = new System.Drawing.Point(0, 0);
            this.bunifuPanel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuPanel2.Name = "bunifuPanel2";
            this.bunifuPanel2.ShowBorders = true;
            this.bunifuPanel2.Size = new System.Drawing.Size(267, 160);
            this.bunifuPanel2.TabIndex = 0;
            // 
            // label21
            // 
            this.label21.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.SystemColors.Window;
            this.label21.Location = new System.Drawing.Point(61, 111);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(117, 36);
            this.label21.TabIndex = 2;
            this.label21.Text = "ADMIN";
            // 
            // bunifuPictureBox1
            // 
            this.bunifuPictureBox1.AllowFocused = false;
            this.bunifuPictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuPictureBox1.AutoSizeHeight = true;
            this.bunifuPictureBox1.BackColor = System.Drawing.Color.AliceBlue;
            this.bunifuPictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuPictureBox1.BorderRadius = 50;
            this.bunifuPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("bunifuPictureBox1.Image")));
            this.bunifuPictureBox1.IsCircle = true;
            this.bunifuPictureBox1.Location = new System.Drawing.Point(68, 10);
            this.bunifuPictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuPictureBox1.Name = "bunifuPictureBox1";
            this.bunifuPictureBox1.Size = new System.Drawing.Size(100, 100);
            this.bunifuPictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuPictureBox1.TabIndex = 0;
            this.bunifuPictureBox1.TabStop = false;
            this.bunifuPictureBox1.Type = Bunifu.UI.WinForms.BunifuPictureBox.Types.Circle;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.bunifuPages1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(269, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1543, 866);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // bunifuPages1
            // 
            this.bunifuPages1.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.bunifuPages1.AllowTransitions = true;
            this.bunifuPages1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuPages1.Controls.Add(this.tabPage1);
            this.bunifuPages1.Controls.Add(this.tabPage2);
            this.bunifuPages1.Controls.Add(this.tabPage3);
            this.bunifuPages1.Controls.Add(this.tabPage4);
            this.bunifuPages1.Controls.Add(this.tabPage5);
            this.bunifuPages1.Controls.Add(this.tabPage6);
            this.bunifuPages1.Controls.Add(this.tabPage7);
            this.bunifuPages1.Controls.Add(this.tabPage8);
            this.bunifuPages1.Controls.Add(this.tabPage9);
            this.bunifuPages1.Controls.Add(this.tabPage10);
            this.bunifuPages1.Location = new System.Drawing.Point(3, 2);
            this.bunifuPages1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuPages1.Multiline = true;
            this.bunifuPages1.Name = "bunifuPages1";
            this.bunifuPages1.Page = this.tabPage7;
            this.bunifuPages1.PageIndex = 6;
            this.bunifuPages1.PageName = "tabPage7";
            this.bunifuPages1.PageTitle = "tabPage7";
            this.bunifuPages1.SelectedIndex = 0;
            this.bunifuPages1.Size = new System.Drawing.Size(1537, 862);
            this.bunifuPages1.TabIndex = 0;
            animation1.AnimateOnlyDifferences = false;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 0F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(0);
            animation1.RotateCoeff = 0F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 0F;
            animation1.TransparencyCoeff = 0F;
            this.bunifuPages1.Transition = animation1;
            this.bunifuPages1.TransitionType = Utilities.BunifuPages.BunifuAnimatorNS.AnimationType.Custom;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanel6);
            this.tabPage1.Controls.Add(this.tableLayoutPanel5);
            this.tabPage1.Controls.Add(this.tableLayoutPanel3);
            this.tabPage1.Controls.Add(this.tableLayoutPanel2);
            this.tabPage1.Location = new System.Drawing.Point(4, 4);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Size = new System.Drawing.Size(1529, 833);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel6.ColumnCount = 3;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.34F));
            this.tableLayoutPanel6.Controls.Add(this.bunifuShadowPanel13, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.bunifuShadowPanel12, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.bunifuShadowPanel11, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.bunifuShadowPanel9, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.bunifuShadowPanel10, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.bunifuShadowPanel8, 0, 0);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(7, 468);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1489, 348);
            this.tableLayoutPanel6.TabIndex = 8;
            // 
            // bunifuShadowPanel13
            // 
            this.bunifuShadowPanel13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuShadowPanel13.BackColor = System.Drawing.Color.Transparent;
            this.bunifuShadowPanel13.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel13.BorderRadius = 1;
            this.bunifuShadowPanel13.BorderThickness = 1;
            this.bunifuShadowPanel13.Controls.Add(this.pictureBox5);
            this.bunifuShadowPanel13.Controls.Add(this.label20);
            this.bunifuShadowPanel13.FillStyle = Bunifu.UI.WinForms.BunifuShadowPanel.FillStyles.Solid;
            this.bunifuShadowPanel13.GradientMode = Bunifu.UI.WinForms.BunifuShadowPanel.GradientModes.Vertical;
            this.bunifuShadowPanel13.Location = new System.Drawing.Point(3, 176);
            this.bunifuShadowPanel13.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuShadowPanel13.Name = "bunifuShadowPanel13";
            this.bunifuShadowPanel13.PanelColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel13.PanelColor2 = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel13.ShadowColor = System.Drawing.Color.DarkGray;
            this.bunifuShadowPanel13.ShadowDept = 2;
            this.bunifuShadowPanel13.ShadowDepth = 5;
            this.bunifuShadowPanel13.ShadowStyle = Bunifu.UI.WinForms.BunifuShadowPanel.ShadowStyles.Surrounded;
            this.bunifuShadowPanel13.ShadowTopLeftVisible = false;
            this.bunifuShadowPanel13.Size = new System.Drawing.Size(490, 170);
            this.bunifuShadowPanel13.Style = Bunifu.UI.WinForms.BunifuShadowPanel.BevelStyles.Flat;
            this.bunifuShadowPanel13.TabIndex = 20;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox5.Location = new System.Drawing.Point(171, 32);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(88, 66);
            this.pictureBox5.TabIndex = 3;
            this.pictureBox5.TabStop = false;
            // 
            // label20
            // 
            this.label20.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(95, 102);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(235, 29);
            this.label20.TabIndex = 2;
            this.label20.Text = "Add New Purchase";
            // 
            // bunifuShadowPanel12
            // 
            this.bunifuShadowPanel12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuShadowPanel12.BackColor = System.Drawing.Color.Transparent;
            this.bunifuShadowPanel12.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel12.BorderRadius = 1;
            this.bunifuShadowPanel12.BorderThickness = 1;
            this.bunifuShadowPanel12.Controls.Add(this.pictureBox4);
            this.bunifuShadowPanel12.Controls.Add(this.label19);
            this.bunifuShadowPanel12.FillStyle = Bunifu.UI.WinForms.BunifuShadowPanel.FillStyles.Solid;
            this.bunifuShadowPanel12.GradientMode = Bunifu.UI.WinForms.BunifuShadowPanel.GradientModes.Vertical;
            this.bunifuShadowPanel12.Location = new System.Drawing.Point(499, 176);
            this.bunifuShadowPanel12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuShadowPanel12.Name = "bunifuShadowPanel12";
            this.bunifuShadowPanel12.PanelColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel12.PanelColor2 = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel12.ShadowColor = System.Drawing.Color.DarkGray;
            this.bunifuShadowPanel12.ShadowDept = 2;
            this.bunifuShadowPanel12.ShadowDepth = 5;
            this.bunifuShadowPanel12.ShadowStyle = Bunifu.UI.WinForms.BunifuShadowPanel.ShadowStyles.Surrounded;
            this.bunifuShadowPanel12.ShadowTopLeftVisible = false;
            this.bunifuShadowPanel12.Size = new System.Drawing.Size(490, 170);
            this.bunifuShadowPanel12.Style = Bunifu.UI.WinForms.BunifuShadowPanel.BevelStyles.Flat;
            this.bunifuShadowPanel12.TabIndex = 19;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(214, 44);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(61, 50);
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(126, 102);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(221, 29);
            this.label19.TabIndex = 2;
            this.label19.Text = "Purchase Reports";
            // 
            // bunifuShadowPanel11
            // 
            this.bunifuShadowPanel11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuShadowPanel11.BackColor = System.Drawing.Color.Transparent;
            this.bunifuShadowPanel11.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel11.BorderRadius = 1;
            this.bunifuShadowPanel11.BorderThickness = 1;
            this.bunifuShadowPanel11.Controls.Add(this.pictureBox6);
            this.bunifuShadowPanel11.Controls.Add(this.label18);
            this.bunifuShadowPanel11.FillStyle = Bunifu.UI.WinForms.BunifuShadowPanel.FillStyles.Solid;
            this.bunifuShadowPanel11.GradientMode = Bunifu.UI.WinForms.BunifuShadowPanel.GradientModes.Vertical;
            this.bunifuShadowPanel11.Location = new System.Drawing.Point(995, 176);
            this.bunifuShadowPanel11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuShadowPanel11.Name = "bunifuShadowPanel11";
            this.bunifuShadowPanel11.PanelColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel11.PanelColor2 = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel11.ShadowColor = System.Drawing.Color.DarkGray;
            this.bunifuShadowPanel11.ShadowDept = 2;
            this.bunifuShadowPanel11.ShadowDepth = 5;
            this.bunifuShadowPanel11.ShadowStyle = Bunifu.UI.WinForms.BunifuShadowPanel.ShadowStyles.Surrounded;
            this.bunifuShadowPanel11.ShadowTopLeftVisible = false;
            this.bunifuShadowPanel11.Size = new System.Drawing.Size(491, 170);
            this.bunifuShadowPanel11.Style = Bunifu.UI.WinForms.BunifuShadowPanel.BevelStyles.Flat;
            this.bunifuShadowPanel11.TabIndex = 18;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.Location = new System.Drawing.Point(214, 45);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(61, 50);
            this.pictureBox6.TabIndex = 3;
            this.pictureBox6.TabStop = false;
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(161, 102);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(165, 29);
            this.label18.TabIndex = 2;
            this.label18.Text = "Sale Reports";
            // 
            // bunifuShadowPanel9
            // 
            this.bunifuShadowPanel9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuShadowPanel9.BackColor = System.Drawing.Color.Transparent;
            this.bunifuShadowPanel9.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel9.BorderRadius = 1;
            this.bunifuShadowPanel9.BorderThickness = 1;
            this.bunifuShadowPanel9.Controls.Add(this.pictureBox2);
            this.bunifuShadowPanel9.Controls.Add(this.label16);
            this.bunifuShadowPanel9.FillStyle = Bunifu.UI.WinForms.BunifuShadowPanel.FillStyles.Solid;
            this.bunifuShadowPanel9.GradientMode = Bunifu.UI.WinForms.BunifuShadowPanel.GradientModes.Vertical;
            this.bunifuShadowPanel9.Location = new System.Drawing.Point(499, 2);
            this.bunifuShadowPanel9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuShadowPanel9.Name = "bunifuShadowPanel9";
            this.bunifuShadowPanel9.PanelColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel9.PanelColor2 = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel9.ShadowColor = System.Drawing.Color.DarkGray;
            this.bunifuShadowPanel9.ShadowDept = 2;
            this.bunifuShadowPanel9.ShadowDepth = 5;
            this.bunifuShadowPanel9.ShadowStyle = Bunifu.UI.WinForms.BunifuShadowPanel.ShadowStyles.Surrounded;
            this.bunifuShadowPanel9.ShadowTopLeftVisible = false;
            this.bunifuShadowPanel9.Size = new System.Drawing.Size(490, 170);
            this.bunifuShadowPanel9.Style = Bunifu.UI.WinForms.BunifuShadowPanel.BevelStyles.Flat;
            this.bunifuShadowPanel9.TabIndex = 14;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(215, 44);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(61, 50);
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(126, 100);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(233, 29);
            this.label16.TabIndex = 2;
            this.label16.Text = "Add New Medicine";
            // 
            // bunifuShadowPanel10
            // 
            this.bunifuShadowPanel10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuShadowPanel10.BackColor = System.Drawing.Color.Transparent;
            this.bunifuShadowPanel10.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel10.BorderRadius = 1;
            this.bunifuShadowPanel10.BorderThickness = 1;
            this.bunifuShadowPanel10.Controls.Add(this.pictureBox3);
            this.bunifuShadowPanel10.Controls.Add(this.label17);
            this.bunifuShadowPanel10.FillStyle = Bunifu.UI.WinForms.BunifuShadowPanel.FillStyles.Solid;
            this.bunifuShadowPanel10.GradientMode = Bunifu.UI.WinForms.BunifuShadowPanel.GradientModes.Vertical;
            this.bunifuShadowPanel10.Location = new System.Drawing.Point(995, 2);
            this.bunifuShadowPanel10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuShadowPanel10.Name = "bunifuShadowPanel10";
            this.bunifuShadowPanel10.PanelColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel10.PanelColor2 = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel10.ShadowColor = System.Drawing.Color.DarkGray;
            this.bunifuShadowPanel10.ShadowDept = 2;
            this.bunifuShadowPanel10.ShadowDepth = 5;
            this.bunifuShadowPanel10.ShadowStyle = Bunifu.UI.WinForms.BunifuShadowPanel.ShadowStyles.Surrounded;
            this.bunifuShadowPanel10.ShadowTopLeftVisible = false;
            this.bunifuShadowPanel10.Size = new System.Drawing.Size(491, 170);
            this.bunifuShadowPanel10.Style = Bunifu.UI.WinForms.BunifuShadowPanel.BevelStyles.Flat;
            this.bunifuShadowPanel10.TabIndex = 15;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(214, 42);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(61, 50);
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(140, 100);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(225, 29);
            this.label17.TabIndex = 2;
            this.label17.Text = "Add New Supplier";
            // 
            // bunifuShadowPanel8
            // 
            this.bunifuShadowPanel8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuShadowPanel8.BackColor = System.Drawing.Color.Transparent;
            this.bunifuShadowPanel8.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel8.BorderRadius = 1;
            this.bunifuShadowPanel8.BorderThickness = 1;
            this.bunifuShadowPanel8.Controls.Add(this.pictureBox1);
            this.bunifuShadowPanel8.Controls.Add(this.label15);
            this.bunifuShadowPanel8.FillStyle = Bunifu.UI.WinForms.BunifuShadowPanel.FillStyles.Solid;
            this.bunifuShadowPanel8.GradientMode = Bunifu.UI.WinForms.BunifuShadowPanel.GradientModes.Vertical;
            this.bunifuShadowPanel8.Location = new System.Drawing.Point(3, 2);
            this.bunifuShadowPanel8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuShadowPanel8.Name = "bunifuShadowPanel8";
            this.bunifuShadowPanel8.PanelColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel8.PanelColor2 = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel8.ShadowColor = System.Drawing.Color.DarkGray;
            this.bunifuShadowPanel8.ShadowDept = 2;
            this.bunifuShadowPanel8.ShadowDepth = 5;
            this.bunifuShadowPanel8.ShadowStyle = Bunifu.UI.WinForms.BunifuShadowPanel.ShadowStyles.Surrounded;
            this.bunifuShadowPanel8.ShadowTopLeftVisible = false;
            this.bunifuShadowPanel8.Size = new System.Drawing.Size(490, 170);
            this.bunifuShadowPanel8.Style = Bunifu.UI.WinForms.BunifuShadowPanel.BevelStyles.Flat;
            this.bunifuShadowPanel8.TabIndex = 13;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(186, 45);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(61, 50);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(95, 100);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(238, 29);
            this.label15.TabIndex = 1;
            this.label15.Text = "Add New Customer";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.guna2HScrollBar2, 0, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(7, 409);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1489, 53);
            this.tableLayoutPanel5.TabIndex = 7;
            // 
            // guna2HScrollBar2
            // 
            this.guna2HScrollBar2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2HScrollBar2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.guna2HScrollBar2.HoverState.Parent = null;
            this.guna2HScrollBar2.LargeChange = 10;
            this.guna2HScrollBar2.Location = new System.Drawing.Point(5, 21);
            this.guna2HScrollBar2.Margin = new System.Windows.Forms.Padding(5);
            this.guna2HScrollBar2.MouseWheelBarPartitions = 10;
            this.guna2HScrollBar2.Name = "guna2HScrollBar2";
            this.guna2HScrollBar2.PressedState.Parent = this.guna2HScrollBar2;
            this.guna2HScrollBar2.ScrollbarSize = 10;
            this.guna2HScrollBar2.Size = new System.Drawing.Size(1479, 10);
            this.guna2HScrollBar2.TabIndex = 5;
            this.guna2HScrollBar2.ThumbColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(152)))), ((int)(((byte)(166)))));
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Controls.Add(this.bunifuPanel3, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 178);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1495, 225);
            this.tableLayoutPanel3.TabIndex = 5;
            // 
            // bunifuPanel3
            // 
            this.bunifuPanel3.BackgroundColor = System.Drawing.Color.Transparent;
            this.bunifuPanel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuPanel3.BackgroundImage")));
            this.bunifuPanel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuPanel3.BorderColor = System.Drawing.Color.DarkRed;
            this.bunifuPanel3.BorderRadius = 3;
            this.bunifuPanel3.BorderThickness = 2;
            this.bunifuPanel3.Controls.Add(this.label26);
            this.bunifuPanel3.Controls.Add(this.label25);
            this.bunifuPanel3.Controls.Add(this.label24);
            this.bunifuPanel3.Controls.Add(this.label23);
            this.bunifuPanel3.Controls.Add(this.label22);
            this.bunifuPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bunifuPanel3.Location = new System.Drawing.Point(1124, 2);
            this.bunifuPanel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuPanel3.Name = "bunifuPanel3";
            this.bunifuPanel3.ShowBorders = true;
            this.bunifuPanel3.Size = new System.Drawing.Size(368, 221);
            this.bunifuPanel3.TabIndex = 13;
            // 
            // label26
            // 
            this.label26.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(213, 70);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(19, 20);
            this.label26.TabIndex = 7;
            this.label26.Text = "0";
            // 
            // label25
            // 
            this.label25.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(213, 100);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(19, 20);
            this.label25.TabIndex = 6;
            this.label25.Text = "0";
            // 
            // label24
            // 
            this.label24.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(56, 100);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(136, 20);
            this.label24.TabIndex = 5;
            this.label24.Text = "Total Purchase";
            // 
            // label23
            // 
            this.label23.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(56, 66);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(94, 20);
            this.label23.TabIndex = 4;
            this.label23.Text = "Total Sale";
            // 
            // label22
            // 
            this.label22.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(19, 18);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(190, 29);
            this.label22.TabIndex = 3;
            this.label22.Text = "Today\'s Report";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.34F));
            this.tableLayoutPanel4.Controls.Add(this.bunifuShadowPanel7, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.bunifuShadowPanel6, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.bunifuShadowPanel5, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.bunifuShadowPanel2, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.bunifuShadowPanel4, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.bunifuShadowPanel3, 2, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 2);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1115, 221);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // bunifuShadowPanel7
            // 
            this.bunifuShadowPanel7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuShadowPanel7.BackColor = System.Drawing.Color.Transparent;
            this.bunifuShadowPanel7.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel7.BorderRadius = 1;
            this.bunifuShadowPanel7.BorderThickness = 1;
            this.bunifuShadowPanel7.Controls.Add(this.label14);
            this.bunifuShadowPanel7.Controls.Add(this.label6);
            this.bunifuShadowPanel7.FillStyle = Bunifu.UI.WinForms.BunifuShadowPanel.FillStyles.Solid;
            this.bunifuShadowPanel7.GradientMode = Bunifu.UI.WinForms.BunifuShadowPanel.GradientModes.Vertical;
            this.bunifuShadowPanel7.Location = new System.Drawing.Point(3, 112);
            this.bunifuShadowPanel7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuShadowPanel7.Name = "bunifuShadowPanel7";
            this.bunifuShadowPanel7.PanelColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel7.PanelColor2 = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel7.ShadowColor = System.Drawing.Color.DarkGray;
            this.bunifuShadowPanel7.ShadowDept = 2;
            this.bunifuShadowPanel7.ShadowDepth = 5;
            this.bunifuShadowPanel7.ShadowStyle = Bunifu.UI.WinForms.BunifuShadowPanel.ShadowStyles.Surrounded;
            this.bunifuShadowPanel7.ShadowTopLeftVisible = false;
            this.bunifuShadowPanel7.Size = new System.Drawing.Size(365, 107);
            this.bunifuShadowPanel7.Style = Bunifu.UI.WinForms.BunifuShadowPanel.BevelStyles.Flat;
            this.bunifuShadowPanel7.TabIndex = 12;
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(141, 66);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(19, 20);
            this.label14.TabIndex = 2;
            this.label14.Text = "0";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(108, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 20);
            this.label6.TabIndex = 1;
            this.label6.Text = "Total Sale";
            // 
            // bunifuShadowPanel6
            // 
            this.bunifuShadowPanel6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuShadowPanel6.BackColor = System.Drawing.Color.Transparent;
            this.bunifuShadowPanel6.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel6.BorderRadius = 1;
            this.bunifuShadowPanel6.BorderThickness = 1;
            this.bunifuShadowPanel6.Controls.Add(this.label13);
            this.bunifuShadowPanel6.Controls.Add(this.label7);
            this.bunifuShadowPanel6.FillStyle = Bunifu.UI.WinForms.BunifuShadowPanel.FillStyles.Solid;
            this.bunifuShadowPanel6.GradientMode = Bunifu.UI.WinForms.BunifuShadowPanel.GradientModes.Vertical;
            this.bunifuShadowPanel6.Location = new System.Drawing.Point(374, 112);
            this.bunifuShadowPanel6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuShadowPanel6.Name = "bunifuShadowPanel6";
            this.bunifuShadowPanel6.PanelColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel6.PanelColor2 = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel6.ShadowColor = System.Drawing.Color.DarkGray;
            this.bunifuShadowPanel6.ShadowDept = 2;
            this.bunifuShadowPanel6.ShadowDepth = 5;
            this.bunifuShadowPanel6.ShadowStyle = Bunifu.UI.WinForms.BunifuShadowPanel.ShadowStyles.Surrounded;
            this.bunifuShadowPanel6.ShadowTopLeftVisible = false;
            this.bunifuShadowPanel6.Size = new System.Drawing.Size(365, 107);
            this.bunifuShadowPanel6.Style = Bunifu.UI.WinForms.BunifuShadowPanel.BevelStyles.Flat;
            this.bunifuShadowPanel6.TabIndex = 11;
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(173, 66);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(19, 20);
            this.label13.TabIndex = 2;
            this.label13.Text = "0";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(143, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 20);
            this.label7.TabIndex = 1;
            this.label7.Text = "Expired";
            // 
            // bunifuShadowPanel5
            // 
            this.bunifuShadowPanel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuShadowPanel5.BackColor = System.Drawing.Color.Transparent;
            this.bunifuShadowPanel5.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel5.BorderRadius = 1;
            this.bunifuShadowPanel5.BorderThickness = 1;
            this.bunifuShadowPanel5.Controls.Add(this.label12);
            this.bunifuShadowPanel5.Controls.Add(this.label8);
            this.bunifuShadowPanel5.FillStyle = Bunifu.UI.WinForms.BunifuShadowPanel.FillStyles.Solid;
            this.bunifuShadowPanel5.GradientMode = Bunifu.UI.WinForms.BunifuShadowPanel.GradientModes.Vertical;
            this.bunifuShadowPanel5.Location = new System.Drawing.Point(745, 112);
            this.bunifuShadowPanel5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuShadowPanel5.Name = "bunifuShadowPanel5";
            this.bunifuShadowPanel5.PanelColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel5.PanelColor2 = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel5.ShadowColor = System.Drawing.Color.DarkGray;
            this.bunifuShadowPanel5.ShadowDept = 2;
            this.bunifuShadowPanel5.ShadowDepth = 5;
            this.bunifuShadowPanel5.ShadowStyle = Bunifu.UI.WinForms.BunifuShadowPanel.ShadowStyles.Surrounded;
            this.bunifuShadowPanel5.ShadowTopLeftVisible = false;
            this.bunifuShadowPanel5.Size = new System.Drawing.Size(367, 107);
            this.bunifuShadowPanel5.Style = Bunifu.UI.WinForms.BunifuShadowPanel.BevelStyles.Flat;
            this.bunifuShadowPanel5.TabIndex = 10;
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(156, 65);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(19, 20);
            this.label12.TabIndex = 2;
            this.label12.Text = "0";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(110, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(114, 20);
            this.label8.TabIndex = 1;
            this.label8.Text = "Out of Stock";
            // 
            // bunifuShadowPanel2
            // 
            this.bunifuShadowPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuShadowPanel2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuShadowPanel2.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel2.BorderRadius = 1;
            this.bunifuShadowPanel2.BorderThickness = 1;
            this.bunifuShadowPanel2.Controls.Add(this.label9);
            this.bunifuShadowPanel2.Controls.Add(this.label3);
            this.bunifuShadowPanel2.FillStyle = Bunifu.UI.WinForms.BunifuShadowPanel.FillStyles.Solid;
            this.bunifuShadowPanel2.GradientMode = Bunifu.UI.WinForms.BunifuShadowPanel.GradientModes.Vertical;
            this.bunifuShadowPanel2.Location = new System.Drawing.Point(3, 2);
            this.bunifuShadowPanel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuShadowPanel2.Name = "bunifuShadowPanel2";
            this.bunifuShadowPanel2.PanelColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel2.PanelColor2 = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel2.ShadowColor = System.Drawing.Color.DarkGray;
            this.bunifuShadowPanel2.ShadowDept = 2;
            this.bunifuShadowPanel2.ShadowDepth = 5;
            this.bunifuShadowPanel2.ShadowStyle = Bunifu.UI.WinForms.BunifuShadowPanel.ShadowStyles.Surrounded;
            this.bunifuShadowPanel2.ShadowTopLeftVisible = false;
            this.bunifuShadowPanel2.Size = new System.Drawing.Size(365, 106);
            this.bunifuShadowPanel2.Style = Bunifu.UI.WinForms.BunifuShadowPanel.BevelStyles.Flat;
            this.bunifuShadowPanel2.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(141, 55);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(19, 20);
            this.label9.TabIndex = 1;
            this.label9.Text = "0";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(95, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(148, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "Total Customers";
            // 
            // bunifuShadowPanel4
            // 
            this.bunifuShadowPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuShadowPanel4.BackColor = System.Drawing.Color.Transparent;
            this.bunifuShadowPanel4.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel4.BorderRadius = 1;
            this.bunifuShadowPanel4.BorderThickness = 1;
            this.bunifuShadowPanel4.Controls.Add(this.label10);
            this.bunifuShadowPanel4.Controls.Add(this.label4);
            this.bunifuShadowPanel4.FillStyle = Bunifu.UI.WinForms.BunifuShadowPanel.FillStyles.Solid;
            this.bunifuShadowPanel4.GradientMode = Bunifu.UI.WinForms.BunifuShadowPanel.GradientModes.Vertical;
            this.bunifuShadowPanel4.Location = new System.Drawing.Point(374, 2);
            this.bunifuShadowPanel4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuShadowPanel4.Name = "bunifuShadowPanel4";
            this.bunifuShadowPanel4.PanelColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel4.PanelColor2 = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel4.ShadowColor = System.Drawing.Color.DarkGray;
            this.bunifuShadowPanel4.ShadowDept = 2;
            this.bunifuShadowPanel4.ShadowDepth = 5;
            this.bunifuShadowPanel4.ShadowStyle = Bunifu.UI.WinForms.BunifuShadowPanel.ShadowStyles.Surrounded;
            this.bunifuShadowPanel4.ShadowTopLeftVisible = false;
            this.bunifuShadowPanel4.Size = new System.Drawing.Size(365, 106);
            this.bunifuShadowPanel4.Style = Bunifu.UI.WinForms.BunifuShadowPanel.BevelStyles.Flat;
            this.bunifuShadowPanel4.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(173, 50);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(19, 20);
            this.label10.TabIndex = 2;
            this.label10.Text = "0";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(117, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(132, 20);
            this.label4.TabIndex = 1;
            this.label4.Text = "Total Medicine";
            // 
            // bunifuShadowPanel3
            // 
            this.bunifuShadowPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuShadowPanel3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuShadowPanel3.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel3.BorderRadius = 1;
            this.bunifuShadowPanel3.BorderThickness = 1;
            this.bunifuShadowPanel3.Controls.Add(this.label11);
            this.bunifuShadowPanel3.Controls.Add(this.label5);
            this.bunifuShadowPanel3.FillStyle = Bunifu.UI.WinForms.BunifuShadowPanel.FillStyles.Solid;
            this.bunifuShadowPanel3.GradientMode = Bunifu.UI.WinForms.BunifuShadowPanel.GradientModes.Vertical;
            this.bunifuShadowPanel3.Location = new System.Drawing.Point(745, 2);
            this.bunifuShadowPanel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bunifuShadowPanel3.Name = "bunifuShadowPanel3";
            this.bunifuShadowPanel3.PanelColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel3.PanelColor2 = System.Drawing.Color.WhiteSmoke;
            this.bunifuShadowPanel3.ShadowColor = System.Drawing.Color.DarkGray;
            this.bunifuShadowPanel3.ShadowDept = 2;
            this.bunifuShadowPanel3.ShadowDepth = 5;
            this.bunifuShadowPanel3.ShadowStyle = Bunifu.UI.WinForms.BunifuShadowPanel.ShadowStyles.Surrounded;
            this.bunifuShadowPanel3.ShadowTopLeftVisible = false;
            this.bunifuShadowPanel3.Size = new System.Drawing.Size(367, 106);
            this.bunifuShadowPanel3.Style = Bunifu.UI.WinForms.BunifuShadowPanel.BevelStyles.Flat;
            this.bunifuShadowPanel3.TabIndex = 8;
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(156, 54);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 20);
            this.label11.TabIndex = 2;
            this.label11.Text = "0";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(102, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 20);
            this.label5.TabIndex = 1;
            this.label5.Text = "Total Supplier";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.guna2HScrollBar1, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 2);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1523, 169);
            this.tableLayoutPanel2.TabIndex = 4;
            // 
            // guna2HScrollBar1
            // 
            this.guna2HScrollBar1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2HScrollBar1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.guna2HScrollBar1.HoverState.Parent = null;
            this.guna2HScrollBar1.LargeChange = 10;
            this.guna2HScrollBar1.Location = new System.Drawing.Point(5, 129);
            this.guna2HScrollBar1.Margin = new System.Windows.Forms.Padding(5);
            this.guna2HScrollBar1.MouseWheelBarPartitions = 10;
            this.guna2HScrollBar1.Name = "guna2HScrollBar1";
            this.guna2HScrollBar1.PressedState.Parent = this.guna2HScrollBar1;
            this.guna2HScrollBar1.ScrollbarSize = 10;
            this.guna2HScrollBar1.Size = new System.Drawing.Size(1513, 10);
            this.guna2HScrollBar1.TabIndex = 5;
            this.guna2HScrollBar1.ThumbColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(152)))), ((int)(((byte)(166)))));
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 32);
            this.label2.TabIndex = 1;
            this.label2.Text = "Home";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Dashboard";
            // 
            // tabPage2
            // 
            this.tabPage2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage2.Controls.Add(this.tableLayoutPanel8);
            this.tabPage2.Controls.Add(this.tableLayoutPanel7);
            this.tabPage2.Location = new System.Drawing.Point(4, 4);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Size = new System.Drawing.Size(1529, 833);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 4;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.57488F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.42512F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 151F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 397F));
            this.tableLayoutPanel8.Controls.Add(this.bunifuLabel36, 0, 6);
            this.tableLayoutPanel8.Controls.Add(this.bunifuLabel2, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.emp_name, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.bunifuLabel26, 2, 0);
            this.tableLayoutPanel8.Controls.Add(this.emp_pin, 3, 0);
            this.tableLayoutPanel8.Controls.Add(this.bunifuLabel28, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.emp_cnic, 1, 1);
            this.tableLayoutPanel8.Controls.Add(this.bunifuLabel27, 2, 1);
            this.tableLayoutPanel8.Controls.Add(this.emp_gender, 3, 1);
            this.tableLayoutPanel8.Controls.Add(this.bunifuLabel30, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this.emp_salary, 1, 2);
            this.tableLayoutPanel8.Controls.Add(this.bunifuLabel29, 2, 2);
            this.tableLayoutPanel8.Controls.Add(this.emp_designation, 3, 2);
            this.tableLayoutPanel8.Controls.Add(this.bunifuLabel32, 0, 3);
            this.tableLayoutPanel8.Controls.Add(this.emp_dob, 1, 3);
            this.tableLayoutPanel8.Controls.Add(this.bunifuLabel31, 2, 3);
            this.tableLayoutPanel8.Controls.Add(this.emp_city, 3, 3);
            this.tableLayoutPanel8.Controls.Add(this.bunifuLabel34, 0, 4);
            this.tableLayoutPanel8.Controls.Add(this.emp_country, 1, 4);
            this.tableLayoutPanel8.Controls.Add(this.bunifuLabel33, 2, 4);
            this.tableLayoutPanel8.Controls.Add(this.emp_address, 3, 4);
            this.tableLayoutPanel8.Controls.Add(this.emp_contact, 0, 5);
            this.tableLayoutPanel8.Controls.Add(this.emp_email, 2, 5);
            this.tableLayoutPanel8.Controls.Add(this.guna2TextBox1, 1, 5);
            this.tableLayoutPanel8.Controls.Add(this.guna2TextBox2, 3, 5);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 64);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 10;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.13636F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.86364F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 78F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 81F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 78F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 225F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(1523, 767);
            this.tableLayoutPanel8.TabIndex = 1;
            // 
            // bunifuLabel36
            // 
            this.bunifuLabel36.AllowParentOverrides = false;
            this.bunifuLabel36.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.bunifuLabel36.AutoEllipsis = false;
            this.bunifuLabel36.CursorType = null;
            this.bunifuLabel36.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuLabel36.Location = new System.Drawing.Point(219, 359);
            this.bunifuLabel36.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuLabel36.Name = "bunifuLabel36";
            this.bunifuLabel36.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel36.Size = new System.Drawing.Size(104, 28);
            this.bunifuLabel36.TabIndex = 36;
            this.bunifuLabel36.Text = "Pharmacy : ";
            this.bunifuLabel36.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel36.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel2
            // 
            this.bunifuLabel2.AllowParentOverrides = false;
            this.bunifuLabel2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.bunifuLabel2.AutoEllipsis = false;
            this.bunifuLabel2.CursorType = null;
            this.bunifuLabel2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuLabel2.Location = new System.Drawing.Point(256, 14);
            this.bunifuLabel2.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuLabel2.Name = "bunifuLabel2";
            this.bunifuLabel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel2.Size = new System.Drawing.Size(67, 28);
            this.bunifuLabel2.TabIndex = 1;
            this.bunifuLabel2.Text = "Name : ";
            this.bunifuLabel2.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel2.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // emp_name
            // 
            this.emp_name.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.emp_name.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.emp_name.DefaultText = "";
            this.emp_name.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.emp_name.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.emp_name.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.emp_name.DisabledState.Parent = this.emp_name;
            this.emp_name.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.emp_name.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.emp_name.FocusedState.Parent = this.emp_name;
            this.emp_name.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.emp_name.HoverState.Parent = this.emp_name;
            this.emp_name.Location = new System.Drawing.Point(517, 15);
            this.emp_name.Margin = new System.Windows.Forms.Padding(5);
            this.emp_name.Name = "emp_name";
            this.emp_name.PasswordChar = '\0';
            this.emp_name.PlaceholderText = "";
            this.emp_name.SelectedText = "";
            this.emp_name.ShadowDecoration.Parent = this.emp_name;
            this.emp_name.Size = new System.Drawing.Size(267, 27);
            this.emp_name.TabIndex = 14;
            // 
            // bunifuLabel26
            // 
            this.bunifuLabel26.AllowParentOverrides = false;
            this.bunifuLabel26.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuLabel26.AutoEllipsis = false;
            this.bunifuLabel26.CursorType = null;
            this.bunifuLabel26.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuLabel26.Location = new System.Drawing.Point(1027, 14);
            this.bunifuLabel26.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuLabel26.Name = "bunifuLabel26";
            this.bunifuLabel26.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel26.Size = new System.Drawing.Size(45, 28);
            this.bunifuLabel26.TabIndex = 15;
            this.bunifuLabel26.Text = "PIN : ";
            this.bunifuLabel26.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel26.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // emp_pin
            // 
            this.emp_pin.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.emp_pin.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.emp_pin.DefaultText = "";
            this.emp_pin.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.emp_pin.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.emp_pin.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.emp_pin.DisabledState.Parent = this.emp_pin;
            this.emp_pin.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.emp_pin.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.emp_pin.FocusedState.Parent = this.emp_pin;
            this.emp_pin.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.emp_pin.HoverState.Parent = this.emp_pin;
            this.emp_pin.Location = new System.Drawing.Point(1130, 15);
            this.emp_pin.Margin = new System.Windows.Forms.Padding(5);
            this.emp_pin.Name = "emp_pin";
            this.emp_pin.PasswordChar = '*';
            this.emp_pin.PlaceholderText = "Min 8 characters, one letter, one number,one special character";
            this.emp_pin.SelectedText = "";
            this.emp_pin.ShadowDecoration.Parent = this.emp_pin;
            this.emp_pin.Size = new System.Drawing.Size(376, 27);
            this.emp_pin.TabIndex = 16;
            // 
            // bunifuLabel28
            // 
            this.bunifuLabel28.AllowParentOverrides = false;
            this.bunifuLabel28.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.bunifuLabel28.AutoEllipsis = false;
            this.bunifuLabel28.CursorType = null;
            this.bunifuLabel28.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuLabel28.Location = new System.Drawing.Point(266, 70);
            this.bunifuLabel28.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuLabel28.Name = "bunifuLabel28";
            this.bunifuLabel28.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel28.Size = new System.Drawing.Size(57, 28);
            this.bunifuLabel28.TabIndex = 17;
            this.bunifuLabel28.Text = "CNIC : ";
            this.bunifuLabel28.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel28.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // emp_cnic
            // 
            this.emp_cnic.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.emp_cnic.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.emp_cnic.DefaultText = "";
            this.emp_cnic.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.emp_cnic.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.emp_cnic.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.emp_cnic.DisabledState.Parent = this.emp_cnic;
            this.emp_cnic.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.emp_cnic.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.emp_cnic.FocusedState.Parent = this.emp_cnic;
            this.emp_cnic.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.emp_cnic.HoverState.Parent = this.emp_cnic;
            this.emp_cnic.Location = new System.Drawing.Point(517, 68);
            this.emp_cnic.Margin = new System.Windows.Forms.Padding(5);
            this.emp_cnic.Name = "emp_cnic";
            this.emp_cnic.PasswordChar = '\0';
            this.emp_cnic.PlaceholderText = "";
            this.emp_cnic.SelectedText = "";
            this.emp_cnic.ShadowDecoration.Parent = this.emp_cnic;
            this.emp_cnic.Size = new System.Drawing.Size(267, 32);
            this.emp_cnic.TabIndex = 18;
            // 
            // bunifuLabel27
            // 
            this.bunifuLabel27.AllowParentOverrides = false;
            this.bunifuLabel27.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuLabel27.AutoEllipsis = false;
            this.bunifuLabel27.CursorType = null;
            this.bunifuLabel27.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuLabel27.Location = new System.Drawing.Point(1010, 70);
            this.bunifuLabel27.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuLabel27.Name = "bunifuLabel27";
            this.bunifuLabel27.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel27.Size = new System.Drawing.Size(79, 28);
            this.bunifuLabel27.TabIndex = 19;
            this.bunifuLabel27.Text = "Gender : ";
            this.bunifuLabel27.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel27.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // emp_gender
            // 
            this.emp_gender.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.emp_gender.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.emp_gender.DefaultText = "";
            this.emp_gender.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.emp_gender.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.emp_gender.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.emp_gender.DisabledState.Parent = this.emp_gender;
            this.emp_gender.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.emp_gender.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.emp_gender.FocusedState.Parent = this.emp_gender;
            this.emp_gender.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.emp_gender.HoverState.Parent = this.emp_gender;
            this.emp_gender.Location = new System.Drawing.Point(1130, 63);
            this.emp_gender.Margin = new System.Windows.Forms.Padding(5);
            this.emp_gender.Name = "emp_gender";
            this.emp_gender.PasswordChar = '\0';
            this.emp_gender.PlaceholderText = "\"M\",\"F\",\"U\"";
            this.emp_gender.SelectedText = "";
            this.emp_gender.ShadowDecoration.Parent = this.emp_gender;
            this.emp_gender.Size = new System.Drawing.Size(267, 42);
            this.emp_gender.TabIndex = 20;
            // 
            // bunifuLabel30
            // 
            this.bunifuLabel30.AllowParentOverrides = false;
            this.bunifuLabel30.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.bunifuLabel30.AutoEllipsis = false;
            this.bunifuLabel30.CursorType = null;
            this.bunifuLabel30.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuLabel30.Location = new System.Drawing.Point(254, 121);
            this.bunifuLabel30.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuLabel30.Name = "bunifuLabel30";
            this.bunifuLabel30.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel30.Size = new System.Drawing.Size(69, 28);
            this.bunifuLabel30.TabIndex = 21;
            this.bunifuLabel30.Text = "Salary : ";
            this.bunifuLabel30.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel30.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // emp_salary
            // 
            this.emp_salary.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.emp_salary.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.emp_salary.DefaultText = "";
            this.emp_salary.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.emp_salary.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.emp_salary.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.emp_salary.DisabledState.Parent = this.emp_salary;
            this.emp_salary.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.emp_salary.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.emp_salary.FocusedState.Parent = this.emp_salary;
            this.emp_salary.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.emp_salary.HoverState.Parent = this.emp_salary;
            this.emp_salary.Location = new System.Drawing.Point(517, 117);
            this.emp_salary.Margin = new System.Windows.Forms.Padding(5);
            this.emp_salary.Name = "emp_salary";
            this.emp_salary.PasswordChar = '\0';
            this.emp_salary.PlaceholderText = "";
            this.emp_salary.SelectedText = "";
            this.emp_salary.ShadowDecoration.Parent = this.emp_salary;
            this.emp_salary.Size = new System.Drawing.Size(267, 36);
            this.emp_salary.TabIndex = 22;
            // 
            // bunifuLabel29
            // 
            this.bunifuLabel29.AllowParentOverrides = false;
            this.bunifuLabel29.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuLabel29.AutoEllipsis = false;
            this.bunifuLabel29.CursorType = null;
            this.bunifuLabel29.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuLabel29.Location = new System.Drawing.Point(987, 121);
            this.bunifuLabel29.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuLabel29.Name = "bunifuLabel29";
            this.bunifuLabel29.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel29.Size = new System.Drawing.Size(125, 28);
            this.bunifuLabel29.TabIndex = 23;
            this.bunifuLabel29.Text = "Designation : ";
            this.bunifuLabel29.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel29.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // emp_designation
            // 
            this.emp_designation.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.emp_designation.BackColor = System.Drawing.Color.Transparent;
            this.emp_designation.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.emp_designation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.emp_designation.FocusedColor = System.Drawing.Color.Empty;
            this.emp_designation.FocusedState.Parent = this.emp_designation;
            this.emp_designation.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.emp_designation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.emp_designation.FormattingEnabled = true;
            this.emp_designation.HoverState.Parent = this.emp_designation;
            this.emp_designation.ItemHeight = 30;
            this.emp_designation.ItemsAppearance.Parent = this.emp_designation;
            this.emp_designation.Location = new System.Drawing.Point(1129, 117);
            this.emp_designation.Margin = new System.Windows.Forms.Padding(4);
            this.emp_designation.Name = "emp_designation";
            this.emp_designation.ShadowDecoration.Parent = this.emp_designation;
            this.emp_designation.Size = new System.Drawing.Size(265, 36);
            this.emp_designation.TabIndex = 27;
            // 
            // bunifuLabel32
            // 
            this.bunifuLabel32.AllowParentOverrides = false;
            this.bunifuLabel32.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.bunifuLabel32.AutoEllipsis = false;
            this.bunifuLabel32.CursorType = null;
            this.bunifuLabel32.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuLabel32.Location = new System.Drawing.Point(185, 162);
            this.bunifuLabel32.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuLabel32.Name = "bunifuLabel32";
            this.bunifuLabel32.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel32.Size = new System.Drawing.Size(138, 25);
            this.bunifuLabel32.TabIndex = 28;
            this.bunifuLabel32.Text = "Date Of Birth : ";
            this.bunifuLabel32.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel32.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // emp_dob
            // 
            this.emp_dob.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.emp_dob.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(59)))), ((int)(((byte)(90)))));
            this.emp_dob.CheckedState.Parent = this.emp_dob;
            this.emp_dob.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(59)))), ((int)(((byte)(90)))));
            this.emp_dob.ForeColor = System.Drawing.Color.White;
            this.emp_dob.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.emp_dob.HoverState.Parent = this.emp_dob;
            this.emp_dob.Location = new System.Drawing.Point(517, 162);
            this.emp_dob.Margin = new System.Windows.Forms.Padding(4);
            this.emp_dob.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.emp_dob.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.emp_dob.Name = "emp_dob";
            this.emp_dob.ShadowDecoration.Parent = this.emp_dob;
            this.emp_dob.Size = new System.Drawing.Size(267, 25);
            this.emp_dob.TabIndex = 29;
            this.emp_dob.Value = new System.DateTime(2022, 4, 13, 21, 31, 57, 385);
            // 
            // bunifuLabel31
            // 
            this.bunifuLabel31.AllowParentOverrides = false;
            this.bunifuLabel31.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuLabel31.AutoEllipsis = false;
            this.bunifuLabel31.CursorType = null;
            this.bunifuLabel31.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuLabel31.Location = new System.Drawing.Point(1025, 162);
            this.bunifuLabel31.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuLabel31.Name = "bunifuLabel31";
            this.bunifuLabel31.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel31.Size = new System.Drawing.Size(48, 25);
            this.bunifuLabel31.TabIndex = 30;
            this.bunifuLabel31.Text = "City : ";
            this.bunifuLabel31.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel31.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // emp_city
            // 
            this.emp_city.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.emp_city.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.emp_city.DefaultText = "";
            this.emp_city.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.emp_city.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.emp_city.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.emp_city.DisabledState.Parent = this.emp_city;
            this.emp_city.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.emp_city.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.emp_city.FocusedState.Parent = this.emp_city;
            this.emp_city.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.emp_city.HoverState.Parent = this.emp_city;
            this.emp_city.Location = new System.Drawing.Point(1130, 163);
            this.emp_city.Margin = new System.Windows.Forms.Padding(5);
            this.emp_city.Name = "emp_city";
            this.emp_city.PasswordChar = '\0';
            this.emp_city.PlaceholderText = "";
            this.emp_city.SelectedText = "";
            this.emp_city.ShadowDecoration.Parent = this.emp_city;
            this.emp_city.Size = new System.Drawing.Size(267, 23);
            this.emp_city.TabIndex = 31;
            // 
            // bunifuLabel34
            // 
            this.bunifuLabel34.AllowParentOverrides = false;
            this.bunifuLabel34.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.bunifuLabel34.AutoEllipsis = false;
            this.bunifuLabel34.CursorType = null;
            this.bunifuLabel34.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuLabel34.Location = new System.Drawing.Point(237, 216);
            this.bunifuLabel34.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuLabel34.Name = "bunifuLabel34";
            this.bunifuLabel34.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel34.Size = new System.Drawing.Size(86, 28);
            this.bunifuLabel34.TabIndex = 32;
            this.bunifuLabel34.Text = "Country : ";
            this.bunifuLabel34.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel34.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // emp_country
            // 
            this.emp_country.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.emp_country.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.emp_country.DefaultText = "";
            this.emp_country.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.emp_country.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.emp_country.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.emp_country.DisabledState.Parent = this.emp_country;
            this.emp_country.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.emp_country.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.emp_country.FocusedState.Parent = this.emp_country;
            this.emp_country.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.emp_country.HoverState.Parent = this.emp_country;
            this.emp_country.Location = new System.Drawing.Point(517, 208);
            this.emp_country.Margin = new System.Windows.Forms.Padding(5);
            this.emp_country.Name = "emp_country";
            this.emp_country.PasswordChar = '\0';
            this.emp_country.PlaceholderText = "";
            this.emp_country.SelectedText = "";
            this.emp_country.ShadowDecoration.Parent = this.emp_country;
            this.emp_country.Size = new System.Drawing.Size(267, 44);
            this.emp_country.TabIndex = 33;
            // 
            // bunifuLabel33
            // 
            this.bunifuLabel33.AllowParentOverrides = false;
            this.bunifuLabel33.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuLabel33.AutoEllipsis = false;
            this.bunifuLabel33.CursorType = null;
            this.bunifuLabel33.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuLabel33.Location = new System.Drawing.Point(1006, 216);
            this.bunifuLabel33.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuLabel33.Name = "bunifuLabel33";
            this.bunifuLabel33.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel33.Size = new System.Drawing.Size(86, 28);
            this.bunifuLabel33.TabIndex = 34;
            this.bunifuLabel33.Text = "Address : ";
            this.bunifuLabel33.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel33.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // emp_address
            // 
            this.emp_address.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.emp_address.Location = new System.Drawing.Point(1129, 195);
            this.emp_address.Margin = new System.Windows.Forms.Padding(4);
            this.emp_address.Multiline = true;
            this.emp_address.Name = "emp_address";
            this.emp_address.Size = new System.Drawing.Size(316, 69);
            this.emp_address.TabIndex = 35;
            // 
            // emp_contact
            // 
            this.emp_contact.AllowParentOverrides = false;
            this.emp_contact.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.emp_contact.AutoEllipsis = false;
            this.emp_contact.CursorType = null;
            this.emp_contact.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.emp_contact.Location = new System.Drawing.Point(156, 287);
            this.emp_contact.Margin = new System.Windows.Forms.Padding(4);
            this.emp_contact.Name = "emp_contact";
            this.emp_contact.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.emp_contact.Size = new System.Drawing.Size(167, 28);
            this.emp_contact.TabIndex = 37;
            this.emp_contact.Text = "Contact Number : ";
            this.emp_contact.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.emp_contact.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // emp_email
            // 
            this.emp_email.AllowParentOverrides = false;
            this.emp_email.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.emp_email.AutoEllipsis = false;
            this.emp_email.CursorType = null;
            this.emp_email.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.emp_email.Location = new System.Drawing.Point(1018, 287);
            this.emp_email.Margin = new System.Windows.Forms.Padding(4);
            this.emp_email.Name = "emp_email";
            this.emp_email.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.emp_email.Size = new System.Drawing.Size(63, 28);
            this.emp_email.TabIndex = 38;
            this.emp_email.Text = "Email : ";
            this.emp_email.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.emp_email.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // guna2TextBox1
            // 
            this.guna2TextBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.guna2TextBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox1.DefaultText = "";
            this.guna2TextBox1.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox1.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox1.DisabledState.Parent = this.guna2TextBox1;
            this.guna2TextBox1.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox1.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox1.FocusedState.Parent = this.guna2TextBox1;
            this.guna2TextBox1.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox1.HoverState.Parent = this.guna2TextBox1;
            this.guna2TextBox1.Location = new System.Drawing.Point(517, 279);
            this.guna2TextBox1.Margin = new System.Windows.Forms.Padding(5);
            this.guna2TextBox1.Name = "guna2TextBox1";
            this.guna2TextBox1.PasswordChar = '\0';
            this.guna2TextBox1.PlaceholderText = "";
            this.guna2TextBox1.SelectedText = "";
            this.guna2TextBox1.ShadowDecoration.Parent = this.guna2TextBox1;
            this.guna2TextBox1.Size = new System.Drawing.Size(267, 44);
            this.guna2TextBox1.TabIndex = 39;
            // 
            // guna2TextBox2
            // 
            this.guna2TextBox2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.guna2TextBox2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox2.DefaultText = "";
            this.guna2TextBox2.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox2.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox2.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox2.DisabledState.Parent = this.guna2TextBox2;
            this.guna2TextBox2.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox2.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox2.FocusedState.Parent = this.guna2TextBox2;
            this.guna2TextBox2.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox2.HoverState.Parent = this.guna2TextBox2;
            this.guna2TextBox2.Location = new System.Drawing.Point(1130, 279);
            this.guna2TextBox2.Margin = new System.Windows.Forms.Padding(5);
            this.guna2TextBox2.Name = "guna2TextBox2";
            this.guna2TextBox2.PasswordChar = '\0';
            this.guna2TextBox2.PlaceholderText = "";
            this.guna2TextBox2.SelectedText = "";
            this.guna2TextBox2.ShadowDecoration.Parent = this.guna2TextBox2;
            this.guna2TextBox2.Size = new System.Drawing.Size(267, 44);
            this.guna2TextBox2.TabIndex = 40;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.69136F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65.30864F));
            this.tableLayoutPanel7.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 2);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(1523, 62);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label27);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(4, 4);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(520, 54);
            this.panel1.TabIndex = 0;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(19, 16);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(157, 28);
            this.label27.TabIndex = 0;
            this.label27.Text = "Add Employees";
            // 
            // tabPage3
            // 
            this.tabPage3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabPage3.BackgroundImage")));
            this.tabPage3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage3.Location = new System.Drawing.Point(4, 4);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage3.Size = new System.Drawing.Size(1529, 833);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "tabPage3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Location = new System.Drawing.Point(4, 4);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1529, 833);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "tabPage4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.dataGridView1);
            this.tabPage5.Controls.Add(this.tableLayoutPanel11);
            this.tabPage5.Controls.Add(this.tableLayoutPanel10);
            this.tabPage5.Controls.Add(this.tableLayoutPanel9);
            this.tabPage5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage5.Location = new System.Drawing.Point(4, 4);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1529, 833);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "tabPage5";
            this.tabPage5.UseVisualStyleBackColor = true;
            this.tabPage5.Click += new System.EventHandler(this.tabPage5_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(3, 477);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1520, 350);
            this.dataGridView1.TabIndex = 3;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel11.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Controls.Add(this.addMedicine, 0, 0);
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 409);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(1520, 63);
            this.tableLayoutPanel11.TabIndex = 2;
            // 
            // addMedicine
            // 
            this.addMedicine.BorderRadius = 20;
            this.addMedicine.CheckedState.Parent = this.addMedicine;
            this.addMedicine.CustomImages.Parent = this.addMedicine;
            this.addMedicine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addMedicine.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(50)))), ((int)(((byte)(150)))));
            this.addMedicine.FillColor2 = System.Drawing.Color.White;
            this.addMedicine.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.addMedicine.ForeColor = System.Drawing.Color.White;
            this.addMedicine.HoverState.Parent = this.addMedicine;
            this.addMedicine.Location = new System.Drawing.Point(3, 3);
            this.addMedicine.Name = "addMedicine";
            this.addMedicine.ShadowDecoration.Parent = this.addMedicine;
            this.addMedicine.Size = new System.Drawing.Size(1514, 57);
            this.addMedicine.TabIndex = 18;
            this.addMedicine.Text = "Add Medicine";
            this.addMedicine.Click += new System.EventHandler(this.addMedicine_Click);
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel10.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel10.ColumnCount = 4;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel10.Controls.Add(this.expiryDate, 3, 2);
            this.tableLayoutPanel10.Controls.Add(this.manufacturingDate, 3, 1);
            this.tableLayoutPanel10.Controls.Add(this.label30, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.label32, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.label33, 0, 3);
            this.tableLayoutPanel10.Controls.Add(this.medicineCompanyName, 2, 0);
            this.tableLayoutPanel10.Controls.Add(this.medicineManufacturinfgDate, 2, 1);
            this.tableLayoutPanel10.Controls.Add(this.medicineExpiryDate, 2, 2);
            this.tableLayoutPanel10.Controls.Add(this.medicineDescription, 2, 3);
            this.tableLayoutPanel10.Controls.Add(this.label31, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.medicineName, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.medicinePurchasePrice, 1, 2);
            this.tableLayoutPanel10.Controls.Add(this.medicineSalePrice, 1, 3);
            this.tableLayoutPanel10.Controls.Add(this.companyNameCombobox, 3, 0);
            this.tableLayoutPanel10.Controls.Add(this.description, 3, 3);
            this.tableLayoutPanel10.Controls.Add(this.medicineTypeComboBox, 1, 1);
            this.tableLayoutPanel10.Location = new System.Drawing.Point(3, 109);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 5;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(1520, 296);
            this.tableLayoutPanel10.TabIndex = 1;
            // 
            // expiryDate
            // 
            this.expiryDate.CheckedState.Parent = this.expiryDate;
            this.expiryDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.expiryDate.FillColor = System.Drawing.Color.LightSteelBlue;
            this.expiryDate.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.expiryDate.HoverState.Parent = this.expiryDate;
            this.expiryDate.Location = new System.Drawing.Point(1143, 121);
            this.expiryDate.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.expiryDate.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.expiryDate.Name = "expiryDate";
            this.expiryDate.ShadowDecoration.Parent = this.expiryDate;
            this.expiryDate.Size = new System.Drawing.Size(374, 53);
            this.expiryDate.TabIndex = 5;
            this.expiryDate.Value = new System.DateTime(2022, 4, 15, 20, 19, 30, 517);
            // 
            // manufacturingDate
            // 
            this.manufacturingDate.CheckedState.Parent = this.manufacturingDate;
            this.manufacturingDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.manufacturingDate.FillColor = System.Drawing.Color.LightSteelBlue;
            this.manufacturingDate.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.manufacturingDate.HoverState.Parent = this.manufacturingDate;
            this.manufacturingDate.Location = new System.Drawing.Point(1143, 62);
            this.manufacturingDate.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.manufacturingDate.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.manufacturingDate.Name = "manufacturingDate";
            this.manufacturingDate.ShadowDecoration.Parent = this.manufacturingDate;
            this.manufacturingDate.Size = new System.Drawing.Size(374, 53);
            this.manufacturingDate.TabIndex = 4;
            this.manufacturingDate.Value = new System.DateTime(2022, 4, 15, 20, 19, 30, 517);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(3, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(374, 59);
            this.label30.TabIndex = 2;
            this.label30.Text = "Medicine Name";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(3, 118);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(374, 59);
            this.label32.TabIndex = 4;
            this.label32.Text = "Purchase Price";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(3, 177);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(374, 59);
            this.label33.TabIndex = 5;
            this.label33.Text = "Sale Price";
            // 
            // medicineCompanyName
            // 
            this.medicineCompanyName.AutoSize = true;
            this.medicineCompanyName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.medicineCompanyName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.medicineCompanyName.Location = new System.Drawing.Point(763, 0);
            this.medicineCompanyName.Name = "medicineCompanyName";
            this.medicineCompanyName.Size = new System.Drawing.Size(374, 59);
            this.medicineCompanyName.TabIndex = 6;
            this.medicineCompanyName.Text = "Company Name";
            // 
            // medicineManufacturinfgDate
            // 
            this.medicineManufacturinfgDate.AutoSize = true;
            this.medicineManufacturinfgDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.medicineManufacturinfgDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.medicineManufacturinfgDate.Location = new System.Drawing.Point(763, 59);
            this.medicineManufacturinfgDate.Name = "medicineManufacturinfgDate";
            this.medicineManufacturinfgDate.Size = new System.Drawing.Size(374, 59);
            this.medicineManufacturinfgDate.TabIndex = 7;
            this.medicineManufacturinfgDate.Text = "Manufacturing Date";
            // 
            // medicineExpiryDate
            // 
            this.medicineExpiryDate.AutoSize = true;
            this.medicineExpiryDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.medicineExpiryDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.medicineExpiryDate.Location = new System.Drawing.Point(763, 118);
            this.medicineExpiryDate.Name = "medicineExpiryDate";
            this.medicineExpiryDate.Size = new System.Drawing.Size(374, 59);
            this.medicineExpiryDate.TabIndex = 8;
            this.medicineExpiryDate.Text = "Expiry Date";
            // 
            // medicineDescription
            // 
            this.medicineDescription.AutoSize = true;
            this.medicineDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.medicineDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.medicineDescription.Location = new System.Drawing.Point(763, 177);
            this.medicineDescription.Name = "medicineDescription";
            this.medicineDescription.Size = new System.Drawing.Size(374, 59);
            this.medicineDescription.TabIndex = 9;
            this.medicineDescription.Text = "Description";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(3, 59);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(374, 59);
            this.label31.TabIndex = 3;
            this.label31.Text = "Medicine Type";
            // 
            // medicineName
            // 
            this.medicineName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.medicineName.DefaultText = "";
            this.medicineName.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.medicineName.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.medicineName.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.medicineName.DisabledState.Parent = this.medicineName;
            this.medicineName.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.medicineName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.medicineName.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.medicineName.FocusedState.Parent = this.medicineName;
            this.medicineName.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.medicineName.HoverState.Parent = this.medicineName;
            this.medicineName.Location = new System.Drawing.Point(385, 6);
            this.medicineName.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.medicineName.Name = "medicineName";
            this.medicineName.PasswordChar = '\0';
            this.medicineName.PlaceholderText = "enter medicine name";
            this.medicineName.SelectedText = "";
            this.medicineName.ShadowDecoration.Parent = this.medicineName;
            this.medicineName.Size = new System.Drawing.Size(370, 47);
            this.medicineName.TabIndex = 10;
            // 
            // medicinePurchasePrice
            // 
            this.medicinePurchasePrice.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.medicinePurchasePrice.DefaultText = "";
            this.medicinePurchasePrice.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.medicinePurchasePrice.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.medicinePurchasePrice.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.medicinePurchasePrice.DisabledState.Parent = this.medicinePurchasePrice;
            this.medicinePurchasePrice.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.medicinePurchasePrice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.medicinePurchasePrice.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.medicinePurchasePrice.FocusedState.Parent = this.medicinePurchasePrice;
            this.medicinePurchasePrice.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.medicinePurchasePrice.HoverState.Parent = this.medicinePurchasePrice;
            this.medicinePurchasePrice.Location = new System.Drawing.Point(385, 124);
            this.medicinePurchasePrice.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.medicinePurchasePrice.Name = "medicinePurchasePrice";
            this.medicinePurchasePrice.PasswordChar = '\0';
            this.medicinePurchasePrice.PlaceholderText = "enter purchase price";
            this.medicinePurchasePrice.SelectedText = "";
            this.medicinePurchasePrice.ShadowDecoration.Parent = this.medicinePurchasePrice;
            this.medicinePurchasePrice.Size = new System.Drawing.Size(370, 47);
            this.medicinePurchasePrice.TabIndex = 12;
            // 
            // medicineSalePrice
            // 
            this.medicineSalePrice.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.medicineSalePrice.DefaultText = "";
            this.medicineSalePrice.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.medicineSalePrice.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.medicineSalePrice.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.medicineSalePrice.DisabledState.Parent = this.medicineSalePrice;
            this.medicineSalePrice.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.medicineSalePrice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.medicineSalePrice.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.medicineSalePrice.FocusedState.Parent = this.medicineSalePrice;
            this.medicineSalePrice.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.medicineSalePrice.HoverState.Parent = this.medicineSalePrice;
            this.medicineSalePrice.Location = new System.Drawing.Point(385, 183);
            this.medicineSalePrice.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.medicineSalePrice.Name = "medicineSalePrice";
            this.medicineSalePrice.PasswordChar = '\0';
            this.medicineSalePrice.PlaceholderText = "enter sale price";
            this.medicineSalePrice.SelectedText = "";
            this.medicineSalePrice.ShadowDecoration.Parent = this.medicineSalePrice;
            this.medicineSalePrice.Size = new System.Drawing.Size(370, 47);
            this.medicineSalePrice.TabIndex = 0;
            // 
            // companyNameCombobox
            // 
            this.companyNameCombobox.BackColor = System.Drawing.Color.Transparent;
            this.companyNameCombobox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.companyNameCombobox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.companyNameCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.companyNameCombobox.FocusedColor = System.Drawing.Color.Empty;
            this.companyNameCombobox.FocusedState.Parent = this.companyNameCombobox;
            this.companyNameCombobox.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.companyNameCombobox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.companyNameCombobox.FormattingEnabled = true;
            this.companyNameCombobox.HoverState.Parent = this.companyNameCombobox;
            this.companyNameCombobox.ItemHeight = 30;
            this.companyNameCombobox.Items.AddRange(new object[] {
            "select company name"});
            this.companyNameCombobox.ItemsAppearance.Parent = this.companyNameCombobox;
            this.companyNameCombobox.Location = new System.Drawing.Point(1143, 3);
            this.companyNameCombobox.Name = "companyNameCombobox";
            this.companyNameCombobox.ShadowDecoration.Parent = this.companyNameCombobox;
            this.companyNameCombobox.Size = new System.Drawing.Size(374, 36);
            this.companyNameCombobox.TabIndex = 18;
            // 
            // description
            // 
            this.description.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.description.DefaultText = "";
            this.description.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.description.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.description.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.description.DisabledState.Parent = this.description;
            this.description.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.description.Dock = System.Windows.Forms.DockStyle.Fill;
            this.description.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.description.FocusedState.Parent = this.description;
            this.description.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.description.HoverState.Parent = this.description;
            this.description.Location = new System.Drawing.Point(1146, 185);
            this.description.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.description.Name = "description";
            this.description.PasswordChar = '\0';
            this.description.PlaceholderText = "enter medicine description";
            this.description.SelectedText = "";
            this.description.ShadowDecoration.Parent = this.description;
            this.description.Size = new System.Drawing.Size(368, 43);
            this.description.TabIndex = 19;
            // 
            // medicineTypeComboBox
            // 
            this.medicineTypeComboBox.BackColor = System.Drawing.Color.Transparent;
            this.medicineTypeComboBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.medicineTypeComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.medicineTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.medicineTypeComboBox.FocusedColor = System.Drawing.Color.Empty;
            this.medicineTypeComboBox.FocusedState.Parent = this.medicineTypeComboBox;
            this.medicineTypeComboBox.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.medicineTypeComboBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.medicineTypeComboBox.FormattingEnabled = true;
            this.medicineTypeComboBox.HoverState.Parent = this.medicineTypeComboBox;
            this.medicineTypeComboBox.ItemHeight = 30;
            this.medicineTypeComboBox.Items.AddRange(new object[] {
            "select medicine type"});
            this.medicineTypeComboBox.ItemsAppearance.Parent = this.medicineTypeComboBox;
            this.medicineTypeComboBox.Location = new System.Drawing.Point(383, 62);
            this.medicineTypeComboBox.Name = "medicineTypeComboBox";
            this.medicineTypeComboBox.ShadowDecoration.Parent = this.medicineTypeComboBox;
            this.medicineTypeComboBox.Size = new System.Drawing.Size(374, 36);
            this.medicineTypeComboBox.TabIndex = 20;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Controls.Add(this.label28, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.label29, 0, 1);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 2;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(1523, 100);
            this.tableLayoutPanel9.TabIndex = 0;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(3, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(329, 46);
            this.label28.TabIndex = 0;
            this.label28.Text = "Medicine Details";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(3, 50);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(251, 32);
            this.label29.TabIndex = 1;
            this.label29.Text = "Add New Medicine";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.tableLayoutPanel15);
            this.tabPage6.Controls.Add(this.tableLayoutPanel14);
            this.tabPage6.Controls.Add(this.tableLayoutPanel13);
            this.tabPage6.Controls.Add(this.tableLayoutPanel12);
            this.tabPage6.Location = new System.Drawing.Point(4, 4);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(1529, 833);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "tabPage6";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 2;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Controls.Add(this.guna2GradientTileButton3, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.guna2GradientTileButton2, 0, 0);
            this.tableLayoutPanel15.Location = new System.Drawing.Point(7, 548);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 1;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(599, 61);
            this.tableLayoutPanel15.TabIndex = 3;
            // 
            // guna2GradientTileButton3
            // 
            this.guna2GradientTileButton3.BorderRadius = 35;
            this.guna2GradientTileButton3.CheckedState.Parent = this.guna2GradientTileButton3;
            this.guna2GradientTileButton3.CustomImages.Parent = this.guna2GradientTileButton3;
            this.guna2GradientTileButton3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.guna2GradientTileButton3.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(50)))), ((int)(((byte)(150)))));
            this.guna2GradientTileButton3.FillColor2 = System.Drawing.Color.SteelBlue;
            this.guna2GradientTileButton3.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2GradientTileButton3.ForeColor = System.Drawing.Color.White;
            this.guna2GradientTileButton3.HoverState.Parent = this.guna2GradientTileButton3;
            this.guna2GradientTileButton3.Location = new System.Drawing.Point(302, 3);
            this.guna2GradientTileButton3.Name = "guna2GradientTileButton3";
            this.guna2GradientTileButton3.ShadowDecoration.Parent = this.guna2GradientTileButton3;
            this.guna2GradientTileButton3.Size = new System.Drawing.Size(294, 55);
            this.guna2GradientTileButton3.TabIndex = 3;
            this.guna2GradientTileButton3.Text = "Delete";
            this.guna2GradientTileButton3.Click += new System.EventHandler(this.guna2GradientTileButton3_Click);
            // 
            // guna2GradientTileButton2
            // 
            this.guna2GradientTileButton2.BorderRadius = 34;
            this.guna2GradientTileButton2.CheckedState.Parent = this.guna2GradientTileButton2;
            this.guna2GradientTileButton2.CustomImages.Parent = this.guna2GradientTileButton2;
            this.guna2GradientTileButton2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.guna2GradientTileButton2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(50)))), ((int)(((byte)(150)))));
            this.guna2GradientTileButton2.FillColor2 = System.Drawing.Color.SteelBlue;
            this.guna2GradientTileButton2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2GradientTileButton2.ForeColor = System.Drawing.Color.White;
            this.guna2GradientTileButton2.HoverState.Parent = this.guna2GradientTileButton2;
            this.guna2GradientTileButton2.Location = new System.Drawing.Point(3, 3);
            this.guna2GradientTileButton2.Name = "guna2GradientTileButton2";
            this.guna2GradientTileButton2.ShadowDecoration.Parent = this.guna2GradientTileButton2;
            this.guna2GradientTileButton2.Size = new System.Drawing.Size(293, 55);
            this.guna2GradientTileButton2.TabIndex = 2;
            this.guna2GradientTileButton2.Text = "update";
            this.guna2GradientTileButton2.Click += new System.EventHandler(this.guna2GradientTileButton2_Click);
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel14.ColumnCount = 1;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.Controls.Add(this.dataGridView2, 0, 0);
            this.tableLayoutPanel14.Location = new System.Drawing.Point(7, 185);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 1;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(1516, 357);
            this.tableLayoutPanel14.TabIndex = 2;
            // 
            // dataGridView2
            // 
            this.dataGridView2.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(3, 3);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersWidth = 51;
            this.dataGridView2.RowTemplate.Height = 24;
            this.dataGridView2.Size = new System.Drawing.Size(1510, 351);
            this.dataGridView2.TabIndex = 0;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 2;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel13.Controls.Add(this.searchBar, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.guna2GradientTileButton1, 1, 0);
            this.tableLayoutPanel13.Location = new System.Drawing.Point(3, 111);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 1;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(773, 67);
            this.tableLayoutPanel13.TabIndex = 1;
            // 
            // searchBar
            // 
            this.searchBar.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.searchBar.DefaultText = "";
            this.searchBar.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.searchBar.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.searchBar.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.searchBar.DisabledState.Parent = this.searchBar;
            this.searchBar.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.searchBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchBar.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.searchBar.FocusedState.Parent = this.searchBar;
            this.searchBar.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.searchBar.HoverState.Parent = this.searchBar;
            this.searchBar.IconRight = ((System.Drawing.Image)(resources.GetObject("searchBar.IconRight")));
            this.searchBar.Location = new System.Drawing.Point(4, 4);
            this.searchBar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.searchBar.Name = "searchBar";
            this.searchBar.PasswordChar = '\0';
            this.searchBar.PlaceholderText = "search medicine by name";
            this.searchBar.SelectedText = "";
            this.searchBar.ShadowDecoration.Parent = this.searchBar;
            this.searchBar.Size = new System.Drawing.Size(533, 59);
            this.searchBar.TabIndex = 0;
            // 
            // guna2GradientTileButton1
            // 
            this.guna2GradientTileButton1.CheckedState.Parent = this.guna2GradientTileButton1;
            this.guna2GradientTileButton1.CustomImages.Parent = this.guna2GradientTileButton1;
            this.guna2GradientTileButton1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.guna2GradientTileButton1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(50)))), ((int)(((byte)(150)))));
            this.guna2GradientTileButton1.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.guna2GradientTileButton1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2GradientTileButton1.ForeColor = System.Drawing.Color.White;
            this.guna2GradientTileButton1.HoverState.Parent = this.guna2GradientTileButton1;
            this.guna2GradientTileButton1.Location = new System.Drawing.Point(544, 3);
            this.guna2GradientTileButton1.Name = "guna2GradientTileButton1";
            this.guna2GradientTileButton1.ShadowDecoration.Parent = this.guna2GradientTileButton1;
            this.guna2GradientTileButton1.Size = new System.Drawing.Size(226, 61);
            this.guna2GradientTileButton1.TabIndex = 1;
            this.guna2GradientTileButton1.Text = "search";
            this.guna2GradientTileButton1.Click += new System.EventHandler(this.guna2GradientTileButton1_Click);
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 1;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Controls.Add(this.guna2HtmlLabel1, 0, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(1523, 105);
            this.tableLayoutPanel12.TabIndex = 0;
            // 
            // guna2HtmlLabel1
            // 
            this.guna2HtmlLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.guna2HtmlLabel1.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel1.Location = new System.Drawing.Point(556, 24);
            this.guna2HtmlLabel1.Name = "guna2HtmlLabel1";
            this.guna2HtmlLabel1.Size = new System.Drawing.Size(411, 56);
            this.guna2HtmlLabel1.TabIndex = 0;
            this.guna2HtmlLabel1.Text = "Manage Medicines";
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.tableLayoutPanel19);
            this.tabPage7.Controls.Add(this.tableLayoutPanel18);
            this.tabPage7.Controls.Add(this.tableLayoutPanel17);
            this.tabPage7.Controls.Add(this.tableLayoutPanel16);
            this.tabPage7.Location = new System.Drawing.Point(4, 4);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(1529, 833);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "tabPage7";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel19.ColumnCount = 1;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.Controls.Add(this.dataGridView3, 0, 0);
            this.tableLayoutPanel19.Location = new System.Drawing.Point(6, 383);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 1;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 444F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(1515, 444);
            this.tableLayoutPanel19.TabIndex = 4;
            // 
            // dataGridView3
            // 
            this.dataGridView3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView3.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(3, 3);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.RowHeadersWidth = 51;
            this.dataGridView3.RowTemplate.Height = 24;
            this.dataGridView3.Size = new System.Drawing.Size(1509, 438);
            this.dataGridView3.TabIndex = 4;
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel18.ColumnCount = 1;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.Controls.Add(this.guna2GradientButton1, 0, 0);
            this.tableLayoutPanel18.Location = new System.Drawing.Point(6, 301);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 1;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(1517, 75);
            this.tableLayoutPanel18.TabIndex = 3;
            // 
            // guna2GradientButton1
            // 
            this.guna2GradientButton1.BorderRadius = 20;
            this.guna2GradientButton1.CheckedState.Parent = this.guna2GradientButton1;
            this.guna2GradientButton1.CustomImages.Parent = this.guna2GradientButton1;
            this.guna2GradientButton1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.guna2GradientButton1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(50)))), ((int)(((byte)(150)))));
            this.guna2GradientButton1.FillColor2 = System.Drawing.Color.White;
            this.guna2GradientButton1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2GradientButton1.ForeColor = System.Drawing.Color.White;
            this.guna2GradientButton1.HoverState.Parent = this.guna2GradientButton1;
            this.guna2GradientButton1.Location = new System.Drawing.Point(3, 3);
            this.guna2GradientButton1.Name = "guna2GradientButton1";
            this.guna2GradientButton1.ShadowDecoration.Parent = this.guna2GradientButton1;
            this.guna2GradientButton1.Size = new System.Drawing.Size(1511, 69);
            this.guna2GradientButton1.TabIndex = 19;
            this.guna2GradientButton1.Text = "add new company";
            this.guna2GradientButton1.Click += new System.EventHandler(this.guna2GradientButton1_Click);
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel17.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel17.ColumnCount = 4;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel17.Controls.Add(this.companyEmail, 3, 2);
            this.tableLayoutPanel17.Controls.Add(this.label36, 0, 2);
            this.tableLayoutPanel17.Controls.Add(this.label37, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.label38, 0, 1);
            this.tableLayoutPanel17.Controls.Add(this.label41, 2, 0);
            this.tableLayoutPanel17.Controls.Add(this.label42, 2, 1);
            this.tableLayoutPanel17.Controls.Add(this.companyName, 1, 0);
            this.tableLayoutPanel17.Controls.Add(this.companyCity, 1, 1);
            this.tableLayoutPanel17.Controls.Add(this.companyContactNumber, 3, 1);
            this.tableLayoutPanel17.Controls.Add(this.label43, 2, 2);
            this.tableLayoutPanel17.Controls.Add(this.companyCountry, 1, 2);
            this.tableLayoutPanel17.Controls.Add(this.companyAddress, 3, 0);
            this.tableLayoutPanel17.Location = new System.Drawing.Point(4, 110);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 3;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.34F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(1520, 185);
            this.tableLayoutPanel17.TabIndex = 2;
            // 
            // companyEmail
            // 
            this.companyEmail.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.companyEmail.DefaultText = "";
            this.companyEmail.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.companyEmail.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.companyEmail.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.companyEmail.DisabledState.Parent = this.companyEmail;
            this.companyEmail.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.companyEmail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.companyEmail.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.companyEmail.FocusedState.Parent = this.companyEmail;
            this.companyEmail.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.companyEmail.HoverState.Parent = this.companyEmail;
            this.companyEmail.Location = new System.Drawing.Point(1144, 126);
            this.companyEmail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.companyEmail.Name = "companyEmail";
            this.companyEmail.PasswordChar = '\0';
            this.companyEmail.PlaceholderText = "enter company email";
            this.companyEmail.SelectedText = "";
            this.companyEmail.ShadowDecoration.Parent = this.companyEmail;
            this.companyEmail.Size = new System.Drawing.Size(372, 55);
            this.companyEmail.TabIndex = 23;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(3, 122);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(374, 63);
            this.label36.TabIndex = 20;
            this.label36.Text = "Country";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(3, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(374, 61);
            this.label37.TabIndex = 4;
            this.label37.Text = "Company Name";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(3, 61);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(374, 61);
            this.label38.TabIndex = 5;
            this.label38.Text = "City";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(763, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(374, 61);
            this.label41.TabIndex = 8;
            this.label41.Text = "Address";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(763, 61);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(374, 61);
            this.label42.TabIndex = 9;
            this.label42.Text = "Contact Number";
            // 
            // companyName
            // 
            this.companyName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.companyName.DefaultText = "";
            this.companyName.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.companyName.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.companyName.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.companyName.DisabledState.Parent = this.companyName;
            this.companyName.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.companyName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.companyName.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.companyName.FocusedState.Parent = this.companyName;
            this.companyName.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.companyName.HoverState.Parent = this.companyName;
            this.companyName.Location = new System.Drawing.Point(385, 6);
            this.companyName.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.companyName.Name = "companyName";
            this.companyName.PasswordChar = '\0';
            this.companyName.PlaceholderText = "enter company name";
            this.companyName.SelectedText = "";
            this.companyName.ShadowDecoration.Parent = this.companyName;
            this.companyName.Size = new System.Drawing.Size(370, 49);
            this.companyName.TabIndex = 12;
            // 
            // companyCity
            // 
            this.companyCity.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.companyCity.DefaultText = "";
            this.companyCity.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.companyCity.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.companyCity.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.companyCity.DisabledState.Parent = this.companyCity;
            this.companyCity.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.companyCity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.companyCity.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.companyCity.FocusedState.Parent = this.companyCity;
            this.companyCity.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.companyCity.HoverState.Parent = this.companyCity;
            this.companyCity.Location = new System.Drawing.Point(385, 67);
            this.companyCity.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.companyCity.Name = "companyCity";
            this.companyCity.PasswordChar = '\0';
            this.companyCity.PlaceholderText = "enter company city";
            this.companyCity.SelectedText = "";
            this.companyCity.ShadowDecoration.Parent = this.companyCity;
            this.companyCity.Size = new System.Drawing.Size(370, 49);
            this.companyCity.TabIndex = 0;
            // 
            // companyContactNumber
            // 
            this.companyContactNumber.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.companyContactNumber.DefaultText = "";
            this.companyContactNumber.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.companyContactNumber.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.companyContactNumber.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.companyContactNumber.DisabledState.Parent = this.companyContactNumber;
            this.companyContactNumber.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.companyContactNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.companyContactNumber.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.companyContactNumber.FocusedState.Parent = this.companyContactNumber;
            this.companyContactNumber.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.companyContactNumber.HoverState.Parent = this.companyContactNumber;
            this.companyContactNumber.Location = new System.Drawing.Point(1146, 69);
            this.companyContactNumber.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.companyContactNumber.Name = "companyContactNumber";
            this.companyContactNumber.PasswordChar = '\0';
            this.companyContactNumber.PlaceholderText = "enter medicine description";
            this.companyContactNumber.SelectedText = "";
            this.companyContactNumber.ShadowDecoration.Parent = this.companyContactNumber;
            this.companyContactNumber.Size = new System.Drawing.Size(368, 45);
            this.companyContactNumber.TabIndex = 19;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(763, 122);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(374, 63);
            this.label43.TabIndex = 21;
            this.label43.Text = "Email";
            // 
            // companyCountry
            // 
            this.companyCountry.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.companyCountry.DefaultText = "";
            this.companyCountry.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.companyCountry.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.companyCountry.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.companyCountry.DisabledState.Parent = this.companyCountry;
            this.companyCountry.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.companyCountry.Dock = System.Windows.Forms.DockStyle.Fill;
            this.companyCountry.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.companyCountry.FocusedState.Parent = this.companyCountry;
            this.companyCountry.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.companyCountry.HoverState.Parent = this.companyCountry;
            this.companyCountry.Location = new System.Drawing.Point(384, 126);
            this.companyCountry.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.companyCountry.Name = "companyCountry";
            this.companyCountry.PasswordChar = '\0';
            this.companyCountry.PlaceholderText = "enter country";
            this.companyCountry.SelectedText = "";
            this.companyCountry.ShadowDecoration.Parent = this.companyCountry;
            this.companyCountry.Size = new System.Drawing.Size(372, 55);
            this.companyCountry.TabIndex = 22;
            // 
            // companyAddress
            // 
            this.companyAddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.companyAddress.DefaultText = "";
            this.companyAddress.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.companyAddress.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.companyAddress.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.companyAddress.DisabledState.Parent = this.companyAddress;
            this.companyAddress.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.companyAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.companyAddress.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.companyAddress.FocusedState.Parent = this.companyAddress;
            this.companyAddress.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.companyAddress.HoverState.Parent = this.companyAddress;
            this.companyAddress.Location = new System.Drawing.Point(1144, 4);
            this.companyAddress.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.companyAddress.Name = "companyAddress";
            this.companyAddress.PasswordChar = '\0';
            this.companyAddress.PlaceholderText = "enter company address";
            this.companyAddress.SelectedText = "";
            this.companyAddress.ShadowDecoration.Parent = this.companyAddress;
            this.companyAddress.Size = new System.Drawing.Size(372, 53);
            this.companyAddress.TabIndex = 24;
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel16.ColumnCount = 1;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.Controls.Add(this.label34, 0, 0);
            this.tableLayoutPanel16.Controls.Add(this.label35, 0, 1);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 2;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(1523, 100);
            this.tableLayoutPanel16.TabIndex = 1;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(3, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(339, 46);
            this.label34.TabIndex = 0;
            this.label34.Text = "Company Details";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(3, 50);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(257, 32);
            this.label35.TabIndex = 1;
            this.label35.Text = "Add New Company";
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.tableLayoutPanel23);
            this.tabPage8.Controls.Add(this.tableLayoutPanel22);
            this.tabPage8.Controls.Add(this.tableLayoutPanel21);
            this.tabPage8.Controls.Add(this.tableLayoutPanel20);
            this.tabPage8.Location = new System.Drawing.Point(4, 4);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(1529, 833);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "tabPage8";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel23
            // 
            this.tableLayoutPanel23.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel23.ColumnCount = 1;
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.Controls.Add(this.dataGridView4, 0, 0);
            this.tableLayoutPanel23.Location = new System.Drawing.Point(7, 331);
            this.tableLayoutPanel23.Name = "tableLayoutPanel23";
            this.tableLayoutPanel23.RowCount = 1;
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.Size = new System.Drawing.Size(1516, 496);
            this.tableLayoutPanel23.TabIndex = 5;
            // 
            // dataGridView4
            // 
            this.dataGridView4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView4.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Location = new System.Drawing.Point(3, 3);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.RowHeadersWidth = 51;
            this.dataGridView4.RowTemplate.Height = 24;
            this.dataGridView4.Size = new System.Drawing.Size(1510, 490);
            this.dataGridView4.TabIndex = 5;
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 1;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.Controls.Add(this.guna2GradientButton2, 0, 0);
            this.tableLayoutPanel22.Location = new System.Drawing.Point(7, 255);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 1;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(1517, 69);
            this.tableLayoutPanel22.TabIndex = 4;
            // 
            // guna2GradientButton2
            // 
            this.guna2GradientButton2.BorderRadius = 20;
            this.guna2GradientButton2.CheckedState.Parent = this.guna2GradientButton2;
            this.guna2GradientButton2.CustomImages.Parent = this.guna2GradientButton2;
            this.guna2GradientButton2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.guna2GradientButton2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(50)))), ((int)(((byte)(150)))));
            this.guna2GradientButton2.FillColor2 = System.Drawing.Color.White;
            this.guna2GradientButton2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2GradientButton2.ForeColor = System.Drawing.Color.White;
            this.guna2GradientButton2.HoverState.Parent = this.guna2GradientButton2;
            this.guna2GradientButton2.Location = new System.Drawing.Point(3, 3);
            this.guna2GradientButton2.Name = "guna2GradientButton2";
            this.guna2GradientButton2.ShadowDecoration.Parent = this.guna2GradientButton2;
            this.guna2GradientButton2.Size = new System.Drawing.Size(1511, 63);
            this.guna2GradientButton2.TabIndex = 20;
            this.guna2GradientButton2.Text = "Add Supplier";
            this.guna2GradientButton2.Click += new System.EventHandler(this.guna2GradientButton2_Click);
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel21.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel21.ColumnCount = 4;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel21.Controls.Add(this.label44, 0, 0);
            this.tableLayoutPanel21.Controls.Add(this.label45, 0, 1);
            this.tableLayoutPanel21.Controls.Add(this.label46, 2, 0);
            this.tableLayoutPanel21.Controls.Add(this.label47, 2, 1);
            this.tableLayoutPanel21.Controls.Add(this.shipperName, 1, 0);
            this.tableLayoutPanel21.Controls.Add(this.shipperContactNumber, 1, 1);
            this.tableLayoutPanel21.Controls.Add(this.shipperAddress, 3, 1);
            this.tableLayoutPanel21.Controls.Add(this.shipperCompanyNameComboBox, 3, 0);
            this.tableLayoutPanel21.Location = new System.Drawing.Point(4, 109);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 2;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(1520, 136);
            this.tableLayoutPanel21.TabIndex = 3;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(3, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(374, 68);
            this.label44.TabIndex = 4;
            this.label44.Text = "Shipper Name";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(3, 68);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(374, 68);
            this.label45.TabIndex = 5;
            this.label45.Text = "Contact Number";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(763, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(374, 68);
            this.label46.TabIndex = 8;
            this.label46.Text = "Company Name";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(763, 68);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(374, 68);
            this.label47.TabIndex = 9;
            this.label47.Text = "Address";
            // 
            // shipperName
            // 
            this.shipperName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.shipperName.DefaultText = "";
            this.shipperName.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.shipperName.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.shipperName.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.shipperName.DisabledState.Parent = this.shipperName;
            this.shipperName.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.shipperName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.shipperName.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.shipperName.FocusedState.Parent = this.shipperName;
            this.shipperName.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.shipperName.HoverState.Parent = this.shipperName;
            this.shipperName.Location = new System.Drawing.Point(385, 6);
            this.shipperName.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.shipperName.Name = "shipperName";
            this.shipperName.PasswordChar = '\0';
            this.shipperName.PlaceholderText = "enter shipper name";
            this.shipperName.SelectedText = "";
            this.shipperName.ShadowDecoration.Parent = this.shipperName;
            this.shipperName.Size = new System.Drawing.Size(370, 56);
            this.shipperName.TabIndex = 12;
            // 
            // shipperContactNumber
            // 
            this.shipperContactNumber.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.shipperContactNumber.DefaultText = "";
            this.shipperContactNumber.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.shipperContactNumber.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.shipperContactNumber.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.shipperContactNumber.DisabledState.Parent = this.shipperContactNumber;
            this.shipperContactNumber.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.shipperContactNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.shipperContactNumber.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.shipperContactNumber.FocusedState.Parent = this.shipperContactNumber;
            this.shipperContactNumber.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.shipperContactNumber.HoverState.Parent = this.shipperContactNumber;
            this.shipperContactNumber.Location = new System.Drawing.Point(385, 74);
            this.shipperContactNumber.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.shipperContactNumber.Name = "shipperContactNumber";
            this.shipperContactNumber.PasswordChar = '\0';
            this.shipperContactNumber.PlaceholderText = "enter contact number";
            this.shipperContactNumber.SelectedText = "";
            this.shipperContactNumber.ShadowDecoration.Parent = this.shipperContactNumber;
            this.shipperContactNumber.Size = new System.Drawing.Size(370, 56);
            this.shipperContactNumber.TabIndex = 0;
            // 
            // shipperAddress
            // 
            this.shipperAddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.shipperAddress.DefaultText = "";
            this.shipperAddress.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.shipperAddress.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.shipperAddress.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.shipperAddress.DisabledState.Parent = this.shipperAddress;
            this.shipperAddress.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.shipperAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.shipperAddress.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.shipperAddress.FocusedState.Parent = this.shipperAddress;
            this.shipperAddress.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.shipperAddress.HoverState.Parent = this.shipperAddress;
            this.shipperAddress.Location = new System.Drawing.Point(1146, 76);
            this.shipperAddress.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.shipperAddress.Name = "shipperAddress";
            this.shipperAddress.PasswordChar = '\0';
            this.shipperAddress.PlaceholderText = "enter shipper address";
            this.shipperAddress.SelectedText = "";
            this.shipperAddress.ShadowDecoration.Parent = this.shipperAddress;
            this.shipperAddress.Size = new System.Drawing.Size(368, 52);
            this.shipperAddress.TabIndex = 19;
            // 
            // shipperCompanyNameComboBox
            // 
            this.shipperCompanyNameComboBox.BackColor = System.Drawing.Color.Transparent;
            this.shipperCompanyNameComboBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.shipperCompanyNameComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.shipperCompanyNameComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.shipperCompanyNameComboBox.FocusedColor = System.Drawing.Color.Empty;
            this.shipperCompanyNameComboBox.FocusedState.Parent = this.shipperCompanyNameComboBox;
            this.shipperCompanyNameComboBox.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.shipperCompanyNameComboBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.shipperCompanyNameComboBox.FormattingEnabled = true;
            this.shipperCompanyNameComboBox.HoverState.Parent = this.shipperCompanyNameComboBox;
            this.shipperCompanyNameComboBox.ItemHeight = 30;
            this.shipperCompanyNameComboBox.ItemsAppearance.Parent = this.shipperCompanyNameComboBox;
            this.shipperCompanyNameComboBox.Location = new System.Drawing.Point(1143, 3);
            this.shipperCompanyNameComboBox.Name = "shipperCompanyNameComboBox";
            this.shipperCompanyNameComboBox.ShadowDecoration.Parent = this.shipperCompanyNameComboBox;
            this.shipperCompanyNameComboBox.Size = new System.Drawing.Size(374, 36);
            this.shipperCompanyNameComboBox.TabIndex = 20;
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel20.ColumnCount = 1;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.Controls.Add(this.label39, 0, 0);
            this.tableLayoutPanel20.Controls.Add(this.label40, 0, 1);
            this.tableLayoutPanel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel20.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 2;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(1523, 100);
            this.tableLayoutPanel20.TabIndex = 2;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(3, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(305, 46);
            this.label39.TabIndex = 0;
            this.label39.Text = "Shipper Details";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(3, 50);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(235, 32);
            this.label40.TabIndex = 1;
            this.label40.Text = "Add New Shipper";
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.tableLayoutPanel26);
            this.tabPage9.Controls.Add(this.tableLayoutPanel27);
            this.tabPage9.Controls.Add(this.tableLayoutPanel25);
            this.tabPage9.Controls.Add(this.tableLayoutPanel24);
            this.tabPage9.Location = new System.Drawing.Point(4, 4);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(1529, 833);
            this.tabPage9.TabIndex = 8;
            this.tabPage9.Text = "tabPage9";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel26
            // 
            this.tableLayoutPanel26.ColumnCount = 2;
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel26.Controls.Add(this.deleteCompany, 0, 0);
            this.tableLayoutPanel26.Controls.Add(this.updateCompany, 0, 0);
            this.tableLayoutPanel26.Location = new System.Drawing.Point(6, 548);
            this.tableLayoutPanel26.Name = "tableLayoutPanel26";
            this.tableLayoutPanel26.RowCount = 1;
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel26.Size = new System.Drawing.Size(599, 61);
            this.tableLayoutPanel26.TabIndex = 5;
            // 
            // deleteCompany
            // 
            this.deleteCompany.BorderRadius = 35;
            this.deleteCompany.CheckedState.Parent = this.deleteCompany;
            this.deleteCompany.CustomImages.Parent = this.deleteCompany;
            this.deleteCompany.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deleteCompany.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(50)))), ((int)(((byte)(150)))));
            this.deleteCompany.FillColor2 = System.Drawing.Color.SteelBlue;
            this.deleteCompany.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.deleteCompany.ForeColor = System.Drawing.Color.White;
            this.deleteCompany.HoverState.Parent = this.deleteCompany;
            this.deleteCompany.Location = new System.Drawing.Point(302, 3);
            this.deleteCompany.Name = "deleteCompany";
            this.deleteCompany.ShadowDecoration.Parent = this.deleteCompany;
            this.deleteCompany.Size = new System.Drawing.Size(294, 55);
            this.deleteCompany.TabIndex = 3;
            this.deleteCompany.Text = "Delete";
            this.deleteCompany.Click += new System.EventHandler(this.deleteCompany_Click);
            // 
            // updateCompany
            // 
            this.updateCompany.BorderRadius = 34;
            this.updateCompany.CheckedState.Parent = this.updateCompany;
            this.updateCompany.CustomImages.Parent = this.updateCompany;
            this.updateCompany.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updateCompany.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(50)))), ((int)(((byte)(150)))));
            this.updateCompany.FillColor2 = System.Drawing.Color.SteelBlue;
            this.updateCompany.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.updateCompany.ForeColor = System.Drawing.Color.White;
            this.updateCompany.HoverState.Parent = this.updateCompany;
            this.updateCompany.Location = new System.Drawing.Point(3, 3);
            this.updateCompany.Name = "updateCompany";
            this.updateCompany.ShadowDecoration.Parent = this.updateCompany;
            this.updateCompany.Size = new System.Drawing.Size(293, 55);
            this.updateCompany.TabIndex = 2;
            this.updateCompany.Text = "update";
            this.updateCompany.Click += new System.EventHandler(this.updateCompany_Click);
            // 
            // tableLayoutPanel27
            // 
            this.tableLayoutPanel27.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel27.ColumnCount = 1;
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel27.Controls.Add(this.dataGridView5, 0, 0);
            this.tableLayoutPanel27.Location = new System.Drawing.Point(6, 185);
            this.tableLayoutPanel27.Name = "tableLayoutPanel27";
            this.tableLayoutPanel27.RowCount = 1;
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel27.Size = new System.Drawing.Size(1516, 357);
            this.tableLayoutPanel27.TabIndex = 4;
            // 
            // dataGridView5
            // 
            this.dataGridView5.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView5.Location = new System.Drawing.Point(3, 3);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.RowHeadersWidth = 51;
            this.dataGridView5.RowTemplate.Height = 24;
            this.dataGridView5.Size = new System.Drawing.Size(1510, 351);
            this.dataGridView5.TabIndex = 0;
            // 
            // tableLayoutPanel25
            // 
            this.tableLayoutPanel25.ColumnCount = 2;
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel25.Controls.Add(this.searchBar1, 0, 0);
            this.tableLayoutPanel25.Controls.Add(this.searchCompany, 1, 0);
            this.tableLayoutPanel25.Location = new System.Drawing.Point(3, 114);
            this.tableLayoutPanel25.Name = "tableLayoutPanel25";
            this.tableLayoutPanel25.RowCount = 1;
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel25.Size = new System.Drawing.Size(773, 67);
            this.tableLayoutPanel25.TabIndex = 2;
            // 
            // searchBar1
            // 
            this.searchBar1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.searchBar1.DefaultText = "";
            this.searchBar1.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.searchBar1.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.searchBar1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.searchBar1.DisabledState.Parent = this.searchBar1;
            this.searchBar1.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.searchBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchBar1.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.searchBar1.FocusedState.Parent = this.searchBar1;
            this.searchBar1.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.searchBar1.HoverState.Parent = this.searchBar1;
            this.searchBar1.IconRight = ((System.Drawing.Image)(resources.GetObject("searchBar1.IconRight")));
            this.searchBar1.Location = new System.Drawing.Point(4, 4);
            this.searchBar1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.searchBar1.Name = "searchBar1";
            this.searchBar1.PasswordChar = '\0';
            this.searchBar1.PlaceholderText = "search company by name";
            this.searchBar1.SelectedText = "";
            this.searchBar1.ShadowDecoration.Parent = this.searchBar1;
            this.searchBar1.Size = new System.Drawing.Size(533, 59);
            this.searchBar1.TabIndex = 0;
            // 
            // searchCompany
            // 
            this.searchCompany.CheckedState.Parent = this.searchCompany;
            this.searchCompany.CustomImages.Parent = this.searchCompany;
            this.searchCompany.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchCompany.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(50)))), ((int)(((byte)(150)))));
            this.searchCompany.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.searchCompany.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.searchCompany.ForeColor = System.Drawing.Color.White;
            this.searchCompany.HoverState.Parent = this.searchCompany;
            this.searchCompany.Location = new System.Drawing.Point(544, 3);
            this.searchCompany.Name = "searchCompany";
            this.searchCompany.ShadowDecoration.Parent = this.searchCompany;
            this.searchCompany.Size = new System.Drawing.Size(226, 61);
            this.searchCompany.TabIndex = 1;
            this.searchCompany.Text = "search";
            this.searchCompany.Click += new System.EventHandler(this.searchCompany_Click);
            // 
            // tableLayoutPanel24
            // 
            this.tableLayoutPanel24.ColumnCount = 1;
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel24.Controls.Add(this.guna2HtmlLabel2, 0, 0);
            this.tableLayoutPanel24.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel24.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel24.Name = "tableLayoutPanel24";
            this.tableLayoutPanel24.RowCount = 1;
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel24.Size = new System.Drawing.Size(1523, 105);
            this.tableLayoutPanel24.TabIndex = 1;
            // 
            // guna2HtmlLabel2
            // 
            this.guna2HtmlLabel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.guna2HtmlLabel2.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel2.Location = new System.Drawing.Point(543, 24);
            this.guna2HtmlLabel2.Name = "guna2HtmlLabel2";
            this.guna2HtmlLabel2.Size = new System.Drawing.Size(437, 56);
            this.guna2HtmlLabel2.TabIndex = 0;
            this.guna2HtmlLabel2.Text = "Manage Companies";
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.tableLayoutPanel29);
            this.tabPage10.Controls.Add(this.tableLayoutPanel30);
            this.tabPage10.Controls.Add(this.tableLayoutPanel31);
            this.tabPage10.Controls.Add(this.tableLayoutPanel28);
            this.tabPage10.Location = new System.Drawing.Point(4, 4);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(1529, 833);
            this.tabPage10.TabIndex = 9;
            this.tabPage10.Text = "tabPage10";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel29
            // 
            this.tableLayoutPanel29.ColumnCount = 2;
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel29.Controls.Add(this.deleetShippers, 0, 0);
            this.tableLayoutPanel29.Controls.Add(this.updateShippers, 0, 0);
            this.tableLayoutPanel29.Location = new System.Drawing.Point(8, 548);
            this.tableLayoutPanel29.Name = "tableLayoutPanel29";
            this.tableLayoutPanel29.RowCount = 1;
            this.tableLayoutPanel29.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel29.Size = new System.Drawing.Size(599, 61);
            this.tableLayoutPanel29.TabIndex = 8;
            // 
            // deleetShippers
            // 
            this.deleetShippers.BorderRadius = 35;
            this.deleetShippers.CheckedState.Parent = this.deleetShippers;
            this.deleetShippers.CustomImages.Parent = this.deleetShippers;
            this.deleetShippers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deleetShippers.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(50)))), ((int)(((byte)(150)))));
            this.deleetShippers.FillColor2 = System.Drawing.Color.SteelBlue;
            this.deleetShippers.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.deleetShippers.ForeColor = System.Drawing.Color.White;
            this.deleetShippers.HoverState.Parent = this.deleetShippers;
            this.deleetShippers.Location = new System.Drawing.Point(302, 3);
            this.deleetShippers.Name = "deleetShippers";
            this.deleetShippers.ShadowDecoration.Parent = this.deleetShippers;
            this.deleetShippers.Size = new System.Drawing.Size(294, 55);
            this.deleetShippers.TabIndex = 3;
            this.deleetShippers.Text = "Delete";
            this.deleetShippers.Click += new System.EventHandler(this.guna2GradientTileButton7_Click);
            // 
            // updateShippers
            // 
            this.updateShippers.BorderRadius = 34;
            this.updateShippers.CheckedState.Parent = this.updateShippers;
            this.updateShippers.CustomImages.Parent = this.updateShippers;
            this.updateShippers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updateShippers.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(50)))), ((int)(((byte)(150)))));
            this.updateShippers.FillColor2 = System.Drawing.Color.SteelBlue;
            this.updateShippers.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.updateShippers.ForeColor = System.Drawing.Color.White;
            this.updateShippers.HoverState.Parent = this.updateShippers;
            this.updateShippers.Location = new System.Drawing.Point(3, 3);
            this.updateShippers.Name = "updateShippers";
            this.updateShippers.ShadowDecoration.Parent = this.updateShippers;
            this.updateShippers.Size = new System.Drawing.Size(293, 55);
            this.updateShippers.TabIndex = 2;
            this.updateShippers.Text = "update";
            this.updateShippers.Click += new System.EventHandler(this.guna2GradientTileButton8_Click_1);
            // 
            // tableLayoutPanel30
            // 
            this.tableLayoutPanel30.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel30.ColumnCount = 1;
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel30.Controls.Add(this.dataGridView6, 0, 0);
            this.tableLayoutPanel30.Location = new System.Drawing.Point(8, 185);
            this.tableLayoutPanel30.Name = "tableLayoutPanel30";
            this.tableLayoutPanel30.RowCount = 1;
            this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel30.Size = new System.Drawing.Size(1516, 357);
            this.tableLayoutPanel30.TabIndex = 7;
            // 
            // dataGridView6
            // 
            this.dataGridView6.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView6.Location = new System.Drawing.Point(3, 3);
            this.dataGridView6.Name = "dataGridView6";
            this.dataGridView6.RowHeadersWidth = 51;
            this.dataGridView6.RowTemplate.Height = 24;
            this.dataGridView6.Size = new System.Drawing.Size(1510, 351);
            this.dataGridView6.TabIndex = 0;
            // 
            // tableLayoutPanel31
            // 
            this.tableLayoutPanel31.ColumnCount = 2;
            this.tableLayoutPanel31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel31.Controls.Add(this.searchBar2, 0, 0);
            this.tableLayoutPanel31.Controls.Add(this.searchShippers, 1, 0);
            this.tableLayoutPanel31.Location = new System.Drawing.Point(5, 114);
            this.tableLayoutPanel31.Name = "tableLayoutPanel31";
            this.tableLayoutPanel31.RowCount = 1;
            this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel31.Size = new System.Drawing.Size(773, 67);
            this.tableLayoutPanel31.TabIndex = 6;
            // 
            // searchBar2
            // 
            this.searchBar2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.searchBar2.DefaultText = "";
            this.searchBar2.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.searchBar2.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.searchBar2.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.searchBar2.DisabledState.Parent = this.searchBar2;
            this.searchBar2.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.searchBar2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchBar2.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.searchBar2.FocusedState.Parent = this.searchBar2;
            this.searchBar2.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.searchBar2.HoverState.Parent = this.searchBar2;
            this.searchBar2.Location = new System.Drawing.Point(4, 4);
            this.searchBar2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.searchBar2.Name = "searchBar2";
            this.searchBar2.PasswordChar = '\0';
            this.searchBar2.PlaceholderText = "search shipper by name";
            this.searchBar2.SelectedText = "";
            this.searchBar2.ShadowDecoration.Parent = this.searchBar2;
            this.searchBar2.Size = new System.Drawing.Size(533, 59);
            this.searchBar2.TabIndex = 0;
            // 
            // searchShippers
            // 
            this.searchShippers.CheckedState.Parent = this.searchShippers;
            this.searchShippers.CustomImages.Parent = this.searchShippers;
            this.searchShippers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchShippers.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(50)))), ((int)(((byte)(150)))));
            this.searchShippers.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.searchShippers.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.searchShippers.ForeColor = System.Drawing.Color.White;
            this.searchShippers.HoverState.Parent = this.searchShippers;
            this.searchShippers.Location = new System.Drawing.Point(544, 3);
            this.searchShippers.Name = "searchShippers";
            this.searchShippers.ShadowDecoration.Parent = this.searchShippers;
            this.searchShippers.Size = new System.Drawing.Size(226, 61);
            this.searchShippers.TabIndex = 1;
            this.searchShippers.Text = "search";
            this.searchShippers.Click += new System.EventHandler(this.guna2GradientTileButton9_Click);
            // 
            // tableLayoutPanel28
            // 
            this.tableLayoutPanel28.ColumnCount = 1;
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.Controls.Add(this.guna2HtmlLabel3, 0, 0);
            this.tableLayoutPanel28.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel28.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel28.Name = "tableLayoutPanel28";
            this.tableLayoutPanel28.RowCount = 1;
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.Size = new System.Drawing.Size(1523, 105);
            this.tableLayoutPanel28.TabIndex = 2;
            // 
            // guna2HtmlLabel3
            // 
            this.guna2HtmlLabel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.guna2HtmlLabel3.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel3.Location = new System.Drawing.Point(569, 24);
            this.guna2HtmlLabel3.Name = "guna2HtmlLabel3";
            this.guna2HtmlLabel3.Size = new System.Drawing.Size(384, 56);
            this.guna2HtmlLabel3.TabIndex = 0;
            this.guna2HtmlLabel3.Text = "Manage Shippers";
            // 
            // mainPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1812, 866);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.bunifuShadowPanel1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "mainPage";
            this.Text = "mainPage";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.mainPage_FormClosing);
            this.Load += new System.EventHandler(this.mainPage_Load);
            this.bunifuShadowPanel1.ResumeLayout(false);
            this.bunifuPanel1.ResumeLayout(false);
            this.manage_stock_subPanel.ResumeLayout(false);
            this.manage_pro_subPanel.ResumeLayout(false);
            this.manage_manu_subPanel.ResumeLayout(false);
            this.manage_emp_subPanel.ResumeLayout(false);
            this.bunifuPanel2.ResumeLayout(false);
            this.bunifuPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuPictureBox1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.bunifuPages1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.bunifuShadowPanel13.ResumeLayout(false);
            this.bunifuShadowPanel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.bunifuShadowPanel12.ResumeLayout(false);
            this.bunifuShadowPanel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.bunifuShadowPanel11.ResumeLayout(false);
            this.bunifuShadowPanel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.bunifuShadowPanel9.ResumeLayout(false);
            this.bunifuShadowPanel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.bunifuShadowPanel10.ResumeLayout(false);
            this.bunifuShadowPanel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.bunifuShadowPanel8.ResumeLayout(false);
            this.bunifuShadowPanel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.bunifuPanel3.ResumeLayout(false);
            this.bunifuPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.bunifuShadowPanel7.ResumeLayout(false);
            this.bunifuShadowPanel7.PerformLayout();
            this.bunifuShadowPanel6.ResumeLayout(false);
            this.bunifuShadowPanel6.PerformLayout();
            this.bunifuShadowPanel5.ResumeLayout(false);
            this.bunifuShadowPanel5.PerformLayout();
            this.bunifuShadowPanel2.ResumeLayout(false);
            this.bunifuShadowPanel2.PerformLayout();
            this.bunifuShadowPanel4.ResumeLayout(false);
            this.bunifuShadowPanel4.PerformLayout();
            this.bunifuShadowPanel3.ResumeLayout(false);
            this.bunifuShadowPanel3.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tableLayoutPanel19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.tableLayoutPanel18.ResumeLayout(false);
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel17.PerformLayout();
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel16.PerformLayout();
            this.tabPage8.ResumeLayout(false);
            this.tableLayoutPanel23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.tableLayoutPanel22.ResumeLayout(false);
            this.tableLayoutPanel21.ResumeLayout(false);
            this.tableLayoutPanel21.PerformLayout();
            this.tableLayoutPanel20.ResumeLayout(false);
            this.tableLayoutPanel20.PerformLayout();
            this.tabPage9.ResumeLayout(false);
            this.tableLayoutPanel26.ResumeLayout(false);
            this.tableLayoutPanel27.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            this.tableLayoutPanel25.ResumeLayout(false);
            this.tableLayoutPanel24.ResumeLayout(false);
            this.tableLayoutPanel24.PerformLayout();
            this.tabPage10.ResumeLayout(false);
            this.tableLayoutPanel29.ResumeLayout(false);
            this.tableLayoutPanel30.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).EndInit();
            this.tableLayoutPanel31.ResumeLayout(false);
            this.tableLayoutPanel28.ResumeLayout(false);
            this.tableLayoutPanel28.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel3;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel4;
        private Bunifu.UI.WinForms.BunifuPanel bunifuPanel1;
        private Bunifu.UI.WinForms.BunifuPanel bunifuPanel2;
        private System.Windows.Forms.Label label21;
        private Bunifu.UI.WinForms.BunifuPictureBox bunifuPictureBox1;
        private Bunifu.UI.WinForms.BunifuShadowPanel bunifuShadowPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Bunifu.UI.WinForms.BunifuPages bunifuPages1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Guna.UI2.WinForms.Guna2HScrollBar guna2HScrollBar1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private Bunifu.UI.WinForms.BunifuPanel bunifuPanel3;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private Bunifu.UI.WinForms.BunifuShadowPanel bunifuShadowPanel7;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label6;
        private Bunifu.UI.WinForms.BunifuShadowPanel bunifuShadowPanel6;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label7;
        private Bunifu.UI.WinForms.BunifuShadowPanel bunifuShadowPanel5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label8;
        private Bunifu.UI.WinForms.BunifuShadowPanel bunifuShadowPanel2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label3;
        private Bunifu.UI.WinForms.BunifuShadowPanel bunifuShadowPanel4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label4;
        private Bunifu.UI.WinForms.BunifuShadowPanel bunifuShadowPanel3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private Guna.UI2.WinForms.Guna2HScrollBar guna2HScrollBar2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private Bunifu.UI.WinForms.BunifuShadowPanel bunifuShadowPanel13;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label20;
        private Bunifu.UI.WinForms.BunifuShadowPanel bunifuShadowPanel12;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label19;
        private Bunifu.UI.WinForms.BunifuShadowPanel bunifuShadowPanel11;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label label18;
        private Bunifu.UI.WinForms.BunifuShadowPanel bunifuShadowPanel9;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label16;
        private Bunifu.UI.WinForms.BunifuShadowPanel bunifuShadowPanel10;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label17;
        private Bunifu.UI.WinForms.BunifuShadowPanel bunifuShadowPanel8;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel manage_emp_subPanel;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton1;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton2;
        private System.Windows.Forms.Panel manage_manu_subPanel;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton3;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel manage_stock_subPanel;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton7;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton8;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Panel manage_pro_subPanel;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton5;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton6;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel36;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel2;
        private Guna.UI2.WinForms.Guna2TextBox emp_name;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel26;
        private Guna.UI2.WinForms.Guna2TextBox emp_pin;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel28;
        private Guna.UI2.WinForms.Guna2TextBox emp_cnic;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel27;
        private Guna.UI2.WinForms.Guna2TextBox emp_gender;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel30;
        private Guna.UI2.WinForms.Guna2TextBox emp_salary;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel29;
        private Guna.UI2.WinForms.Guna2ComboBox emp_designation;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel32;
        private Guna.UI2.WinForms.Guna2DateTimePicker emp_dob;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel31;
        private Guna.UI2.WinForms.Guna2TextBox emp_city;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel34;
        private Guna.UI2.WinForms.Guna2TextBox emp_country;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel33;
        private System.Windows.Forms.TextBox emp_address;
        private Bunifu.UI.WinForms.BunifuLabel emp_contact;
        private Bunifu.UI.WinForms.BunifuLabel emp_email;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox1;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private Guna.UI2.WinForms.Guna2GradientButton addMedicine;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private Guna.UI2.WinForms.Guna2DateTimePicker expiryDate;
        private Guna.UI2.WinForms.Guna2DateTimePicker manufacturingDate;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label medicineCompanyName;
        private System.Windows.Forms.Label medicineManufacturinfgDate;
        private System.Windows.Forms.Label medicineExpiryDate;
        private System.Windows.Forms.Label medicineDescription;
        private System.Windows.Forms.Label label31;
        private Guna.UI2.WinForms.Guna2TextBox medicineName;
        private Guna.UI2.WinForms.Guna2TextBox medicinePurchasePrice;
        private Guna.UI2.WinForms.Guna2TextBox medicineSalePrice;
        private Guna.UI2.WinForms.Guna2ComboBox companyNameCombobox;
        private Guna.UI2.WinForms.Guna2TextBox description;
        private Guna.UI2.WinForms.Guna2ComboBox medicineTypeComboBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private Guna.UI2.WinForms.Guna2TextBox searchBar;
        private Guna.UI2.WinForms.Guna2GradientTileButton guna2GradientTileButton1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private Guna.UI2.WinForms.Guna2GradientTileButton guna2GradientTileButton3;
        private Guna.UI2.WinForms.Guna2GradientTileButton guna2GradientTileButton2;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private Guna.UI2.WinForms.Guna2TextBox companyName;
        private Guna.UI2.WinForms.Guna2TextBox companyCity;
        private Guna.UI2.WinForms.Guna2TextBox companyContactNumber;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel23;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private Guna.UI2.WinForms.Guna2TextBox shipperName;
        private Guna.UI2.WinForms.Guna2TextBox shipperContactNumber;
        private Guna.UI2.WinForms.Guna2TextBox shipperAddress;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private Guna.UI2.WinForms.Guna2TextBox companyEmail;
        private System.Windows.Forms.Label label43;
        private Guna.UI2.WinForms.Guna2TextBox companyCountry;
        private Guna.UI2.WinForms.Guna2TextBox companyAddress;
        private Guna.UI2.WinForms.Guna2ComboBox shipperCompanyNameComboBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel26;
        private Guna.UI2.WinForms.Guna2GradientTileButton deleteCompany;
        private Guna.UI2.WinForms.Guna2GradientTileButton updateCompany;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel27;
        private System.Windows.Forms.DataGridView dataGridView5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel25;
        private Guna.UI2.WinForms.Guna2TextBox searchBar1;
        private Guna.UI2.WinForms.Guna2GradientTileButton searchCompany;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel24;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel29;
        private Guna.UI2.WinForms.Guna2GradientTileButton deleetShippers;
        private Guna.UI2.WinForms.Guna2GradientTileButton updateShippers;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel30;
        private System.Windows.Forms.DataGridView dataGridView6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel31;
        private Guna.UI2.WinForms.Guna2TextBox searchBar2;
        private Guna.UI2.WinForms.Guna2GradientTileButton searchShippers;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel28;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel3;
    }
}