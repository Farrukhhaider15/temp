﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Final_DB_Project
{
    public partial class employeePage : Form
    {
        public static employeePage ep = new employeePage();
        int empId = 24;
        public employeePage()
        {
            InitializeComponent();
            customizeDesign();
        }
        private void customizeDesign()
        {
            manage_cus_subPanel.Visible = false;
            loan_subPanel.Visible = false;

        }
        private void hideSubMenu()
        {
            if (manage_cus_subPanel.Visible == true)
            {
                manage_cus_subPanel.Visible = false;
            }
            if (loan_subPanel.Visible == true)
            {
                loan_subPanel.Visible = false;
            }
        }
        private void showSubMenu(Panel subMenu)
        {
            if (subMenu.Visible == false)
            {
                hideSubMenu();
                subMenu.Visible = true;
            }
            else
            {
                subMenu.Visible = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            showSubMenu(manage_cus_subPanel);
        }

        private void bunifuButton6_Click(object sender, EventArgs e)
        {
            bunifuPages1.SetPage(4);
            
        }

        private void bunifuButton5_Click(object sender, EventArgs e)
        {
            bunifuPages1.SetPage(5);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            bunifuPages1.SetPage(0);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            bunifuPages1.SetPage(1);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            bunifuPages1.SetPage(2);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bunifuPages1.SetPage(3);
        }

        private void bunifuButton1_Click(object sender, EventArgs e)
        {
            bunifuPages1.SetPage(6);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            showSubMenu(loan_subPanel);
        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void bunifuButton9_Click(object sender, EventArgs e)
        {
            bool checkValue = false;
            int getId = 0;
            int salary = 0;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd3 = new SqlCommand("SELECT RegistrationNumber FROM EmployeeDetails WHERE Name = @Name", con);
            cmd3.Parameters.AddWithValue("@Name", emp_name.Text);
            SqlDataReader myReader2 = cmd3.ExecuteReader();
            while (myReader2.Read())
            {
                getId = int.Parse(myReader2.GetValue(0).ToString());
            }
            myReader2.Close();
            SqlCommand cmd4 = new SqlCommand("SELECT Salary FROM EmployeeDetails WHERE Name = @Name", con);
            cmd4.Parameters.AddWithValue("@Name", emp_name.Text);
            SqlDataReader myReader3 = cmd4.ExecuteReader();
            while (myReader3.Read())
            {
                salary = int.Parse(myReader3.GetValue(0).ToString());
            }
            myReader3.Close();
            if (emp_loan_purpose.Text != "" && emp_loan_price.Text != "" && int.Parse(emp_loan_price.Text) > 0)
            {
                checkValue = true;
                SqlCommand cmd = new SqlCommand("Insert into EmployeeLoan values (@Id,@PharId,@Purpose,@Amount,@ApplyDate", con);
                cmd.Parameters.AddWithValue("@Id", getId);
                cmd.Parameters.AddWithValue("@PharId", 1);
                cmd.Parameters.AddWithValue("@Purpose", emp_loan_purpose.Text);
                cmd.Parameters.AddWithValue("@Amount", int.Parse(emp_loan_price.Text));
                cmd.Parameters.AddWithValue("@ApplyDate", DateTime.Parse(emp_apply_date.Text));
                cmd.ExecuteNonQuery();
                MessageBox.Show("Loan Request Submitted");

            }
            else if (int.Parse(emp_loan_price.Text) < 0)
            {
                MessageBox.Show("Please enter valid loan price");
            }
            if(checkValue == false)
            {
                MessageBox.Show("Please fill all values");
            }

        }

        private void bunifuButton4_Click(object sender, EventArgs e)
        {
            bunifuPages1.SetPage(2);
        }

        private void bunifuButton3_Click(object sender, EventArgs e)
        {
            bunifuPages1.SetPage(6);
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT EmployeeLoan.Id,EmployeeDetails.Name,EmployeeLoan.Purpose,EmployeeLoan.Amount,EmployeeLoan.Purpose FROM EmployeeDetails,EmployeeLoan WHERE EmployeeDetails.RegistrationNumber = @Emp_Id", con);
            cmd.Parameters.AddWithValue("@Emp_Id", empId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            emp_loan_grid.DataSource = dt;
            emp_loan_grid.Columns["Id"].Visible = false;    
        }
    }
}
